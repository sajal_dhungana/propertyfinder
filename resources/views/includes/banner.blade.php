<section class="main-banner" style="background-image: url({{asset('images/banner-bg.jpg')}});">
	<div class="container">
		<div class="carousel-cap-wrap">
			<div class="search-form">
					<h2>Find the property you prefer.</h2>
					<p>Search properties for sale and to rent.</p>
				<form action="{{url('search')}}" method="post">
						{{csrf_field()}} 
					
					<div class="form-group row">

						<div class="col-sm-4 col-md-3">
							<select class="form-control" name="property_purpose_id" id="property_purpose_id">
								<option value="">--Property Purpose--</option>

								@foreach($purpose as $p)
									<option value="{{$p->id}}">{{$p->name}}</option>
								@endforeach
							</select>
						</div>

							<div class="col-sm-4 col-md-3">
							<select class="form-control" name="property_type_id" id="property_type_id" onchange="getMinMaxPrice()">
								<option value="">--Property Type--</option>

								@foreach($propertyType as $t)
									<option value="{{$t->id}}">{{$t->name}}</option>
								@endforeach
							</select>
						</div>
		

						<div class="col-sm-4 col-md-3">
							<select class="form-control" name="city_id" id="city_id">
								<option value="">--Location--</option>

								@foreach($city as $c)
									<option value="{{$c->id}}">{{$c->name}}</option>
								@endforeach
							</select>
						</div>

							<!-- <input type="text" class="col-md-1" placeholder="Minimum Price" style="width:10%" name="min_price" id="min_price">
										<input type="text" class="col-md-1" placeholder="Maximum Price" style="width:10%" name="max_price" id="max_price"> -->

						<div class="col-sm-4 col-md-3">
							<select class="form-control" name="min_price" id="min_price">
								<option value="">--Min Price--</option>
									
					
							</select>
						</div>

						<div class="col-sm-4 col-md-3">
							<select class="form-control" name="max_price" id="max_price">
								<option value="">--Max Price--</option>
									
					
							</select>
						</div>

						<div class="col-sm-4 col-md-3">
							<button type="submit" class="btn-button" name="search">Search</button>
							<!-- <button type="submit" class="btn-button" name="search_sale">For sale</button>
							<button type="submit" class="btn-button" name="search_rent">To rent</button> -->
						</div>
					</div><!--end .form-group-->

					<!--end .row-->
				</form>
				
			
		
	
			</div><!--end .search-form-->
		</div>
	</div><!--end .container-->
</section>