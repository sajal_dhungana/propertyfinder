<!-- Modal Log In \ Register-->
<div class="modal fade modalLoginRegister" id="modalLoginRegister" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="left-block">
                <img src="images/login-bg.jpg" alt="">
                <div class="left-block-content">
                    <p class="tag">#Arange Properties</p>
                    <div class="signin-form">
                        <h4>SIGN IN</h4>
                        <form action="#" method="post">
                            <input type="email" name="login_email" placeholder="EMAIL ADDRESS" required>
                            <input type="password" placeholder="PASSWORD" name="login_password" required>
                            <input type="submit" value="LOGIN" name="login">
                        </form>
                        <p><a href="#">Forgot Password?</a></p>
                    </div><!--end .signin-form-->
                    <div class="signin-option">
                        <p><span>OR</span></p>
                        <a href="#"><i class="fa fa-facebook"></i>Login with Facebook</a>
                        <a href="#"><i class="fa fa-twitter"></i>Login with Twitter</a>
                    </div><!--end .sign-->
                </div><!--end .left-block-content-->
            </div><!--end .left-block-->
            <div class="right-block">
                <div class="right-block-wrap">
                    <div class="register-form">
                        <form action="#" method="post">
                            <h4>SIGN UP</h4>
                            <div class="form-group">
                                <div class="input-items">
                                    <input type="text" name="first_name" placeholder="FIRST NAME" required>
                                    <span></span>
                                    <input type="text" placeholder="LAST NAME" name="last_name" required>
                                </div><!--end .inputs-field-->
                            </div><!--end .form-group-->
                            <div class="form-group">
                                <input type="email" name="email" placeholder="EMAIL ADDRESS" required>
                            </div><!--end .form-group-->
                            <div class="form-group">
                                <input type="text" placeholder="CONTACT NO." name="contact_num">
                            </div><!--end .form-group-->
                            <div class="form-group">
                                <div class="input-items input-items-date">
                                    <input type="text" placeholder="ADDRESS" name="address" required>
                                </div><!--end .input-items-->
                            </div><!--end .form-group-->
                            <div class="form-group validate-success">
                                <input type="password" name="password" placeholder="PASSWORD" required>
                            </div><!--end .form-group-->
                            <div class="form-group">
                                <input type="password" name="verity_password" placeholder="VERIFY PASSWORD">
                            </div><!--end .form-group-->
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">
                                        <span></span>
                                        I Agree to the Terms & Conditions
                                    </label>
                                </div><!--end .checkbox-->
                            </div><!--end .form-group-->

                            <div class="form-group">
                                <input type="submit" value="REGISTER" name="register">
                            </div><!--end .form-group-->
                            <ul class="clearfix">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                        Register with Twitter
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                        Register with Facebook
                                    </a>
                                </li>
                            </ul>
                        </form>
                    </div><!--end .register-form-->
                </div><!--end .right-block-wrap-->
            </div><!--end .right-block-->
        </div>

    </div>
</div><!--end .modal-->

<div id="output"></div>