<div class="search-form">
    <h2>Find your happy</h2>
    <p>Search properties for sale and to rent in the Nepal</p>
    <form action="{{url('search')}}" method="post">
    {{csrf_field()}} 
        <div class="form-group">
            <div class="row">
                <div class="col-xs-6">
                    <select class="form-control combobox" onchange="getCity()" name="country_id" id="country_id" required>
                        <option value="">Select Country *</option>

                        @foreach($country as $c)
                            <option value="{{$c->id}}">{{$c->name}}</option>
                        @endforeach
                        
                    </select>
                </div><!--end .col-xs-6-->

                <div class="col-xs-6">
                    <select class="form-control" name="city_id" id="city_id">
                        <option value="">--Select City--</option>
                    </select>
                </div><!--end .col-xs-6-->
            </div><!--end .row-->
        </div><!--end .form-group-->
        <div class="row">
            <div class="col-xs-12 text-right">
                <button type="submit" class="btn-button" name="search">Search</button>
                <!-- <button type="submit" class="btn-button" name="search_sale">For sale</button>
                <button type="submit" class="btn-button" name="search_rent">To rent</button> -->
            </div><!--end .col-xs-6-->
        </div><!--end .row-->
    </form>
  
</div><!--end .search-form-->

<script>
function getCity(){
                $('#city_id').html('<option value="">--Select City--</option>');
                var country = document.getElementById('country_id').value;
                $.ajax({
                url: "{{url('getCity')}}"+'/'+country,
                type: 'GET',
                success: function(res) {
                    $('#city_id').html(res);
                }
            });
        
    }
<script>

