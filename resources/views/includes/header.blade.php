<?php
    $settings = \App\SettingModel::first();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{$settings['site_title']}}</title>
    <base href="{{url('/')}}">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{asset('images/ghar-logo.png')}}">

    <!-- core CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap-style.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/slick-slider.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-combobox.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.min.css')}}">
    <!-- <script src="{{asset('js/jquery-1.12.0.min.js')}}"></script> -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

   <!--  <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8uOyHB_Wr-iaE2TWKTEdc9T-_VN1-AhE&callback=initMap&libraries=&v=weekly"
      defer
    ></script> -->

    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

  <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v9.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="102299941758167">
      </div>  

<header  class="main-header">
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="text-right">
                        <li><div><a href="<?php if(auth::user()){echo url('agent/property/create');}else{echo route('login');}?>">Post Property</a></div></li>
                        <li><a href="{{url('careers')}}">Careers</a></li>
                        <li><a href="{{url('contact')}}">Contact us</a></li>
                    </ul>
                </div><!--end .col-md-7-->
            </div><!--end .row-->
        </div><!--end .container-->
    </div><!--end .top-bar-->
    <!--navigation-->
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light navbar-default">
            <a class="navbar-brand logo" href=""><img src="{{asset('images/ghar-logo.png')}}" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="nav navbar-nav ml-auto d-flex">
                    <li class="nav-item active">
                        <a href="{{url('buy')}}">Buy</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('rent')}}">Rent</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('agent')}}">Find Agent</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('login')}}">Login</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- 
        
     -->
    <!--end .navigation-->
</header><!--end .main-header-->