<?php
    $socialSettings = \App\SocialSettingModel::first();

    $settings = \App\SettingModel::first();

    $city = \App\CityModel::all();
?>
<footer class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 footer-main-left">
                <div class="row">
                    <div class="col-12">
                        <div class="left-title">Real state in Nepal</div>
                        <div class="links">
                          @foreach($city as $c)
                            <a href="{{url('city/'.$c->id.'/property')}}">Property in {{ucfirst($c->name)}}</a>
                          @endforeach
                        </div>
                    </div>
                </div>
                <!-- <div class="row mt-5">
                    <div class="col-12">
                        <div class="left-title">New Projects in Nepal</div>
                        <div class="links">
                            <a href="#">New Projects in Kathmandu</a><a href="#">New Projects in Chitwan</a>
                            <a href="#">New Projects in Pokhara</a>
                        </div>
                    </div>
                </div> -->
            </div><!--end .col-xs-6 col-sm-3-->
            <div class="col-sm-6 footer-main-right">
                <div class="footer-logo">
                    <a href=""><img src="{{asset('images/ghar-logo.png')}}" alt=""></a>
                </div>
                <div class="footer-about">
                    Launched in 2020, Ghargharana is Nepal's No.1 online Property marketplace to buy, sell, and rent residential and commercial properties. Adjudged as the most preferred real estate portal in Nepal by various independent surveys, Ghargharana offers a one-stop destination for all Property needs.
                </div>
                <div class="row mt-5">
                    <div class="col-6 footer-app-box">
                        <a href=""><img src="{{asset('images/google-play.png')}}" alt=""></a>
                        <a href=""><img src="{{asset('images/appStore.png')}}" alt=""></a>
                    </div>
                    <div class="col-6 social-links">
                        <ul>
                            <li><a href="{{$socialSettings['site_facebook']}}" class="fab fa-facebook-f" target="_blank" data-toggle="tooltip" data-placement="top" title="Follow on Facebook"></a></li>
                            <li><a href="{{$socialSettings['site_twitter']}}" class="fab fa-twitter" target="_blank" data-toggle="tooltip" data-placement="top" title="Follow on Twitter"></a></li>
                            <li><a href="{{$socialSettings['site_youtube']}}" class="fab fa-youtube" target="_blank" data-toggle="tooltip" data-placement="top" title="Follow on YouTube"></a></li>
                            <li><a href="{{$socialSettings['site_instagram']}}" class="fab fa-instagram" target="_blank" data-toggle="tooltip" data-placement="top" title="Follow on Instagram"></a></li>
                        </ul>
                    </div>
                </div>
            </div><!--end .col-xs-6 col-sm-3-->
            
            
            
        </div><!--end .row-->
    </div><!--end .container-->
    <div class="footer-menu">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul>
                        <li><a href="{{url('sitemap')}}">Sitemap</a></li>
                        <li><a href="{{url('terms')}}">Terms & Conditions</a></li>
                        <li><a href="{{url('policy')}}">Privacy Policy</a></li>
                        <li><a href="{{url('blogs')}}">Blog</a></li>
                        <li><a href="{{url('careers')}}">Careers</a></li>
                        <li><a href="{{url('faq')}}">FAQ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-desc">
        <div class="container">
            <div class="row">
                <div class="col-12">
                Disclaimer: Ghargharana Realty Services Limited is only an intermediary offering its platform to advertise properties of Seller for a Customer/Buyer/User coming on its Website and is not and cannot be a party to or privy to or control in any manner any transactions between the Seller and the Customer/Buyer/User. All the offers and discounts on this Website have been extended by various Builder(s)/Developer(s) who have advertised their products. Ghargharana is only communicating the offers and not selling or rendering any of those products or services. It neither warrants nor is it making any representations with respect to offer(s) made on the site. Ghargharana Realty Services Limited shall neither be responsible nor liable to mediate or resolve any disputes or disagreements between the Customer/Buyer/User and the Seller and both Seller and Customer/Buyer/User shall settle all such disputes without involving Ghargharana Realty Services Limited in any manner.
                </div>
            </row>
        </div>
    </div>
    <div class="copyright">
        <p>&copy; Copyright 2020</p>
    </div><!--end .copyright-->
</footer><!--end #footer-->
@include('includes.popup')


<!-- <script src="{{asset('js/jquery-1.12.0.min.js')}}"></script> -->
<script src="{{asset('js/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<!-- <script src="{{asset('js/bootstrap.min.js')}}"></script> -->
<script src="{{asset('js/slick.js')}}"></script>
<script src="{{asset('js/bootstrap-combobox.js')}}"></script>
<script src="{{asset('js/ajax.js')}}"></script>
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>




<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip()


  $('.agenciesTooltip').tooltip()
});

	$('.ads-gallery .owl-carousel').owlCarousel({
    autoplay: false,
    autoplayTimeout: 6000,
    loop: true,
    animateOut: "fadeOut",
    margin: 15,
    stagePadding: 0,
    drag: true,
    // pagination: true,
    // mouseDrag: false,
    // pullDrag: false,
    // freeDrag: false,
    // touchDrag: false,
    // dotsEach: true,
    dots : false,
    nav: true,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ],
    items: 1,
    responsive:{
      0:{
          items:1
      },
      475:{
          items:1
      },
      675:{
          items:2
      },
      991:{
          items :3
      },
      1200:{
          items:3
      }
  }
  });
  $('.agents-gallery .owl-carousel').owlCarousel({
    autoplay: true,
    autoplayTimeout: 10000,
    loop: true,
    animateOut: "fadeOut",
    margin: 0,
    stagePadding: 0,
    drag: true,
    // pagination: true,
    // mouseDrag: false,
    // pullDrag: false,
    // freeDrag: false,
    // touchDrag: false,
    // dotsEach: true,
    dots : false,
    nav: true,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ],
    items: 1,
    responsive:{
      0:{
          items:4
      },
      475:{
          items:7
      },
      675:{
          items:9
      },
      991:{
          items :13
      },
      1200:{
          items:17
      }
  }
  });
  $('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
    var next = $(this).next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
  
    for (var i=0;i<4;i++) {
      next=next.next();
      if (!next.length) {
        next=$(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
    }
  });
  $('.blogsCarousel .owl-carousel').owlCarousel({
    autoplay: true,
    autoplayTimeout: 10000,
    loop: true,
    animateOut: "fadeOut",
    margin: 15,
    stagePadding: 0,
    drag: true,
  // pagination: true,
  // mouseDrag: false,
  // pullDrag: false,
  // freeDrag: false,
  // touchDrag: false,
  // dotsEach: true,
  dots : false,
    nav: true,
  navText: [
      '<i class="fa fa-angle-left" aria-hidden="true"></i>',
      '<i class="fa fa-angle-right" aria-hidden="true"></i>'
      ],
  items: 1,
  responsive:{
    0:{
        items:1
    },
    475:{
        items:1
    },
    675:{
        items:2
    },
    991:{
        items :3
    },
    1200:{
        items:4
    }
}
});
$('.featuredBlogCarousel .owl-carousel').owlCarousel({
    autoplay: true,
    autoplayTimeout: 10000,
    loop: true,
    animateOut: "fadeOut",
    drag: true,
  dots : false,
    nav: true,
  navText: [
      '<i class="fa fa-angle-left" aria-hidden="true"></i>',
      '<i class="fa fa-angle-right" aria-hidden="true"></i>'
      ],
  items: 1,
  responsive:{
    0:{
        items:1
    },
    475:{
        items:1
    },
    675:{
        items:1
    },
    991:{
        items :1
    },
    1200:{
        items:1
    }
}
});
								</script>



<?php
if(isset($_SESSION['action']))
{
   // echo $_SESSION['action'];
    if(($_SESSION['action']))
    {
        ?>
        <div id="actionModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <p class="text-center" style="color: #00aa00;"><?php echo $_SESSION['action']; ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" style="width: auto">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <script> $(window).on('load',function(){
                $('#actionModal').modal('show');
            });</script>
        <?php
    }
    //echo $_SESSION['action'];
    unset($_SESSION['action']);

}
//session_unset();
//session_destroy();
?>


</body>
</html>