@include('includes.header')

<style>
#map {
  height: 400px;
  width: 100%;

}

.comment_content{
	list-style: none;
	padding: 0;
	margin-bottom: 15px;
}

#video-box {
  height: 400px;
  width: 100%;
}
</style>

<div class="main-content" id="main-content">
	<div class="inner-content">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<article class="single-article" id="print-content">
						<div class="article-title">
							<div class="row">
								<div class="col-sm-12">
									<h1>{{$property->title}} : {{$property->subtitle}}</h1>
									
									<div class="row">
										<div class="col-sm-6">
											<div style="font-size: 10px;">Posted on: 
												<?php   $time = strtotime($property->created_at);

													echo $postDate = date('M d Y', $time);
											
												?>
											</div>
										</div>
										<div class="col-sm-6">
										@if($property->price)
											<div class="price-par">	Price :
													Rs. <span class="price"> {{$property->price}}</span>
											</div>
										@endif
										</div>		
									
										
									</div>
								</div><!--end .-->
									<!--end .col-sm-5-->
							</div><!--end .row-->
						</div><!--end .article-title-->
						<!-- <div class="item-slider mb-5">
							<div class="slider-for">

								<div><a href="{{asset('uploads/property/'.$property->image)}}"><img src="{{asset('uploads/property/'.$property->image)}}" alt="Photo of {{$property->title}}"></a></div>
								<div class="view-wrap">
									<i class="fa fa-eye ">{{$property->views}}</i>
								</div>
							</div>
							<div class="slider-nav">
								<ul>

								@foreach($gallary as $g)

									<li><a href="{{asset('uploads/property/'.$g->image)}}"><img src="{{asset('uploads/property/'.$g->image)}}" alt="{{$property->title}}"></a></li>
								@endforeach
								</ul>
							</div>
						</div> -->

						<div id="custCarousel" class="carousel slide" data-interval="false">
						<!-- slides -->
							<div class="carousel-inner">
								<?php $j=0;?>

							@foreach($gallary as $g)
								<div class="carousel-item <?php if($j==0){?>active<?php }?>"> <img src="{{asset('uploads/property/'.$g->image)}}" alt=""> </div>
							<?php $j++; ?>
							@endforeach

							</div>

							<div class="view-wrap">
								<i class="far fa-eye ">{{$property->views}}</i>
							</div> <!-- Left right --> 
							<a class="carousel-control-prev" href="#custCarousel" data-slide="prev"> <i class="fas fa-chevron-left"></i> 
							</a> 

							<a class="carousel-control-next" href="#custCarousel" data-slide="next"> <i class="fa fa-chevron-right"></i>
							 </a>
							 
							<ol class="carousel-indicators list-inline">
								<?php $i=0;?>
							@foreach($gallary as $g)
								<li class="list-inline-item <?php if($i==0){?>active<?php }?>"> 
									<a id="carousel-selector-{{$i}}" class="selected" data-slide-to="{{$i}}" data-target="#custCarousel"> <img src="{{asset('uploads/property/'.$g->image)}}" class="img-fluid"> </a>
								 </li>
							<?php $i++;?>
							@endforeach
								

								<!-- <li class="list-inline-item"> <a id="carousel-selector-2" data-slide-to="2" data-target="#custCarousel"> <img src="https://i.imgur.com/83fandJ.jpg" class="img-fluid"> </a> 
								</li>
								<li class="list-inline-item"> <a id="carousel-selector-2" data-slide-to="3" data-target="#custCarousel"> <img src="https://i.imgur.com/JiQ9Ppv.jpg" class="img-fluid"> </a> 
								</li> -->
							</ol>
						</div>

						<div class="social-links gen-social-links" style="float:right; margin-right: 15px;">
							<ul>
								<li><a href="https://www.facebook.com/sharer.php?..." class="fab fa-facebook-f" target="_blank" data-toggle="tooltip" data-placement="top" title="Share on Facebook"></a></li>
								<li><a href="https://twitter.com/intent/tweet?..." target="blank" class="fab fa-twitter" rel="noopener noreferrer" data-toggle="tooltip" data-placement="top" title="Share on Twitter"></a></li>
								  <li><a href="https://www.linkedin.com/shareArticle?..." target="blank" rel="noopener noreferrer" class="fab fa-linkedin-in" data-toggle="tooltip" data-placement="top" title="Share on LinkedIn"></a></li>
								  <li><a href="https://www.linkedin.com/shareArticle?..." target="blank" rel="noopener noreferrer" class="fab fa-instagram" data-toggle="tooltip" data-placement="top" title="Share on Instagram"></a></li>
								  <li><a href="https://www.linkedin.com/shareArticle?..." target="blank" rel="noopener noreferrer" class="fab fa-whatsapp" data-toggle="tooltip" data-placement="top" title="Share on Whatsapp"></a></li>
								  <li><a href="https://www.linkedin.com/shareArticle?..." target="blank" rel="noopener noreferrer" class="fab fa-viber" data-toggle="tooltip" data-placement="top" title="Share on Viber"></a></li>
								  <li><a href="https://www.linkedin.com/shareArticle?..." target="blank" rel="noopener noreferrer" class="fab fa-weixin" data-toggle="tooltip" data-placement="top" title="Share on WeChat"></a></li>
								<!-- <li><a href="https://www.facebook.com/sharer.php?u=https://housing.com/in/buy/projects/page/50457-ace-divino-by-ace-group-in-noida-extension" class="fa fa-facebook" target="_blank"></a></li>
								<li><a href="https://twitter.com/intent/tweet
									?url=http%3A%2F%2Fcss-tricks.com%2F
									&text=Tips%2C+Tricks%2C+and+Techniques+on+using+Cascading+Style+Sheets.
									&hashtags=css,html" class="fa fa-twitter" target="_blank"></a></li>
								<li><a href="https://www.linkedin.com/shareArticle
									?mini=true
									&url=https%3A%2F%2Fwww.css-tricks.com%2F
									&title=CSS-Tricks
									&summary=Tips%2C+Tricks%2C+and+Techniques+on+using+Cascading+Style+Sheets.
									&source=CSS-Tricks" class="fa fa-linkedin" target="_blank"></a></li> -->
							</ul>
						</div>
<!-- check for tabs from here
 -->

						<ul class="nav nav-tabs tab-nav" id="myTab" role="tablist">
							<li class="nav-item" role="presentation">
								<a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
							</li>
							
							<?php
							$location_map = 0;
							if(!empty($property['latitude'] && $property['longitude'])){
								$location_map = 1;
								?>
								<li><a data-toggle="tab" href="#map-street">Map & Street View</a></li>
								<?php
							}
							?>

							<?php
								if(!empty($property['video_url'])){ ?>
								<li class="nav-item" role="presentation">
									<a class="nav-link" id="video-tab" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="false">Video</a>
								</li>

							<?php } ?>
							

						</ul>

<!-- end checking tabs panel here -->
<!-- start checking tabs content from here -->
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
								<div class="item-feature">
									<h4>Key features</h4>

										@if($property['property_type_id'])
										<?php $feature = App\PropertyFeatureModel::where('property_type_id',$property['property_type_id'])->get()->toArray();
											if($feature){
											$feature = $feature[0];
												$name = $feature['name'];

											$feature_list = explode(',',$name);
													
											foreach($feature_list as $list){
													$f = App\FeatureModel::where('id',$list)->get();

												if($f){
													$f = $f[0];
											?>

											<ul class="clearfix">
												   
												<li><i class="{{isset($f->icon->name)?$f->icon->name:''}}"></i>  {{$f->name}}
												<?php
												$bed = similar_text($f->name,'bedrooms');

												$bath = similar_text($f->name,'bathrooms');

												$kitchen = similar_text($f->name,'kitchen');

												$living = similar_text($f->name,'livingroom');

												if($bed>$bath && $bed>$kitchen && $bed>$living){
													echo $property['no_of_bedrooms'];
												}

												if($bath>$bed && $bath>$kitchen && $bath>$living){
													echo $property['no_of_bathrooms'];
												}

												if($kitchen>$bath && $kitchen>$bed && $kitchen>$living){
													echo $property['no_of_kitchens'];
												}

												if($living>$bath && $living>$bed && $living>$kitchen){
													echo $property['no_of_living_rooms'];
												}

																?>
														</li>
													</ul>
											<?php
													}
														
													}
													
												}
												
											?>
										@endif

										<ul class="clearfix">
												   
										   @if($property->near_hospital)
											 <li><b>Near Hospital   </b>   				@if($property->hospital_name)
													- {{$property->hospital_name}}
												 @endif
											 </li>


										   @endif             

											@if($property->near_school)
												<li><b>Near School</b>
													@if($property->school_name)
													- {{$property->school_name}}
													@endif
												</li>
											@endif

											@if($property->near_market)
												<li><b>Near Market</b>
													@if($property->market_name)
													- {{$property->market_name}}
													@endif
												</li>
											@endif

											@if($property->near_bank)
												<li><b>Near Bank</b>
													@if($property->bank_name)
													- {{$property->bank_name}}
													@endif
												</li>

											@endif

											@if($property->near_bus_stop)
												<li><b>Near Bus Stop</b>
													@if($property->bus_stop_name)
													- {{$property->bus_stop_name}}
													@endif
												</li>
											@endif

											@if($property->near_airport)
												<li><b>Near Airport</b>
													@if($property->airport_name)
													- {{$property->airport_name}}
													@endif
												</li>
											@endif
													   
										</ul>

									</div><!--end .item-->

									<div class="item-feature">
										<h4>General Details</h4>

											<table id="dataTable">
											<thead>
											</thead>
											<tbody>

												@if(isset($property->type->name))
													<tr class="row">
														<td width="127" class="desc">Property Type :</td>
														<td width="283"> {{$property->type->name}}</td>
													</tr>
												@endif

												@if(isset($property->purpose->name))
													<tr class="row">
														<td width="127" class="desc">For :</td>
														<td width="283"> {{$property->purpose->name}}</td>
													</tr>
												@endif

												@if($property->price)
													<tr class="row">
														<td width="127" class="desc">Price :</td>
														<td width="283">Rs. {{$property->price}}</td>
													</tr>
												@endif


												@if($property->title)
													<tr class="row">
														<td width="127" class="desc">Title :</td>
														<td width="283">{{$property->title}}</td>
													</tr>
												@endif

												@if($property->subtitle)

													<tr class="row">
														<td width="127" class="desc">Sub Title :</td>
														<td width="283">{{$property->subtitle}}</td>
													</tr>
												@endif

												@if(isset($property->city->name))
													<tr class="row">
														<td width="127" class="desc">City :</td>
														<td width="283">{{$property->city->name}}</td>
													</tr>
												@endif

												@if($property->address)
													<tr class="row">
														<td width="127" class="desc">Address :</td>
														<td width="283">{{$property->address}}</td>
													</tr>
												@endif

												 @if($property->ropani || $property->aana || $property->paisa)
													<tr class="row">
														<td width="127" class="desc">Area :</td>
														<td width="283">
																@if($property->ropani){{$property->ropani}}
																@endif

																@if($property->aana){{$property->aana}} 
																@endif

																@if($property->paisa){{$property->paisa}} 

																 @endif

																 @if($property->daam){{$property->daam}} 

																 @endif

														</td>
													</tr>
												@endif

												@if($property->no_of_floors)
													<tr class="row">
														<td width="127" class="desc">No of Floors :</td>
														<td width="283">{{$property->no_of_floors}}</td>
													</tr>
												@endif

												@if(isset($property->roadType->name))
													<tr class="row">
														<td width="127" class="desc">Road Type :</td>
														<td width="283">{{$property->roadType->name}}</td>
													</tr>
												@endif

												@if($property->road_size)
													<tr class="row">
														<td width="127" class="desc">Road Size :</td>
														<td width="283">{{$property->road_size}} (ft)</td>
													</tr>
												@endif



											</tbody>
											</table>


											
								</div>

									@if($property->description)

										<div class="item-feature">
											<h4>Description</h4>

																																
											<ul class="clearfix">
													<?php echo $property->description; ?>                                                   
											</ul>

										</div>
									@endif

									<div class="item-feature">
											

											<h4 class="pt-3">Comments <a href="{{ url('/auth/redirect/facebook/'.$property->id) }}" class="btn-comment">Click to Comment</a></h4>


																																
											<div class="comment_content">
												@foreach($comments as $c)
												  <p>{{$c->comment}} @if($c->name)
														By {{$c->name}}
													@endif
												  

												  <?php if(auth::user()){?><a class="reply" style="float:right" onclick="openReplyForm(<?php echo $c->id;?>)">Reply</a></h4> <?php }else{ ?>
													<a class="reply" href="{{url('login')}}" style="float:right">Reply</a>

											<?php } ?>

												</p>

												<p>
													
											<?php	
											$reply = \App\ReplyModel::where('comment_id',$c->id)->get();

													foreach($reply as $r){
														echo $r->reply;
											?>

										<?php } ?>
													 </p>

											

												@endforeach	
											</div>

										</div>

<div id="response_msg" style="color:green">
@if(Session::has('message'))
	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {{session('message')}}</em></div>
@endif
</div>



<?php if($getInfo = \Illuminate\Support\Facades\Session::get('getInfo')){ 
		// dd($getInfo);
	?>
	<!-- Comment Form -->

	<div class="form-popup" id="myForm">
		<form method="post" action="{{url('comment')}}" class="form-container">

			{{csrf_field()}}

		   <input type="hidden" name="property_id" value="{{$property->id}}">
		   <input type="hidden" name="comment_user_id" value="{{$getInfo->id}}">

		   <input type="hidden" name="name" value="{{$getInfo->name}}">

			<label for="comment"><b>Comment *</b></label>
			<textarea name="comment" id="comment" required></textarea>

			<button type="submit" class="btn">Comment</button>
			<!-- <button type="button" class="btn cancel" onclick="closeForm()">Close</button> -->
		</form>
	</div>

<?php } ?>

	<!--close comment form  -->


<!--Reply Form -->

<div class="form-popup" id="replyForm" style="display:none">
	<form method="post" action="{{url('reply')}}" class="form-container">
		{{csrf_field()}}

		<input type="hidden" name="property_id" id="property_id" value="{{$property->id}}">
		<input type="hidden" name="comment_id" id="comment_id">
		
		<h1>Comment Reply</h1>

	   <input type="hidden" name="reply_user_id" value="@if(auth::user()){{auth::user()->id}}@endif">

		<label for="reply"><b>Reply *</b></label>
		<textarea name="reply" id="reply" required></textarea>

		<button type="submit" class="btn">Reply</button>
		<button type="button" class="btn cancel" onclick="closeReplyForm()">Close</button>
	</form>
</div>



<!--Close Reply Form -->




								</div>

									

									<div class="tab-pane fade map-street" id="map-street" role="tabpanel" aria-labelledby="map-street-tab">
										<div class="nearest-place">
											<h4>Nearest General Places</h4>
											<ul>
											   
												<?php
												if($property['near_hospital']):
													?>
													<li><span><i class="fa fa-hospital-o"></i> <b>Near Hospital </b> </span></li>
												<?php endif; ?>

												<?php
												if($property['near_school']):
													?>
													<li><span><i class="fa fa-graduation-cap"></i> <b>Near School </b></span></li>
												<?php endif; ?>
												<?php
												if($property['near_market']):
													?>
													<li><span><i class="fa fa-shopping-bag"></i> <b>Near Market</b> </span></li>
												<?php endif; ?>
												<?php
												if($property['near_bank']):
													?>
													<li><span><i class="fa fa-bank"></i> <b>Near Bank </b></span></li>
												<?php endif; ?>
												<?php
												if($property['near_bus_stop']):
													?>
													<li><span><i class="fa fa-bus"></i><b> Near Bus Station </b></span></li>
												<?php endif; ?>
												
												<?php
												if($property['near_airport']):
													?>
													<li><span><i class="fa fa-plane"></i> <b>Near Airport </b></span></li>
												<?php endif; ?>
											</ul>
										</div><!--end .nearest-place-->

													<div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d226659.20929933494!2d<?php echo $property['longitude'];?>!3d<?php echo $property['latitude'];?>!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb497ced46c917%3A0xafb8902c7a4532ab!2s<?php echo $property['address'];?>!5e0!3m2!1sen!2snp!4v1606118056031!5m2!1sen!2snp" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
									  
									</div>


									<div id="video" class="tab-pane fade video">
										<div class="nearest-place">
											<h4>Property Video</h4>
											
										</div><!--end .nearest-place-->

													<div id="video-box">
														
														<iframe src="{{$property->video_url}}" allowfullscreen>
</iframe>
													</div>
									  
									</div>

								
							</div>



						</article>

					   
					</div><!--end .col-md-8-->

				
					<aside class="col-md-4 main-sidebar">
						<div class="widget property-info">
							<div class="inner">
								@if($agent!= null)
								<div class="text-center">
									<p>This property is marketed by: <b><?php echo $agent['name'];   ?></b></p>								   

								</div>
								<ul>
								<table id="dataTable">
									<thead>
									</thead>
									<tbody>

										<li class="request-details">
											<!--<a href="request-details.html" class="btn-button">Request Details</a>-->
											@if($agent['contact_num']) 
										<tr>
											<td width="127"><p>Call: </p></td>
											<td><p><a href="tel:<?php echo $agent['contact_num'];  ?>"><?php echo $agent['contact_num'];  ?></a></p>
											</td>
										</tr>
											@endif

											@if($agent['email'])
										<tr>
											<td><p> Email:</p></td>
											<td><p><?php echo $agent['email'];  ?>      </p>
										</tr>
											@endif

											@if($agent['contact_name'])
										<tr>
											<td><p> Contact Name:</p></td>
											 <td><p><?php echo $agent['contact_name'];  ?></p>
												</td>
										</tr>
											@endif

											@if($agent['address'])
										<tr>
											<td><p> Address:</p></td>
											<td><p><?php echo $agent['address'];  ?></p>
											</td>
										</tr>
											@endif


											@if($agent['company_name'])
										<tr>
											<td><p> Company:</p></td>
											<td><p><?php echo $agent['company_name'];  ?></p>
											</td>
										</tr>
											@endif

											@if($agent['company_registration_num'])
										<tr>
											<td><p> Company Reg. No.:</p></td>
											<td><p><?php echo $agent['company_registration_num'];  ?></p>
											</td>
										</tr>
											@endif
										</li>
									</tbody>
								</table>
								@endif
									<!--<li><a href=""> <i class="fa fa-heart-o"></i>Save property</a></li>-->
								   
									<!--<li><a href="JavaScript:Void(0);" onclick="myFunction('print-content'); return false"><i class="fa fa-print"></i>Print</a> </li> -->

									<!-- <li><a href="#" data-toggle="modal" data-target="#sendFriendModal"><i class="fa fa-send-o"></i>Send to Friend</a></li>
									<li> -->
									
										<div class="disclaimer-content">
											<h4 style="color:red">Beware of fraud & scams</h4>

																																
											<ul class="clearfix">
											Ghar Gharana is not involved in transaction of any goods/services listed in the website. It is only platform to share information. You are directly contacting the person who has posted the advertisement and you agree not to hold Ghargharana.com responsible for their act in any circumstances. We strongly encourage you to take necessary precaution. 
											</ul>

										</div>
									
									</li>
								</ul>
								
								@include('includes.sendToFriend')
							</div><!--end .inner-->
						</div>
						@if(isset($adTop))
							<div class="widget-ads">
								<a href="{{$adTop['link']}}" target="_blank"><img src="{{asset('uploads/ad/'.$adTop['image'])}}" alt=""/></a>
							</div>
						@endif

						<div class="widget-similar">
							<aside class="recommended-properties">
								<h3>Similar Properties</h3>
								@foreach($similar as $s)
									<div class="item">
										<div class="item-img">
											<a href="{{url('property/description/'.$s->id)}}"><img src="{{asset('uploads/property/'.$s->image)}}" alt="{{$s->title}}"></a>
											<div class="sim-property-title"><a href="{{url('property/description/'.$s->id)}}"> {{$s->title}} </a></div>
										</div><!--end .item-img-->
										<div class="item-content">
											<p><span>Rs. {{$s->price}}</span><span class="location">@if(isset($s->city->name))
																 
																	
																		{{ucfirst($s->city->name)}}
																	
															@endif</span></p>
										</div><!--end .item-content-->
									</div><!--end .item-->
								@endforeach
								

							</aside>
						</div>
						@if(isset($adButtom))
							<div class="widget-ads">
								<a href="{{$adButtom['link']}}" target="_blank"><img src="{{asset('uploads/ad/'.$adButtom['image'])}}" alt=""/></a>
							</div>
						@endif

					</aside><!--end .col-md-4-->
				</div><!--end .row-->
				
			</div><!--end .container-->
		</div><!--end .inner-content-->
	</div><!--end .main-content-->

<script>
//comment
function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

//reply 

function openReplyForm(id) {
  document.getElementById("replyForm").style.display = "block";
  $('#comment_id').val(id);
}

function closeReplyForm() {
  document.getElementById("replyForm").style.display = "none";
}
</script>

@include('includes.footer')