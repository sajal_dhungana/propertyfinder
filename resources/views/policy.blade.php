@include('includes.header')


<div class="main-content" id="main-content">
	<div class="inner-content">
        
		<div class="container">
			<div class="row">
                <div class="col-md-12">
                <h1 class="subpage-main-title">Privacy Policy</h1>
                </div>
            </div>
            <div class="row">
				<div class="col-md-8 col-sm-12">
                    <div class="policy-wrap">
                        <div class="policy-title">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                        <div class="policy-desc">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</div>
                        <ul>
                            <li>Contrary to popular belief, Lorem Ipsum is not simply random text.</li>
                            <li>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</li>
                            <li>the leap into electronic typesetting, remaining essentially unchanged.</li>
                            <li>All the Lorem Ipsum generators on the Internet tend to repeat</li>
                            <li>Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc</li>
                        </ul>
                    </div>
                </div>
                <aside class="col-md-4 main-sidebar">
						<div class="widget property-info">
							<div class="inner">
                                <h2 class="title2">Can't find what you're looking for ?</h2>
                                <div class="help-par">
                                    Review our help topics or contact us for your intrigued queries. 
                                </div>
                                <div class="help-btn">
                                    <a class="contact" href="{{url('contact')}}">contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</div>

@include('includes.footer')