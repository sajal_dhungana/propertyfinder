@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($icon);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/admin/icon/create')}}">Add New</a>
</div>
<div class="data-table">
    <table id="dataTable">
        <thead>
            <tr>
            <!-- <th class="id"><input type="checkbox"></th> -->
            <th>Icon</th>

            <th class="action">Action</th>
            <!-- <th class="id">Id</th> -->

            </tr>
        </thead>
        <tbody>


            @foreach($icon as $i)

            <tr>
                <!-- <td><input type="checkbox"/></td> -->
                <td>{{$i->name}}</td>
                <td><a href="{{url('admin/icon/edit').'/'.$i->id}}">Edit</a> <a href="{{url('admin/icon/delete').'/'.$i->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
            </tr>



            @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
