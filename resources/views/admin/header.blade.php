<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php if(isset($_GET['action'])): echo ucwords($_GET['action']); else: echo "HOME"; endif; ?> | ADMIN</title>
<link rel="shortcut icon" href="{{asset('images/ghar-logo.png')}}">
<link rel="stylesheet" href="{{asset('dashboard/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/css/defult.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/css/style.css')}}">


<link rel="stylesheet" href="{{asset('dashboard/dataTable/media/css/custom.dataTable.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/media/font-awesome/css/font-awesome.min.css')}}">

<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link type="text/css" rel="stylesheet" href="{{asset('css/image-uploader.min.css')}}">



</head>

<body>
     <?php 
        $notification = \App\AdminNotificationModel::all();
        $nCount = count($notification);
    ?>
<div id="wrapper">
	<div id="top_nap">
    	<div class="top_left"><p><a href="{{url('/')}}"><i class="fa fa-home">&nbsp;&nbsp;</i><img src="{{asset('images/ghar-logo.png')}}" alt="">&nbsp;&nbsp; Admin Panel</a></p></div>
        <div class="top_center"><p><i class="fa fa-clock-o ">&nbsp;&nbsp;&nbsp;</i><i id="time"></i></p></div>
        <div class="top_right">
            <ul>
                <li>
                    <a href="{{url('admin/notification/list')}}"><span class="messegeNo"><i class="fa fa-envelope-o" aria-hidden="true"></i><span>{{$nCount}}</span></span></a>
                </li>
                <li>    
                    
                             
                            @if(\Auth::guard('admin'))
                                        <a><strong>{{\Auth::guard('admin')->user()->name}}</strong><span class="messegeNo"></span></a>
                            @endif
                                
                       
                    <ul>
                        <li>
                             <a href="{{ route('admin-logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form-3').submit();">
                                                <i class="fa fa-power-off" aria-hidden="true">&nbsp;</i> Logout
                                            </a>

                                            <form id="logout-form-3" action="{{ route('admin-logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                        </li>

                        <li>
                            <a href="{{url('admin/password/edit').'/'.Auth::guard('admin')->user()->id}}"><i class="fa fa-gears">&nbsp;</i> Change Password </a>
                        </li>
                    </ul>
                </li>

                
            </ul>

        </div>
  </div><!-- div id top nap closed -->