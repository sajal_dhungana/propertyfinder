@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/district/create')}}" enctype="multipart/form-data">

{{csrf_field()}}
      <fieldset>
        <legend>Add District</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="name"  placeholder="Type name here" required/>
                  <span></span>
              </div>
          </div>

          <!-- <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Country Name *</label>
              </div>
              <div class="view_elements">
                  <select name="country_id" id="country_id" required onchange="getState()">
                        <option value="">--Select Country--</option>

                        @foreach($country as $c)

                                <option value="{{$c->id}}">{{$c->name}}</option>
                        @endforeach

                  </select>
                  <span></span>
              </div>
          </div> -->

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> State Name *</label>
              </div>
              <div class="view_elements">
                  <select name="state_id" id="state_id" required>
                        <option value="">--Select State--</option>
                    @foreach($state as $s)
                    <option value="{{$s->id}}">{{$s->name}}</option>
                    @endforeach

                  </select>
                  <span></span>
              </div>
          </div>

          
          
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                 
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>

<script>
    function getState(){
        $('#state_id').html('<option value="">--Select State--</option>');
        var country = document.getElementById('country_id').value;
        $.ajax({
        url: "{{url('getState')}}"+'/'+country,
        type: 'GET',
        success: function(res) {
            $('#state_id').html(res);
        }
    });
        
    }
</script>



@include('admin.footer')