@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($faq);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/admin/faq/create')}}">Add New</a>
</div>

<div class="data-table">
<table id="dataTable">
    <thead>
        <tr>
            <!-- <th class="id"><input type="checkbox"></th> -->
            <th width="25%">Question</th>
            <th>Answer</th>
            <th class="action">Action</th>
            <!-- <th class="id">Id</th> -->

        </tr>
    </thead>
    <tbody>


        @foreach($faq as $f)

        <tr>
            <!-- <td><input type="checkbox"/></td> -->
            <td>{{substr($f->question,0,200)}}</td>
            <td>{{substr($f->answer,0,200)}}</td>

            <td><a href="{{url('admin/faq/edit').'/'.$f->id}}">Edit</a> <a href="{{url('admin/faq/delete').'/'.$f->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
        </tr>



        @endforeach

    </tbody>
</table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
