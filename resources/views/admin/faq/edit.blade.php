@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/faq/update')}}">

{{csrf_field()}}
              <input type="hidden" name="id" value="{{$faq->id}}">
      <fieldset>
        <legend>Edit FAQ</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Question *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="question" value="{{$faq->question}}"  placeholder="Type question here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Answer *</label>
              </div>
              <div class="view_elements">
                  <textarea name="answer" placeholder="Type answer here" rows="4" required>{{$faq->answer}}</textarea>
                  <span></span>
              </div>
          </div>

          
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel"/>
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>




@include('admin.footer')