@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/roadSize/update')}}" enctype="multipart/form-data">

{{csrf_field()}}
        <input type="hidden" name="id" value="{{$size['id']}}">
      <fieldset>
        <legend>Edit Road Size</legend>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Size *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="name" step="0.001" value="{{$size['name']}}"  placeholder="Type size here" required/> <label for="form_name" style="color:black"> Feet</label>
                  <span></span>
              </div>
          </div>
         
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1" <?php if($size['status'] == 1){echo 'selected';}?>>Active</option>
                            <option value="0" <?php if($size['status'] == 0){echo 'selected';}?>>Inactive</option>
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>



@include('admin.footer')