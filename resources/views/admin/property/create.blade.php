@include('admin.header')
<script src="{{asset('dashboard/js/jquery-1.11.2.min.js')}}"></script>
<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->

<script>
      $( document ).ready(function() {
          // $('.input-images').imageUploader();
          $('.input-images-1').imageUploader();
      });
      
</script>

<div id="respons_section">

<div class="form">
<form method="post" name="form-example-1" id="form-example-1" action="{{url('admin/property/create')}}" enctype="multipart/form-data">

{{csrf_field()}}
      <fieldset>
        <legend>Add Property</legend>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Title *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="title"  placeholder="Type title here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Sub Title *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="subtitle"  placeholder="Type Sub Title here" required/>
                  <span></span>
              </div>
          </div>


            <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Image </label>
              </div>
              <div class="view_elements">
                  <input type="file" name="image" id="image"  placeholder="Upload Image"/>
                  <span></span>
              </div>
          </div>


             <div class="input-images">
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Property Gallary </label>
              </div>
              <div class="view_elements">
                  <div class="input-images-1" style="padding-top: .5rem;"></div>
                  <span></span>
              </div>
          </div>
        </div>
          
  
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Agent Name *</label>
              </div>
              <div class="view_elements">
                  <select name="agent_id" required>
                            <option value="">--Select Agent--</option>
                       @foreach($user as $u) 
                            <option value="{{$u->id}}">{{$u->name}}</option>
                        @endforeach
                  </select>
                  <span></span>
              </div>
          </div>

          

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">City Name *</label>
              </div>
              <div class="view_elements">
                  <select name="city_id" id="city_id" required>
                            <option value="">--Select City--</option>

                            @foreach($city as $c)

                        <option value="{{$c->id}}">{{$c->name}}</option>
                      @endforeach
                       
                  </select>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Property Type *</label>
              </div>
              <div class="view_elements">
                  <select name="property_type_id" id="property_type_id" onchange="getPropertyType(this)" required>
                            <option value="">--Select Property Type--</option>

                        @foreach($type as $t)
                            <option value="{{$t->id}}" title="{{$t->name}}">{{$t->name}}</option>
                        @endforeach
                       
                  </select>
                  <span></span>
              </div> 
          </div>

          <div class="view_items" id="area" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> Area </label>
                        <div class="col-md-12">
                            <input type="number" placeholder="Ropani" name="ropani">
                            <input type="number" placeholder="Aana" name="aana">
                            <input type="number" placeholder="Paisa" name="paisa">
                            <input type="number" placeholder="Daam" name="daam">
                        </div>

                </div>
            </div>

          <div class="view_items" id="rooms" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Rooms </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_rooms" id="no_of_rooms" placeholder="Type No. of Rooms here" min="1"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items" id="bedrooms" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Bedrooms </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_bedrooms" id="no_of_bedrooms" placeholder="Type No. of Bedrooms here" min="1"/>
                  <span></span>
              </div>
          </div>


          <div class="view_items" id="bathrooms" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Bathrooms </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_bathrooms" id="no_of_bathrooms" placeholder="Type No. of Bathrooms here" min="1"/>
                  <span></span>
              </div>
          </div>

          
          <div class="view_items" id="kitchens" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Kitchens </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_kitchens" id="no_of_kitchens"  placeholder="Type No. of Kitchens here" min="1"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items" id="living_rooms" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Living Rooms </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_living_rooms" id="no_of_living_rooms"  placeholder="Type No. of Living Rooms here" min="1"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items" id="floors" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Floors </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_floors" id="no_of_floors"  placeholder="Type No. of Floors here" min="1"/>
                  <span></span>
              </div>
          </div>

          
          <div class="view_items" style="color:black">
              <div class="view_labels">
                  <label for="form_name"> Road Size *</label>
              </div>

              <div class="view_elements">
                     <select name="road_size_id" id="road_size_id" required>
                            <option value="">--Select Road Size--</option>
                        @foreach($roadSize as $size)
                            <option value="{{$size->id}}">{{$size->name}}</option>
                        @endforeach
                       
                  </select>
            </div>
          </div>

          
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Road Type *</label>
              </div>
              <div class="view_elements">
                  <select name="road_type_id" id="road_type_id" required>
                            <option value="">--Select Road Type--</option>

                        @foreach($roadType as $rt)
                            <option value="{{$rt->id}}">{{$rt->name}}</option>
                        @endforeach
                       
                  </select>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Property Purpose *</label>
              </div>
              <div class="view_elements">
                  <select name="property_purpose_id" id="property_purpose_id" required>
                    <option value="">--Select Property Purpose--</option>
                    @foreach($purpose as $p)
                            
                            <option value="{{$p->id}}">{{$p->name}}</option>

                    @endforeach

                        
                       
                  </select>
                  <span></span>
              </div>
          </div>
      
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Price *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="price"  placeholder="Type Price here" min="1" required/>
                  <span></span>
              </div>
          </div>


          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_hospital"/> Near Hospital </label>
                  
              </div>
              
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_school" /> Near School </label>
              </div>
             
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_market"/> Near Market </label>
              </div>
         
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_bank"/> Near Bank </label>
              </div>
          
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_bus_stop"/> Near Bus Stop </label>
              </div>
              
          </div>


          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_airport" /> Near Airport </label>
              </div>
              
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Latitude *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="latitude" step='0.00001' placeholder="Type Latitude here" min="1" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Longitude *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="longitude" step='0.00001' placeholder="Type Longitude here" min="1" required/>
                  <span></span>
              </div>
          </div>



          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                  </select>
                  <span></span>
              </div>
          </div>


          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>

<script>
    function getCity(){
                $('#city_id').html('<option value="">--Select City--</option>');
                var country = document.getElementById('country_id').value;
                $.ajax({
                url: "{{url('getCity')}}"+'/'+country,
                type: 'GET',
                success: function(res) {
                    $('#city_id').html(res);
                }
            });
        
    }

    function getPropertyType(a){
        var type = $('option:selected', a).attr('title');
        if(type == 'house'){
            $('#rooms').show();
            $('#bedrooms').show();
            $('#bathrooms').show();
            $('#kitchens').show();
            $('#living_rooms').show();
            $('#floors').show();
            $('#area').show();

        }else if(type == 'room'){
            $('#rooms').show();
            $('#bedrooms').show();
            $('#bathrooms').show();
            $('#kitchens').show();
            $('#living_rooms').show();

        }else if(type == 'flat'){
            $('#rooms').show();
            $('#bedrooms').show();
            $('#bathrooms').show();
            $('#kitchens').show();
            $('#living_rooms').show();
            $('#area').show();
           
        }else{
            $('#rooms').hide();
            $('#bedrooms').hide();
            $('#bathrooms').hide();
            $('#kitchens').hide();
            $('#living_rooms').hide();
            $('#floors').hide();
            $('#area').show();
        }

        
    }
</script>

@include('admin.footer')