@include('admin.header')

<div id="activity_section" class="agent-profile">
	<div id="left_nav">
		@include('admin.leftnav')
	</div><!-- end #left_nav -->
	<div id="respons_section">
		<div class="form">
			
		</div>
        
    </div>
    <div class="dash-right">
        <div class="data-table">
            <div class="property-titlebar">
                <div class="property-price">Rs. {{$property->price}}</div>
                <div class="property-title-wrap">
                    <div class="property-title">{{$property->title}}</div>
                    <div class="property-purpose">
                       <?php if(isset($property->purpose->name)){?> for <span class="property-type"><?php echo ucfirst($property->purpose->name); }?>
                           
                       </span> in <span class="location">
                            @if($property->address)
                                {{$property->address}}
                            @endif

                            @if(isset($property->city->name))
                                {{ucfirst($property->city->name)}}
                            @endif
                            </span>
                    </div>
                    
                </div>
                <div class="property-count">
                        Views
                        <span class="views-count">{{$property->views}}</span>
                    </div>
            </div>

            <?php $i = 1; ?>
            <div class="property-det-wrap">
                <div class="property-img">
                <!-- Images used to open the lightbox -->
                    <div class="outerImg">

                        @foreach($gallary as $g)
                            <img src="{{asset('uploads/property/'.$g->image)}}" onclick="openModal();currentSlide(<?php echo $i;?>)" class="hover-shadow">
                        
                           <?php $i++;?>
                        @endforeach
                        
                    </div>
                    <div class="numPhotos">{{$count}} Photos</div>

                    <?php $j = 1; ?>
                    <!-- The Modal/Lightbox -->
                    <div id="propertyImgModal" class="modal">
                        <span class="close cursor" onclick="closeModal()">&times;</span>
                        <div class="modal-content">
                        @foreach($gallary as $g)
                            <div class="propertyImgSlides">
                                <div class="numbertext">{{$j}} / {{$count}}</div>
                                <img src="{{asset('uploads/property/'.$g->image)}}" style="width:100%">
                            </div>
                            <?php $j++;?>
                        @endforeach

                            <!-- Next/previous controls -->
                            <a class="prev" onclick="plusSlides(-1)"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                            <a class="next" onclick="plusSlides(1)"><i class="fa fa-angle-right" aria-hidden="true"></i></a>

            

                            <!-- Thumbnail image controls -->
                            <div class="thumb-wrap">
                                <?php $k = 1;?>
                            @foreach($gallary as $g)
                                <img class="demo" src="{{asset('uploads/property/'.$g->image)}}" onclick="currentSlide(<?php echo $k;?>)" alt="Nature">
                                <?php $k++;?>
                            @endforeach
                            
                            </div>
                        </div>
                    </div>
                    
                    
                </div>

                <div class="property-det">
                    <ul>

                    @if($property['property_type_id'])
                        <?php $feature = App\PropertyFeatureModel::where('property_type_id',$property['property_type_id'])->get()->toArray();

                        if($feature){
                            $feature = $feature[0];
                            $name = $feature['name'];

                            $feature_list = explode(',',$name);
                                                    
                            foreach($feature_list as $list){
                                $f = App\FeatureModel::where('id',$list)->get();

                                    if($f){
                                        $f = $f[0];

                        $bed = similar_text($f->name,'bedrooms');

                        $bath = similar_text($f->name,'bathrooms');

                        $kitchen = similar_text($f->name,'kitchen');

                        $living = similar_text($f->name,'livingroom');
                        ?>
                        

                        @if($bed>$bath && $bed>$kitchen && $bed>$living)
                            @if($property['no_of_bedrooms'])
                                    <li>{{$f->name}}<span>
                                
                                {{$property['no_of_bedrooms']}}
                            @endif
                        @endif

                        @if($bath>$bed && $bath>$kitchen && $bath>$living)
                            @if($property['no_of_bathrooms'])
                                    <li>{{$f->name}}<span>
                            
                                {{$property['no_of_bathrooms']}}
                            @endif
                        @endif

                        @if($kitchen>$bath && $kitchen>$bed && $kitchen>$living)
                            @if($property['no_of_kitchens'])
                                    <li>{{$f->name}}<span>
                        
                                {{$property['no_of_kitchens']}}
                            @endif
                        @endif

                        @if($living>$bath && $living>$bed && $living>$kitchen)
                            @if($property['no_of_living_rooms'])
                                    <li>{{$f->name}}<span>
                        
                                {{$property['no_of_living_rooms']}}
                            @endif
                        @endif
                        </span></li>

                            <?php   }
                            }
                        }
                         ?>
                    @endif

                    @if($property->near_hospital)
                                             <li>Near Hospital                   @if($property->hospital_name)
                                                    - {{$property->hospital_name}}
                                                 @endif
                                             </li>


                                           @endif             

                                            @if($property->near_school)
                                                <li>Near School
                                                    @if($property->school_name)
                                                    - {{$property->school_name}}
                                                    @endif
                                                </li>
                                            @endif

                                            @if($property->near_market)
                                                <li>Near Market
                                                    @if($property->market_name)
                                                    - {{$property->market_name}}
                                                    @endif
                                                </li>
                                            @endif

                                            @if($property->near_bank)
                                                <li>Near Bank
                                                    @if($property->bank_name)
                                                    - {{$property->bank_name}}
                                                    @endif
                                                </li>

                                            @endif

                                            @if($property->near_bus_stop)
                                                <li>Near Bus Stop
                                                    @if($property->bus_stop_name)
                                                    - {{$property->bus_stop_name}}
                                                    @endif
                                                </li>
                                            @endif

                                            @if($property->near_airport)
                                                <li>Near Airport
                                                    @if($property->airport_name)
                                                    - {{$property->airport_name}}
                                                    @endif
                                                </li>
                                            @endif

                                            @if(isset($property->roadType->name))
                                                <li>Road Type<span> 
                                                        {{$property->roadType->name}}
                                                    </span>
                                                </li>
                                                    
                                                @endif

                                                @if($property->road_size)
                                                    <li>Road Size<span>
                                                    {{$property->road_size}} (ft)
                                                </span>
                                            </li>
                                                @endif
                    

                            
                        
                    </ul>
                </div>
            </div>
        @if($property->description)
            <div class="property-desc">
                <h2 class="property-desc-title">Description</h2>
                <p>{{$property->description}}</p>
            </div>
        @endif

        </div>
    </div>
</div><!-- end #activity_section -->
@include('admin.footer')