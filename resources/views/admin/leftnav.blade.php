 <ul class="sidebar-menu">
 	<!-- <li class="active">
        <a href="{{url('admin/dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Admin Home</span>
        </a>
    </li> -->

    <li class="treeview">
    	<a href="" ><i class="fa fa-bars"></i> <span>MANAGE DATA</span><i class="fa fa-angle-left pull-right"></i> </a>
        <ul class="treeview-menu treeview-menu">
        	<li>
                <a href="#"><i class="fa fa-angle-double-right"></i> Agency </a>
                <ul class="type">
                    <li><a href="{{url('admin/agency/create')}}"><i class="fa fa-angle-double-right"></i> Add New </a></li>
                    <li><a href="{{url('admin/agency/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
                </ul>
            </li>

             <li>
                <a href="#"><i class="fa fa-angle-double-right"></i> Advertisements </a>
                <ul class="type">
                    <li><a href="{{url('admin/ad/create')}}"><i class="fa fa-angle-double-right"></i> Add New </a></li>
                    <li><a href="{{url('admin/ad/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-angle-double-right"></i> State </a>
                <ul class="type">
                    <li><a href="{{url('admin/state/create')}}"><i class="fa fa-angle-double-right"></i> Add New </a></li>
                    <li><a href="{{url('admin/state/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
                </ul>
            </li>
            

            <li>
                <a href="#"><i class="fa fa-angle-double-right"></i> District </a>
                <ul class="type">
                    <li><a href="{{url('admin/district/create')}}"><i class="fa fa-angle-double-right"></i> Add New </a></li>
                    <li><a href="{{url('admin/district/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-angle-double-right"></i> City </a>
                <ul class="type">
                    <li><a href="{{url('admin/city/create')}}"><i class="fa fa-angle-double-right"></i> Add New </a></li>
                    <li><a href="{{url('admin/city/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
                </ul>
            </li>

            <li class="single_view">
                <a href="#">
                    <i class="fa fa-gears"></i> <span>Road Type</span>
                </a>

                <ul class="type">
                <li><a href="{{url('admin/roadType/create')}}"  ><i class="fa fa-angle-double-right"></i> Add New </a></li>
                <li><a href="{{url('admin/roadType/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
                </ul>
            </li>

      <!--   <li class="single_view">
            <a href="#">
                <i class="fa fa-gears"></i> <span>Road Size</span>
            </a>

            <ul class="type">
                 <li><a href="{{url('admin/roadSize/list')}}"  ><i class="fa fa-angle-double-right"></i> Add New </a></li>
                 <li><a href="{{url('admin/roadSize/list')}}"><i class="fa fa-angle-double-right"></i> View All </a>
                 </li>
            </ul>
        </li> -->

        <li class="single_view">
            <a href="#">
                <i class="fa fa-gears"></i> <span>Feature</span>
            </a>

            <ul class="type">
             <li><a href="{{url('admin/feature/create')}}"  ><i class="fa fa-angle-double-right"></i> Add New </a></li>
             <li><a href="{{url('admin/feature/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
         </ul>
        </li>

            <li>
                <a href="#"><i class="fa fa-angle-double-right"></i> Property Types </a>
                <ul class="type">
                    <li><a href="{{url('admin/propertyType/create')}}"><i class="fa fa-angle-double-right"></i> Add New </a></li>
                    <li><a href="{{url('admin/propertyType/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
                </ul>
            </li>


            <li>
                <a href="#"><i class="fa fa-angle-double-right"></i> Property Features </a>
                <ul class="type">
                    <li><a href="{{url('admin/propertyFeature/create')}}"><i class="fa fa-angle-double-right"></i> Add New </a></li>
                    <li><a href="{{url('admin/propertyFeature/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-angle-double-right"></i> Features Icon </a>
                <ul class="type">
                    <li><a href="{{url('admin/icon/create')}}"><i class="fa fa-angle-double-right"></i> Add New </a></li>
                    <li><a href="{{url('admin/icon/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
                </ul>
            </li>


            <li>
                <a href="#"><i class="fa fa-angle-double-right"></i> Property </a>
                <ul class="type">
                    
                    <!-- <li><a href="{{url('admin/property/create')}}"><i class="fa fa-angle-double-right"></i> Add New </a></li> -->

                    <li><a href="{{url('admin/property/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
                </ul>
            </li>



            <li class="single_view">
            <a href="#">
                <i class="fa fa-gears"></i> <span>Min-Max Price</span>
            </a>

            <ul class="type">
             <li><a href="{{url('admin/price/create')}}"  ><i class="fa fa-angle-double-right"></i> Add New </a></li>
             <li><a href="{{url('admin/price/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
         </ul>
        </li>

         <li class="single_view">
            <a href="#">
                <i class="fa fa-gears"></i> <span>Plan</span>
            </a>

            <ul class="type">
             <li><a href="{{url('admin/plan/create')}}"  ><i class="fa fa-angle-double-right"></i> Add New </a></li>
             <li><a href="{{url('admin/plan/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
         </ul>
        </li>

        <li class="single_view">
            <a href="#">
                <i class="fa fa-gears"></i> <span>Plan Feature</span>
            </a>

            <ul class="type">
             <li><a href="{{url('admin/plan/feature/create')}}"  ><i class="fa fa-angle-double-right"></i> Add New </a></li>
             <li><a href="{{url('admin/plan/feature/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
         </ul>
        </li>

        <li class="single_view">
            <a href="#">
                <i class="fa fa-gears"></i> <span>Features to Plan</span>
            </a>

            <ul class="type">
             <li><a href="{{url('admin/plans/features/create')}}"  ><i class="fa fa-angle-double-right"></i> Add New </a></li>
             <li><a href="{{url('admin/plans/features/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
         </ul>
        </li>

        <li class="single_view">
           <a href="#">
               <i class="fa fa-ship"></i> <span>Applicants</span>
           </a>
           <ul class="type">
               <li><a href="{{url('admin/applicants/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
           </ul>
        </li>

         <li class="single_view">
           <a href="#">
               <i class="fa fa-ship"></i> <span>Blogs</span>
           </a>
           <ul class="type">
              <li><a href="{{url('admin/blogs/create')}}"><i class="fa fa-angle-double-right"></i> Add New</a></li>

               <li><a href="{{url('admin/blogs/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
           </ul>
        </li>


     <li class="single_view">
         <a href="#">
             <i class="fa fa-ship"></i> <span>Agents</span>
         </a>
         <ul class="type">
            
            <!--  <li><a href="{{url('admin/agent/create')}}"><i class="fa fa-angle-double-right"></i> Add New </a></li> -->

             <li><a href="{{url('admin/agent/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
         </ul>
     </li>

            <li class="single_view">
                <a href="#"><i class="fa fa-angle-double-right"></i> <span>FAQ </span></a>
                <ul class="type">
                    <li><a href="{{url('admin/faq/create')}}"><i class="fa fa-angle-double-right"></i> Add New </a></li>
                    <li><a href="{{url('admin/faq/list')}}"><i class="fa fa-angle-double-right"></i> View All </a></li>
                </ul>
            </li>
     
        <li class="single_view">
            <a href="#">
                <i class="fa fa-gears"></i> <span>Setting</span>
            </a>

            <ul class="type">
             <li><a href="{{url('admin/general-setting/list')}}"  ><i class="fa fa-angle-double-right"></i> General Setting </a></li>
             <li><a href="{{url('admin/social-media-setting/list')}}"><i class="fa fa-angle-double-right"></i> Social Media Setting </a></li>
         </ul>
        </li>
    </ul>
</li>
</ul>