@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/city/create')}}" enctype="multipart/form-data">

{{csrf_field()}}
      <fieldset>
        <legend>Add City</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="name"  placeholder="Type name here" required/>
                  <span></span>
              </div>
          </div>

          

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> District Name *</label>
              </div>
              <div class="view_elements">
                  <select name="district_id" id="district_id" required>
                        <option value="">--Select District--</option>
                    @foreach($district as $d)
                    <option value="{{$d->id}}">{{$d->name}}</option>
                    @endforeach
                  </select>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                 
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel"/>
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>

<script>
    function getDistrict(){
        $('#district_id').html('<option value="">--Select District--</option>');
        var country = document.getElementById('country_id').value;
        $.ajax({
        url: "{{url('getDistrict')}}"+'/'+country,
        type: 'GET',
        success: function(res) {
            $('#district_id').html(res);
        }
    });
        
    }
</script>



@include('admin.footer')