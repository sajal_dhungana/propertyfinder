@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($city);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/admin/city/create')}}">Add New</a>
</div>

<div class="data-table">
    <table id="dataTable">
        <thead>
            <tr>
                <!-- <th class="id"><input type="checkbox"></th> -->
                <th>City</th>

                <th>District</th>
                <th class="action">Action</th>
                <!-- <th class="id">Id</th> -->

            </tr>
        </thead>
        <tbody>


            @foreach($city as $c)

            <tr>
            <!-- <td><input type="checkbox"/></td> -->
            <td>{{$c->name}}</td>

            <td>{{isset($c->district->name)?$c->district->name:''}}</td>
            <td><a href="{{url('admin/city/edit').'/'.$c->id}}">Edit</a> <a href="{{url('admin/city/delete').'/'.$c->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
            </tr>



            @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
