@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/city/update')}}" enctype="multipart/form-data">

{{csrf_field()}}
            <input type="hidden" name="id" value="{{$city['id']}}">
      <fieldset>
        <legend>Edit City</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="name"  placeholder="Type name here" value="{{$city['name']}}" required/>
                  <span></span>
              </div>
          </div>

          <!-- <div class="view_items">
              <div class="view_labels">
                  <label for="country_id"> Country Name *</label>
              </div>
              
              <div class="view_elements">
                  <select name="country_id" id="country_id" required onchange="getDistrict()">
                            <option value="">--Select Country--</option>

                            @foreach($country as $c)

                                 <option value="{{$c->id}}"  <?php //if($c->id == $city['country_id']){echo 'selected';}?>>{{$c->name}}</option>
                            @endforeach
                  </select>
                  <span></span>
              </div>

          </div> -->

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> District Name *</label>
              </div>
              <div class="view_elements">
                  <select name="district_id" id="district_id" required>
                        <option value="">--Select District--</option>
                        @foreach($district as $d)
                                <option value="{{$d->id}}" <?php if($d->id == $city['district_id']){echo 'selected';}?>>{{$d->name}}</option>
                        @endforeach
                  </select>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1" <?php if($city['status'] == 1){echo 'selected';}?>>Active</option>
                            <option value="0" <?php if($city['status'] == 0){echo 'selected';}?>>Inactive</option>
                 
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel"/>
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>

<script>
    function getDistrict(){
        $('#district_id').html('<option value="">--Select District--</option>');
        var country = document.getElementById('country_id').value;
        $.ajax({
        url: "{{url('getDistrict')}}"+'/'+country,
        type: 'GET',
        success: function(res) {
            $('#district_id').html(res);
        }
    });
        
    }
</script>



@include('admin.footer')