<div id="mainContent_response">
    <div id="response_nav">
        <?php
        if (isset($_GET['action'])):
            $action = $_GET['action'];
            $action = explode('-', $action);
            $action = end($action);
            ?>
            <ul>
                <li><a href="<?php echo CMS_URL; ?>">Home <i class="fa fa-angle-double-right"></i> </a></li>
                <li><a href="list-<?php echo $action; ?>"><?php echo "List " . $action;?> <i class="fa fa-angle-double-right"></i>
                    </a></li>
                <!--<li><a href="javascript:void(0)"><?php /*echo "List " . $action; */?> <i class="fa fa-angle-double-right"></i>
                    </a></li>-->
            </ul>
            <?php
        endif;//if(isset($_GET['action'])):
        ?>

    </div><!-- end #response_nav -->
    <div id="response_msg">
        <?php
        if (isset($_GET['msg'])):
            if ($_GET['msg'] == 'success'):
                echo "<p class=\"success\">Successfull !</p>";
            elseif ($_GET['msg'] == 'fail'):
                echo "<p class=\"fail\">Error : Not Valid Entry !</p>";
            endif;
        endif;
        ?>
    </div>
    <?php
        if (isset($_GET['action'])):
            $folder = $_GET['action'];
            $file = "folder/" . $folder . ".php";
            require_once($file);
        else :
        ?>
            <div id="welcome_article">
                <h4> Admin Dashboard<strong style="color:#5E4A00;"></strong> </h4>
                <p>You can use this control panel to update your entire website contents. If you encounter any problem,
                    please contact following:</p>
                
               <!-- <p> Name : BIRENDRA MAHARJAN. </p>
                <p> Email : <a href="mailto:http://brendra8917@gmail.com">BRENDRA8917@GMAIL.COM</a> / <a
                            href="mailto:http://createmailto@gmail.com">CREATEMAILTO@GMAIL.COM</a></p>
                <p>Phone : <a href="tel://+977-9841652032">+977-9841652032</a></p>-->
            </div><!---------- dvi id welcome article close here -------------->
            <div class="highlight-data-wrap">
                <div class="highlight-data text-center green-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-users" aria-hidden="true"></i>
                        <span class="d-block title">{{$agentCount}} Agents</span>
                        <span class="d-block sub-title">are registered</span>
                    </a>            
                </div>
                <div class="highlight-data text-center red-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-building" aria-hidden="true"></i>
                        <span class="d-block title">{{$propertyCount}} Property</span>
                        <span class="d-block sub-title">are listed</span>
                    </a>            
                </div>
                <div class="highlight-data text-center yellow-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-map-o" aria-hidden="true"></i>
                        <span class="d-block title">{{$cityCount}} City</span>
                        <span class="d-block sub-title">locations</span>
                    </a>            
                </div>
            </div>
            <div id="welcome_article">
                <h5> Properties</h5>
            </div>
            <div class="highlight-data-wrap mt15">

                    <div class="highlight-data text-center green-widget c-pointer">
                        <a href="{{url('admin/property/list')}}" class="d-block">
                            <i class="fa fa-list-ul" aria-hidden="true"></i>
                            <span class="d-block title"> Property List</span>
                        </a>            
                    </div>


                <div class="highlight-data text-center green-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-building-o" aria-hidden="true"></i>
                        <span class="d-block title">{{$views}} Property Views</span>
                    </a>            
                </div>
                <div class="highlight-data text-center green-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-building" aria-hidden="true"></i>
                        <span class="d-block title">{{$activeCount}} Active Property</span>
                    </a>            
                </div>
            </div>
            <div id="welcome_article">
                <h5>Features</h5>
            </div>
            <div class="feature-data-wrap mt15">
                <div class="highlight-data text-center green-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                        <span class="d-block title">{{$landCount}} Lands</span>
                        <span class="d-block sub-title">are active</span>
                    </a>            
                </div>
                <div class="highlight-data text-center red-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-home" aria-hidden="true"></i>
                        <span class="d-block title">{{$houseCount}} House</span>
                        <span class="d-block sub-title">are active</span>
                    </a>            
                </div>
                <div class="highlight-data text-center yellow-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-square-o" aria-hidden="true"></i>
                        <span class="d-block title">{{$flatCount}} Flats</span>
                        <span class="d-block sub-title">are active</span>
                    </a>            
                </div>
                <div class="highlight-data text-center blue-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                        <span class="d-block title">{{$shopCount}} Shop</span>
                        <span class="d-block sub-title">are active</span>
                    </a>            
                </div>
                <div class="highlight-data text-center purple-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-table" aria-hidden="true"></i>
                        <span class="d-block title">{{$roomCount}} Rooms</span>
                        <span class="d-block sub-title">are active</span>
                    </a>            
                </div>
                <div class="highlight-data text-center orange-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-money" aria-hidden="true"></i>
                        <span class="d-block title">{{$rentCount}} Rent</span>
                        <span class="d-block sub-title">are active</span>
                    </a>            
                </div>
                <div class="highlight-data text-center ltBlue-widget c-pointer">
                    <a href="javascript:;" class="d-block">
                        <i class="fa fa-handshake-o" aria-hidden="true"></i>
                        <span class="d-block title">{{$sellCount}} Sell</span>
                        <span class="d-block sub-title">are active</span>
                    </a>            
                </div>
            </div>
            <?php
        endif;
    ?>
</div>