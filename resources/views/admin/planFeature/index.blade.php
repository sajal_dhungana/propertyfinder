@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($feature);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/admin/plan/feature/create')}}">Add New</a>
</div>

<div class="data-table">
    <table id="dataTable">
        <thead>
            <tr>
                <!-- <th class="id"><input type="checkbox"></th> -->
                <th>Name</th>

                <th>Status</th>
                <th class="action">Action</th>
                <!-- <th class="id">Id</th> -->

            </tr>
        </thead>
        <tbody>


            @foreach($feature as $f)

            <tr>
            <!-- <td><input type="checkbox"/></td> -->
            <td>{{$f->name}}</td>

            <td><?php if($f->status == 1){echo 'Active';}else{echo 'Inactive';}?></td>
            <td><a href="{{url('admin/plan/feature/edit').'/'.$f->id}}">Edit</a> <a href="{{url('admin/plan/feature/delete').'/'.$f->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
            </tr>



            @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
