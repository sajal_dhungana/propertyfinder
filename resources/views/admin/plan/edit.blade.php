@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/plan/update')}}" enctype="multipart/form-data">

{{csrf_field()}}
        <input type="hidden" name="id" value="{{$plan['id']}}"> 
      <fieldset>
        <legend>Edit Plan</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="name" value="{{$plan['name']}}"  placeholder="Type name here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Price *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="price" value="{{$plan['price']}}"  placeholder="Enter price per month" step="0.001" required/>
                  <span></span>
              </div>
          </div>


          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1" <?php if($plan['status'] == 1){echo 'selected';}?>>Active</option>
                            <option value="0" <?php if($plan['status'] == 0){echo 'selected';}?>>Inactive</option>
                 
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel"/>
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>

@include('admin.footer')