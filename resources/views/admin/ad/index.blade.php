@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">


<div class="list_nav">
<?php
$count= count($ad);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/admin/ad/create')}}">Add New</a>
</div>

<div class="data-table">
    <table id="dataTable">
        <thead>
            <tr>
                <!-- <th class="id"><input type="checkbox"></th> -->
                <th>Image</th>

                <th>Link</th>
                <th class="action">Action</th>
                <!-- <th class="id">Id</th> -->

            </tr>
        </thead>
        <tbody>


            @foreach($ad as $a)

            <tr>
            <!-- <td><input type="checkbox"/></td> -->
            <td><img src="{{asset('uploads/ad/'.$a->image)}}" style="width:50px"/></td>

            <td>{{$a->link}}</td>
            <td><a href="{{url('admin/ad/edit').'/'.$a->id}}">Edit</a> <a href="{{url('admin/ad/delete').'/'.$a->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
            </tr>



            @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
