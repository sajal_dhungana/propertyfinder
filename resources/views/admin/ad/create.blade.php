@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/ad/create')}}" enctype="multipart/form-data">

{{csrf_field()}}
      <fieldset>
        <legend>Add Advertisement</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Link *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="link"  placeholder="Enter Advertisement Link" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Image *</label>
              </div>
              <div class="view_elements">
                  <input type="file" name="image" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Page *</label>
              </div>
              <div class="view_elements">
                  <select type="text" name="page" required/>
                      <option value="">--Select Page--</option>
                      <option value="home">Home</option>
                      <option value="property_description">Property Description</option>
                      <option value="buy">Buy</option>
                      <option value="rent">Rent</option>
                      <option value="careers">Careers</option>
                      <option value="contact">Contact</option>
                      <option value="agent">Agent</option>
                  </select>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Section *</label>
              </div>
              <div class="view_elements">
                  <select type="text" name="section" required/>
                      <option value="">--Select Section--</option>
                      <option value="top">Top</option>
                      <option value="side_top">Side Top</option>
                      <option value="side_buttom">Side Buttom</option>
                      <option value="buttom">Buttom</option>
                      
                  </select>
                  <span></span>
              </div>
          </div>

        

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                 
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel"/>
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>





@include('admin.footer')