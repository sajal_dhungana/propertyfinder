@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/ad/update')}}" enctype="multipart/form-data">

{{csrf_field()}}
            <input type="hidden" name="id" value="{{$ad['id']}}">
      <fieldset>
        <legend>Edit Advertisement</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Link *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="link" value="{{$ad['link']}}" placeholder="Enter Advertisement Link" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Image </label>
              </div>
              <div class="view_elements">
                  <input type="file" name="image"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Page *</label>
              </div>
              <div class="view_elements">
                  <select type="text" name="page" required/>
                      <option value="home" <?php if($ad['page'] == 'home'){echo 'selected';}?>>Home</option>
                      <option value="property_description" <?php if($ad['page'] == 'property_description'){echo 'selected';}?>>Property Description</option>
                      <option value="buy" <?php if($ad['page'] == 'buy'){echo 'selected';}?>>Buy</option>
                      <option value="rent" <?php if($ad['page'] == 'rent'){echo 'selected';}?>>Rent</option>
                      <option value="careers" <?php if($ad['page'] == 'careers'){echo 'selected';}?>>Careers</option>
                      <option value="contact" <?php if($ad['page'] == 'contact'){echo 'selected';}?>>Contact</option>
                      <option value="agent" <?php if($ad['page'] == 'agent'){echo 'selected';}?>>Agent</option>
                  </select>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Section *</label>
              </div>
              <div class="view_elements">
                  <select type="text" name="section" required/>
                      <option value="top" <?php if($ad['section'] == 'top'){echo 'selected';}?>>Top</option>
                      
                      <option value="side_top" <?php if($ad['section'] == 'side_top'){echo 'selected';}?>>Side Top</option>

                      <option value="side_buttom" <?php if($ad['section'] == 'side_buttom'){echo 'selected';}?>>Side Buttom</option>

                      <option value="buttom" <?php if($ad['section'] == 'buttom'){echo 'selected';}?>>Buttom</option>
                      
                  </select>
                  <span></span>
              </div>
          </div>
        

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1" <?php if($ad['status'] == 1){echo 'selected';}?>>Active</option>
                            <option value="0" <?php if($ad['status'] == 0){echo 'selected';}?>>Inactive</option>
                 
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel"/>
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>





@include('admin.footer')