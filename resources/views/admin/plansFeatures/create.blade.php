@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/plans/features/create')}}" enctype="multipart/form-data">

{{csrf_field()}}
      <fieldset>
        <legend>Add Feature for Plan</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Plan *</label>
              </div>
              <div class="view_elements">
                  <select name="plan_id" id="plan_id" required/>
                    <option value="">--Select Plan--</option>
                    @foreach($plans as $p)
                      <option value="{{$p->id}}">{{$p->name}}</option>
                    @endforeach
                  </select>
                  <span></span>
              </div>
        </div>


           <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Feature *</label>
              </div>
              <div class="view_elements">
                  <select name="plan_feature[]" multiple required/>
                    <option value="">--Select Feature--</option>
                    @foreach($features as $f)
                      <option value="{{$f->id}}">{{$f->name}}</option>
                    @endforeach
                  </select>
                  <span></span>
              </div>
        </div>


          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel"/>
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>

<script>
  
</script>

@include('admin.footer')