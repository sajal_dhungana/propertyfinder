@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($plansFeatures);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/admin/plans/features/create')}}">Add New</a>
</div>

<div class="data-table">
    <table id="dataTable">
        <thead>
            <tr>
                <!-- <th class="id"><input type="checkbox"></th> -->
                <th>Plan</th>

                <th>Feature</th>
                <th class="action">Action</th>
                <!-- <th class="id">Id</th> -->

            </tr>
        </thead>
        <tbody>


            @foreach($plansFeatures as $f)

            <tr>
            <!-- <td><input type="checkbox"/></td> -->
            <td>{{isset($f->plan->name)?$f->plan->name:''}}</td>

            <td>
                <?php
                    $features = explode(',',$f->plan_feature);
                    // dd($features);
                    foreach($features as $pf){

                        $feature = \App\PlanFeatureModel::where('id',$pf)->first();
                        echo $feature->name.'<br>';

                        
                    }
                ?>
            </td>
            <td><a href="{{url('admin/plans/features/edit').'/'.$f->id}}">Edit</a> <a href="{{url('admin/plans/features/delete').'/'.$f->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
            </tr>



            @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
