@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->

<?php
    if($notification['agent_id']){
        $agent = \App\User::where('id',$notification['agent_id'])->first();
    }
?>

<div id="respons_section">
    <div class="inbox-detail-container">
        <div class="notice-detail">
            <div class="notice-header">
                <a href="{{url('/')}}"><img src="{{asset('images/ghar-logo.png')}}" alt="Ghargharana Logo"></a>
            </div>
            <div class="notice-body">
                <p class="user">Hi {{auth::guard('admin')->user()->name}},</p>
                <p class="notice-par">{{$notification['description']}}
                </p>
                <a href="{{$notification['link']}}" target="_blank" class="btn-notice">See Detail</a>
                <p class="user">Thanks</p>
                <p class="notice-par">Ghargharana Team</p>
            </div>
            <div class="notice-footer">
               
                <div class="copyright">© 2020 Ghargharan Realstate Pvt. Ltd., New Baneshwor, Kathmandu, Nepal. Ghargharana is a registered business name of Ghargharan Realstate Pvt. Ltd. Ghargharana and the Ghargharana logo are registered trademarks of Ghargharana.</div>
            </div>
        </div>
        
    </div>




@include('admin.footer')