@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($setting);
echo "<strong>$count Record(s) found</strong>";
?>
@if($count == 0)
        <a href="{{url('/admin/general-setting/create')}}">Add New</a>
@endif
</div>
<div class="data-table">
    <table id="dataTable">
        <thead>
            <tr>
                <!-- <th class="id"><input type="checkbox"></th> -->
                <th>Site Title</th>
                <th>Site Address</th>
                <th>Site Email</th>
                <th class="action">Action</th>
                <!-- <th class="id">Id</th> -->

            </tr>
        </thead>
        <tbody>


            @foreach($setting as $s)

            <tr>
                <!-- <td><input type="checkbox"/></td> -->
                <td>{{$s->site_title}}</td>
                <td>{{$s->site_address}}</td>
                <td>{{$s->site_email}}</td>
                <td><a href="{{url('admin/general-setting/edit').'/'.$s->id}}">Edit</a> </td>
            </tr>



            @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
