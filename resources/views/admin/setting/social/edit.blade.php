@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/social-media-setting/update')}}" enctype="multipart/form-data">

{{csrf_field()}}
            <input type="hidden" name="id" value="{{$setting['id']}}">
      <fieldset>
        <legend>Edit Social Media Setting</legend>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Facebook Url *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_facebook" id="site_facebook" value="{{$setting['site_facebook']}}"  placeholder="Type Facebook Url here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Twitter Url </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_twitter" id="site_twitter" value="{{$setting['site_twitter']}}"  placeholder="Type Twitter Url here"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Linkedin Url </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_linkedin" id="site_linkedin" value="{{$setting['site_linkedin']}}"  placeholder="Type Linkedin Url here"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Instagram Url </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_instagram" id="site_instagram" value="{{$setting['site_instagram']}}"  placeholder="Type Instagram Url here"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Viber Url </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_viber" id="site_viber" value="{{$setting['site_viber']}}"  placeholder="Type Viber Url here"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Youtube Url </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_youtube" id="site_youtube" value="{{$setting['site_youtube']}}" placeholder="Type Youtube Url here"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Whatsapp Url </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_whatsapp" id="site_whatsapp" value="{{$setting['site_whatsapp']}}"  placeholder="Type Whatsapp Url here"/>
                  <span></span>
              </div>
          </div>
          
      
         
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>



@include('admin.footer')