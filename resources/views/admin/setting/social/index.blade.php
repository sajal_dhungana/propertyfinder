@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($setting);
echo "<strong>$count Record(s) found</strong>";
?>
@if($count == 0)
        <a href="{{url('/admin/social-media-setting/create')}}">Add New</a>
@endif
</div>
<div class="data-table">
    <table id="dataTable">
        <thead>
            <tr>
                <!-- <th class="id"><input type="checkbox"></th> -->
                <th>Facebook</th>
                <th>Viber</th>
                <th>Whatsapp</th>
                <th class="action">Action</th>
                <!-- <th class="id">Id</th> -->

            </tr>
        </thead>
        <tbody>


            @foreach($setting as $s)

            <tr>
                <!-- <td><input type="checkbox"/></td> -->
                <td>{{$s->site_facebook}}</td>
                <td>{{$s->site_viber}}</td>
                <td>{{$s->site_whatsapp}}</td>
                <td><a href="{{url('admin/social-media-setting/edit').'/'.$s->id}}">Edit</a> </td>
            </tr>



            @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
