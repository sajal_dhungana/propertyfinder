@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/general-setting/update')}}" enctype="multipart/form-data">

{{csrf_field()}}
                <input type="hidden" name="id" value="{{$setting['id']}}">
      <fieldset>
        <legend>Edit Setting</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Title *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_title" id="site_title" value="{{$setting['site_title']}}"  placeholder="Type Site Title here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Address *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_address" id="site_address" value="{{$setting['site_address']}}"  placeholder="Type Site Address here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Email *</label>
              </div>
              <div class="view_elements">
                  <input type="email" name="site_email" id="site_email" value="{{$setting['site_email']}}" placeholder="Type Site Email here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Phone </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="site_phone" id="site_phone" value="{{$setting['site_phone']}}"  placeholder="Type Site Phone No. here"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Mobile </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="site_mobile" id="site_mobile" value="{{$setting['site_mobile']}}"  placeholder="Type Mobile No. here"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Fax </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="site_fax" id="site_fax" value="{{$setting['site_fax']}}"  placeholder="Type Fax No. here"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Open Time *</label>
              </div>
              <div class="view_elements">
                  <input type="time" name="site_open_time" id="site_open_time" value="{{$setting['site_open_time']}}"  placeholder="Type Open Time here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Close Time *</label>
              </div>
              <div class="view_elements">
                  <input type="time" name="site_close_time" id="site_close_time" value="{{$setting['site_close_time']}}"  placeholder="Type Close Time here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Footer Text *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_footer_text" id="site_footer_text" value="{{$setting['site_footer_text']}}"  placeholder="Type Footer Text here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Person Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_person_name" id="site_person_name" value="{{$setting['site_person_name']}}"  placeholder="Type Person Name here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Person Number *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="site_person_number" id="site_person_number" value="{{$setting['site_person_number']}}"  placeholder="Type Person Number here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Person Email *</label>
              </div>
              <div class="view_elements">
                  <input type="email" name="site_person_email" id="site_person_email" value="{{$setting['site_person_email']}}"  placeholder="Type Person Email here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Embed Map </label>
              </div>
              <div class="view_elements">
                  <textarea name="site_embed_map" id="site_embed_map" rows="4" style="width:50%"  placeholder="Embed Map here"/>{{$setting['site_embed_map']}}</textarea>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Message Title </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_msg_title" id="site_msg_title" value="{{$setting['site_msg_title']}}"  placeholder="Type Message Title here"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Message Name </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="site_msg_name" id="site_msg_name" value="{{$setting['site_msg_name']}}" placeholder="Type Message Name here"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Message </label>
              </div>
              <div class="view_elements">
                  <textarea name="site_msg_message" id="site_msg_message" rows="4" style="width:50%"  placeholder="Type Message here"/>{{$setting['site_msg_message']}}</textarea>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Photo Message </label>
              </div>
              <div class="view_elements">
                  <input type="file" name="site_msg_photo" id="site_msg_photo"  placeholder="Image Message"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Logo </label>
              </div>
              <div class="view_elements">
                  <input type="file" name="site_logo" id="site_logo"  placeholder="Logo"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Favicon </label>
              </div>
              <div class="view_elements">
                  <input type="file" name="site_favicon" id="site_favicon"  placeholder="Favicon"/>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>



@include('admin.footer')