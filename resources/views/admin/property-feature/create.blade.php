@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/propertyFeature/create')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <fieldset>
            <legend>Add Property Feature</legend>

            
            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name">Property Type *</label>
                </div>
                <div class="view_elements">
                    <select name="property_type_id" required>

                        <option value="">--Select Property Type--</option>
                            @foreach($types as $t)
                                <option value="{{$t->id}}">{{$t->name}}</option>
                            @endforeach


                    </select>
                    <span></span>
                </div>
            </div>

            <div class="view_items" style="color:black">
                <div class="view_labels">
                    <label for="form_name"> Feature *</label>
                </div>
                <!-- <div class="view_elements"> -->
                    <!-- <select name="name[]" class="custom-select" multiple required> -->
                            @foreach($features as $f)
                                <input type="checkbox" name="name[]" value="{{$f->id}}">{{$f->name}}</option>
                            @endforeach
                    <!-- </select> -->
                <!-- </div> -->
            </div>

            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name"> Feature Type *</label>
                </div>
                <div class="view_elements">
                    <select name="feature_type" required>
                            <option value="">--Select Feature Type--</option>
                        @foreach($featureType as $ft)
                            <option value="{{$ft->name}}">{{$ft->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            


            <div class="view_items">
                <div class="view_labels">
                    <input type="submit" value="Save" name="submit"/>
                    <input type="button" value="Cancel"
                           />
                </div>
                <div class="view_elements">

                    <span></span>
                </div>
            </div>
        </fieldset>
    </form>
</div>

</div>



@include('admin.footer')