@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($features);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/admin/propertyFeature/create')}}">Add New</a>
</div>
<div class="data-table">
    <table id="dataTable">
        <thead>
            <tr>
                <!-- <th class="id"><input type="checkbox"></th> -->
                <th>Property Type</th>
                <th>Feature Type</th>
                <th>Features</th>
                <th class="action">Action</th>
                <!-- <th class="id">Id</th> -->

            </tr>
        </thead>
        <tbody>


            @foreach($features as $f)

            <tr>
                <!-- <td><input type="checkbox"/></td> -->
                <td>{{isset($f->propertyType->name)?$f->propertyType->name:''}}</td>
                <td>@if($f->feature_type){{$f->feature_type}}@endif</td>
                <td><?php if($f->name){ $name = explode(',',$f->name); 
                                    foreach($name as $n){
                                        $row = App\FeatureModel::find($n);
                                    
                                        echo $row['name'].'<br>';
                                    }    
                                }

                ?></td>
                <td><a href="{{url('admin/propertyFeature/edit').'/'.$f->id}}">Edit</a> <a href="{{url('admin/propertyFeature/delete').'/'.$f->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
            </tr>



            @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
