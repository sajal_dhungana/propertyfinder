<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset="UTF-8" /> 
    <title>
        Ghargharana Login
    </title>
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard/css/login.css')}}" />
</head>
<body>
<img src="{{asset('images/ghar-logo.png')}}" alt="logo" style="display:block;margin:2% auto 1% auto;width:10%">
<div id="msg" style="margin:0 auto;width:25%;text-align:center;color:#0d93ff;"><?php if(!empty($error_msg)){echo $error_msg;} ?></div>
<form method="post" action="{{url('/admin')}}">

{{csrf_field()}}
  <h1>Ghargharana Admin</h1>
  <div class="inset">
  <p>
    <label for="email">EMAIL &nbsp;ADDRESS * </label>
    <input type="text" name="email" id="email" required>
  </p>
  <p>
    <label for="password">PASSWORD *</label>
    <input type="password" name="password" id="password" required>
  </p>
  <!-- <p>
    <input type="checkbox" name="remember" id="remember">
    <label for="remember">Remember me </label>
  </p> -->
  </div>
  <p class="p-container">
    <!-- <span><a href="index.php?action=forget">Forgot password ?</a></span> -->
    <input type="submit" name="submit" id="go" value="Log in">
  </p>
</form>

</body>
</html>