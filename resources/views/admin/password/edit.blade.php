@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/password/update')}}" enctype="multipart/form-data">

{{csrf_field()}}
      <input type="hidden" name="id" value="<?php if(Auth::guard('admin')->user()){echo Auth::guard('admin')->user()->id;} ?>">
      <fieldset>
        <legend>Change Password</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Password *</label>
              </div>
              <div class="view_elements">
                  <input type="password" name="password"  placeholder="Type password here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>

      </fieldset>
    </form>

</div>



@include('agent.footer')