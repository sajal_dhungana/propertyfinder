@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($applicants);
echo "<strong>$count Record(s) found</strong>";
?>
</div>
<div class="data-table">
<table id="dataTable">
    <thead>
        <tr>
            <!-- <th class="id"><input type="checkbox"></th> -->
            <th>Name</th>

            <th>Phone</th>
            <th>Email</th>
            <th class="action">Action</th>
            <!-- <th class="id">Id</th> -->

        </tr>
    </thead>
    <tbody>


        @foreach($applicants as $a)

        <tr>
            <!-- <td><input type="checkbox"/></td> -->
            <td>{{$a->name}}</td>

            <td>{{$a->phone}}</td>
            <td>{{$a->email}}</td>
            <td><a href="{{url('admin/applicants/details').'/'.$a->id}}" class=""><i class=""></i>View</a></td>
        </tr>



        @endforeach

    </tbody>
</table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
