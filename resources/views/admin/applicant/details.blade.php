@include('admin.header')


<div id="activity_section" class="agent-profile">
	<div id="left_nav">
		@include('admin.leftnav')
	</div><!-- end #left_nav -->
	<div id="respons_section">
		<div class="form">
			

				<fieldset>
        			<legend>Applicant Details</legend>

<div id="response_msg" style="color:green">
@if(Session::has('message'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {{session('message')}}</em></div>
@endif
</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Name: <span class="profile-det"><b>{{$applicant['name']}}</b></span></label>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Email: <span class="profile-det"><b>{{$applicant['email']}}</b></span></label>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Phone Number: <span class="profile-det"><b>{{$applicant['phone']}}</b></span></label>
						</div>
                    </div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name">Position:<span class="profile-det"><b>{{$applicant['position']}}</b></span></label>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name">As described himself: <span class="profile-det">{{$applicant['tell_us']}}</span></label>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Download File: <span class="profile-det"><span class="download-file"><a href="{{asset('uploads/careers/'.$applicant['cv'])}}" download><i class="fa fa-download" aria-hidden="true"></i></a></span></span></label>
						</div>
                    </div>
                    <div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Status: 
                                <span class="profile-det">
                                    <label class="switch">
                                        <input type="checkbox" id="status" onchange="changeApplicantStatus(<?php echo $applicant['id'];?>)" <?php
                                        	if($applicant['status'] == 1){echo 'checked';}
                                        ?>>
                                        <span class="slider round"></span>
                                    </label>
                                </span>
                            </label>
						</div>
					</div>


				</fieldset>

			</form>
		</div>
        
    </div>
</div><!-- end #activity_section -->

@include('admin.footer')

<script type="text/javascript">


	function changeApplicantStatus(id){
		var val = $('#status').is(':checked');
		var id = id;
		if(val == true) {
			
			var status = 1;
		}

		if(val == false) {
			
			var status = 0;
		}




			$.ajax({
				type:'POST' ,
				url:"{{url('admin/applicant/status')}}",
				data: {id:id,status:status,_token:'{{csrf_token()}}'},
				
				success: function(response) {
					location.reload();
				}
			});
		
	}
</script>