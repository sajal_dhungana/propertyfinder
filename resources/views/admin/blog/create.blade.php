@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/blogs/create')}}" enctype="multipart/form-data">

{{csrf_field()}}
      <fieldset>
        <legend>Add Blog</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Title *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="title"  placeholder="Type blog Title here" required/>
                  <span></span>
              </div>
          </div>

            <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Description *</label>
              </div>
              <div class="view_elements">
                  <textarea name="description" id="description" rows="4"  placeholder="Type blog description here" required/></textarea>
                  <span></span>
              </div>
            </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Image *</label>
              </div>
              <div class="view_elements">
                  <input type="file" name="image" required/>
                  <span></span>
              </div>
          </div>

           <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Featured </label>
              </div>
              <div class="view_elements">
                  <input type="checkbox" name="featured"/>
                  <span></span>
              </div>
          </div>

       

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                 
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel"/>
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>





@include('admin.footer')