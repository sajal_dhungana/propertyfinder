@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/blogs/update')}}" enctype="multipart/form-data">

{{csrf_field()}}
              <input type="hidden" name="id" value="{{$blog['id']}}">
      <fieldset>
        <legend>Edit Blog</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Title *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="title" value="{{$blog['title']}}"  placeholder="Type blog Title here" required/>
                  <span></span>
              </div>
          </div>

           <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Description *</label>
              </div>
              <div class="view_elements">
                  <textarea name="description" id="description" rows="4"  placeholder="Type blog description here" required/>{{$blog['description']}}</textarea>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Image </label>
              </div>
              <div class="view_elements">
                  <input type="file" name="image"/>
                  <span></span>
              </div>
          </div>


          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Featured </label>
              </div>
              <div class="view_elements">
                  <input type="checkbox" name="featured" <?php if($blog['featured'] == 1){echo 'checked';}?>/>
                  <span></span>
              </div>
          </div>

       

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1" <?php if($blog['status'] == 1){echo 'selected';}?>>Active</option>
                            <option value="0" <?php if($blog['status'] == 0){echo 'selected';}?>>Inactive</option>
                 
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel"/>
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>





@include('admin.footer')