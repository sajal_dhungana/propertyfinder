@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($blogs);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/admin/blogs/create')}}">Add New</a>
</div>
<div class="data-table">
    <table id="dataTable">
        <thead>
            <tr>
                <!-- <th class="id"><input type="checkbox"></th> -->
                <th>Image</th>
                <th>Title</th>

                <th>Description</th>
                <th class="action">Action</th>
                <!-- <th class="id">Id</th> -->

            </tr>
        </thead>
        <tbody>


            @foreach($blogs as $b)

            <tr>
                <!-- <td><input type="checkbox"/></td> -->
                <td><img src="{{asset('uploads/blogs/'.$b->image)}}" style="width:100px"/></td>
                <td>{{$b->title}}</td>
                <td>{{substr($b->description,0,200)}}</td>

                <td><a href="{{url('admin/blogs/edit'.'/'.$b->id)}}">Edit</a> <a href="{{url('admin/blogs/delete').'/'.$b->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
            </tr>



            @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
