@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/agency/update')}}" enctype="multipart/form-data">

{{csrf_field()}}
            <input type="hidden" name="id" value="{{$agency['id']}}">
      <fieldset>
        <legend>Edit Agency</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="name" value="{{$agency['name']}}" placeholder="Enter Agency Name" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Email *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="email" value="{{$agency['email']}}" placeholder="Enter Agency Email" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Phone *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="phone" value="{{$agency['phone']}}" placeholder="Enter Agency Phone" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Location *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="location" value="{{$agency['location']}}" placeholder="Enter Agency Location" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Image </label>
              </div>
              <div class="view_elements">
                  <input type="file" name="image"/>
                  <span></span>
              </div>
          </div>


        

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1" <?php if($agency['status'] == 1){echo 'selected';}?>>Active</option>
                            <option value="0" <?php if($agency['status'] == 0){echo 'selected';}?>>Inactive</option>
                 
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel"/>
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>





@include('admin.footer')