@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/feature/update')}}" enctype="multipart/form-data">

{{csrf_field()}}

        <input type="hidden" name="id" value="{{$feature['id']}}">
      <fieldset>
        <legend>Edit Feature</legend>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Feature *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="name" value="{{$feature['name']}}" placeholder="Type feature Name here" required/> 
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Icon *</label>
              </div>
              <div class="view_elements">
                  <select name="icon_id" id="icon_id" required>
                        <option value="">--Select Icon--</option>

                        @foreach($icon as $i)

                                <option value="{{$i->id}}" <?php if($i->id == $feature['icon_id']){echo 'selected';}?>>{{$i->name}}</option>
                        @endforeach

                  </select>
                  <span></span>
              </div>
          </div>
         
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1" <?php if($feature['status'] == 1){echo 'selected';}?>>Active</option>
                            <option value="0" <?php if($feature['status'] == 0){echo 'selected';}?>>Inactive</option>
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>



@include('admin.footer')