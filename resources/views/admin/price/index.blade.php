@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($price);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/admin/price/create')}}">Add New</a>
</div>
<div class="data-table">
    <table id="dataTable">
        <thead>
            <tr>
                <!-- <th class="id"><input type="checkbox"></th> -->
                <th>Property Type</th>
                <th>Min Price</th>
                <th>Max Price</th>
                <th class="action">Action</th>
                <!-- <th class="id">Id</th> -->

            </tr>
        </thead>
        <tbody>


            @foreach($price as $p)

            <tr>
                <!-- <td><input type="checkbox"/></td> -->
                <td>{{isset($p->propertyTye->name)?$p->propertyTye->name:''}}</td>
                <td>{{$p->min_price}}</td>
                <td>{{$p->max_price}}</td>
                <td><a href="{{url('admin/price/edit').'/'.$p->id}}">Edit</a> <a href="{{url('admin/price/delete').'/'.$p->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
            </tr>



            @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
