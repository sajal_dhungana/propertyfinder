@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/price/update')}}" enctype="multipart/form-data">

{{csrf_field()}}
        <input type="hidden" name="id" value="{{$price['id']}}">
      <fieldset>
        <legend>Edit Price</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Property Type *</label>
              </div>
              <div class="view_elements">
                  <select name="property_type_id" id="property_type_id" required/>
                            <option value="">--Select Property Type--</option>
                        @foreach($propertyType as $p)
                                <option value="{{$p->id}}" <?php if($p->id == $price['property_type_id']){echo 'selected';}?>>{{$p->name}}</option>
                        @endforeach
                  </select>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Min Price *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="min_price" id="min_price" value="{{$price['min_price']}}" step="0.001" placeholder="Type min price here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Max Price *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="max_price" id="max_price" value="{{$price['max_price']}}" step="0.001" placeholder="Type max price here" required/>
                  <span></span>
              </div>
          </div>
      
         
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1" <?php if($price['status'] == 1){echo 'selected';}?>>Active</option>
                            <option value="0" <?php if($price['status'] == 0){echo 'selected';}?>>Inactive</option>
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>



@include('admin.footer')