@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($country);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/admin/country/create')}}">Add New</a>
</div>

<table id="dataTable">
<thead>
<tr>
<!-- <th class="id"><input type="checkbox"></th> -->
<th>Country</th>
<th>Short Name</th>
<th class="action">Action</th>
<!-- <th class="id">Id</th> -->

</tr>
</thead>
<tbody>


@foreach($country as $c)

<tr>
<!-- <td><input type="checkbox"/></td> -->
<td>{{$c->name}}</td>
<td>{{$c->sortname}}</td>
<td><a href="{{url('admin/country/edit').'/'.$c->id}}">Edit</a> <a href="{{url('admin/country/delete').'/'.$c->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
</tr>



@endforeach

</tbody>
</table>
</div>
</div>
</div><!-- end #activity_section -->
