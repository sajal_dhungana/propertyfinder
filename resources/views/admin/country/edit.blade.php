@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/country/update')}}" enctype="multipart/form-data">

{{csrf_field()}}

        <input type="hidden" name="id" value="{{$country['id']}}">
      <fieldset>
        <legend>Edit Country</legend>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="name"  placeholder="Type name here" value="{{$country['name']}}" required/>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Short Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="sortname"  placeholder="Type short name here" value="{{$country['sortname']}}" required/>
                  <span></span>
              </div>
          </div>
      
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Google Map</label>
              </div>
              <div class="view_elements">
                  <textarea name="google_map" id="" cols="30" rows="7">{{$country['google_map']}}</textarea>
              </div>
          </div>
  
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Add to Feature *</label>
              </div>
              <div class="view_elements">
                  <select name="add_to_feature" required>
                            <option value="">--Select Add to Feature--</option>
                            <option value="0" <?php if($country['add_to_feature'] == 0){echo 'selected';}?>>No</option>
                            <option value="1" <?php if($country['add_to_feature'] == 1){echo 'selected';}?>>Yes</option>
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Order *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="ordering"  placeholder="Type name here" min="1"  value="{{$country['ordering']}}"/>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1" <?php if($country['status'] == 1){echo 'selected';}?>>Active</option>
                            <option value="0" <?php if($country['status'] == 0){echo 'selected';}?>>Inactive</option>
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>



@include('admin.footer')