
</div><!-- end #wrapper -->
<div id="response_msg" style="color:green">
    @if(Session::has('message'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {{session('message')}}</em>
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true" onclick="removeMessage()">&times;</span>
        </button>
    </div>
    @endif
</div>
<script src="{{asset('dashboard/js/jquery-3.5.1.js')}}"></script>
<!-- <script src="{{asset('dashboard/js/jquery-1.11.2.min.js')}}"></script> -->
<script src="{{asset('dashboard/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('dashboard/ajax/ajax-function.js')}}"></script>
<script src="{{asset('dashboard/js/time_set.js')}}"></script>
<script src="{{asset('dashboard/dataTable/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('dashboard/editor/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('dashboard/js/main.js')}}"></script>

<script  src="{{asset('js/image-uploader.min.js')}}"></script>

<script>
     setTimeout(function() { $('#response_msg').fadeOut('fast'); }, 5000);

     function removeMessage(){
        $("#response_msg").remove();
     }

	$(document).ready( function () {
    $('#dataTable').DataTable();


} );

CKEDITOR.replace( 'description');

// Get the modal
</script>
<script>
    // Open the Modal
    function openModal() {
  document.getElementById("propertyImgModal").style.display = "block";
}

// Close the Modal
function closeModal() {
  document.getElementById("propertyImgModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("propertyImgSlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

</body>
</html>
<?php //ob_end_flush(); ?>

