@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($user);
echo "<strong>$count Record(s) found</strong>";
?>
<!-- <a href="{{url('/admin/agent/create')}}">Add New</a> -->
</div>
<div class="data-table">
    <table id="dataTable">
        <thead>
            <tr>
                <!-- <th class="id"><input type="checkbox"></th> -->
                <th>Name</th>
                <th>Type</th>

                <th>City</th>
                <th class="action">Action</th>
                <!-- <th class="id">Id</th> -->

            </tr>
        </thead>
        <tbody>


            @foreach($user as $u)

            <tr>
                <!-- <td><input type="checkbox"/></td> -->
                <td>{{$u->name}}</td>
                <td>{{isset($u->type->type)?$u->type->type:''}}</td>

                <td>{{isset($u->city->name)?$u->city->name:''}}</td>

                <td><a href="{{url('admin/agent/status').'/'.$u->id}}" class=""><i class=""></i><?php if($u->status == 1){echo 'Active';}else{echo 'Inactive';}?></a><a href="{{url('admin/agent/details').'/'.$u->id}}" class=""><i class=""></i>View</a></td>
            </tr>



            @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->
@include('admin.footer')
