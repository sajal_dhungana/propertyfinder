@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/agent/create')}}" enctype="multipart/form-data">

{{csrf_field()}}
      <fieldset>
        <legend>Add Agent</legend>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="name"  placeholder="Type name here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Email *</label>
              </div>
              <div class="view_elements">
                  <input type="email" name="email"  placeholder="Type email here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Password *</label>
              </div>
              <div class="view_elements">
                  <input type="password" name="password"  placeholder="Type password here" required/>
                  <span></span>
              </div>
          </div>
      
          
  
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Agent Type *</label>
              </div>
              <div class="view_elements">
                  <select name="type_id" required>
                            <option value="">--Select Agent Type--</option>
                       @foreach($type as $t) 
                            <option value="{{$t->id}}">{{$t->type}}</option>
                        @endforeach
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Company Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="company_name"  placeholder="Type company name here" required />
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Company Registration Number </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="company_registration_num"  placeholder="Type company registration number here" />
                  <span></span>
              </div>
          </div>


          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Company Established Date </label>
              </div>
              <div class="view_elements">
                  <input type="date" name="company_established_date"  placeholder="Type company established date here" />
                  <span></span>
              </div>
          </div>

          
          <!-- <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Country Name *</label>
              </div>
              <div class="view_elements">
                  <select name="country_id" id="country_id" required onchange="getCity()">
                            <option value="">--Select Agent Country--</option>
                       @foreach($country as $c) 
                            <option value="{{$c->id}}">{{$c->name}}</option>
                        @endforeach
                  </select>
                  <span></span>
              </div>
          </div> -->

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">City Name *</label>
              </div>
              <div class="view_elements">
                  <select name="city_id" id="city_id" required>
                            <option value="">--Select Agent City--</option>
                        
                       
                  </select>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Contact Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="contact_name"  placeholder="Type contact name here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Contact Number *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="contact_num"  placeholder="Type contact number here" required/>
                  <span></span>
              </div>
          </div>



          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>

<script>
    function getCity(){
        $('#city_id').html('<option value="">--Select City--</option>');
        var country = document.getElementById('country_id').value;
        $.ajax({
        url: "{{url('getCity')}}"+'/'+country,
        type: 'GET',
        success: function(res) {
            $('#city_id').html(res);
        }
    });
        
    }
</script>

@include('admin.footer')