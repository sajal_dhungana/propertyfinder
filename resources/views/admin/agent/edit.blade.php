@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
    <form method="post" action="{{url('admin/agent/update')}}" enctype="multipart/form-data">

        {{csrf_field()}}
            <input type="hidden" name="id" value="{{$user['id']}}">
        <fieldset>
            <legend>Edit Agent</legend>

            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name"> Name *</label>
                </div>
                <div class="view_elements">
                    <input type="text" name="name" value="{{$user['name']}}" placeholder="Type name here" required/>
                    <span></span>
                </div>
            </div>

            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name"> Email *</label>
                </div>
                <div class="view_elements">
                    <input type="email" name="email" value="{{$user['email']}}"  placeholder="Type email here" required readonly/>
                    <span></span>
                </div>
            </div>

            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name"> Password</label>
                </div>
                <div class="view_elements">
                    <input type="password" name="password"  placeholder="Type password here"/>
                    <span></span>
                </div>
            </div>
        
            
    
            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name">Agent Type *</label>
                </div>
                <div class="view_elements">
                    <select name="type_id" required>
                                <option value="">--Select Agent Type--</option>
                        @foreach($type as $t) 
                                <option value="{{$t->id}}" <?php if($t->id == $user['type_id']){echo 'selected';}?>>{{$t->type}}</option>
                            @endforeach
                    </select>
                    <span></span>
                </div>
            </div>
            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name"> Company Name *</label>
                </div>
                <div class="view_elements">
                    <input type="text" name="company_name"  placeholder="Type company name here" value="{{$user['company_name']}}" required />
                    <span></span>
                </div>
            </div>

            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name"> Company Registration Number </label>
                </div>
                <div class="view_elements">
                    <input type="text" name="company_registration_num"  placeholder="Type company registration number here" value="{{$user['company_registration_num']}}"/>
                    <span></span>
                </div>
            </div>


            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name"> Company Established Date </label>
                </div>
                <div class="view_elements">
                    <input type="date" name="company_established_date" value="{{$user['company_established_date']}}"  placeholder="Type company established date here" />
                    <span></span>
                </div>
            </div>

            
            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name">Country Name *</label>
                </div>
                <div class="view_elements">
                    <select name="country_id" id="country_id" required onchange="getCity()">
                                <option value="">--Select Agent Country--</option>
                        @foreach($country as $c) 
                                <option value="{{$c->id}}" <?php if($user['country_id'] == $c->id){echo 'selected';}?>>{{$c->name}}</option>
                            @endforeach
                    </select>
                    <span></span>
                </div>
            </div>

            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name">City Name *</label>
                </div>
                <div class="view_elements">
                    <select name="city_id" id="city_id" required>
                                <option value="">--Select Agent City--</option>
                            @foreach($city as $ct)
                                <option value="{{$ct->id}}" <?php if($ct->id == $user['city_id']){echo 'selected';}?>>{{$ct->name}}</option>
                            @endforeach
                        
                    </select>
                    <span></span>
                </div>
            </div>

            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name"> Contact Name *</label>
                </div>
                <div class="view_elements">
                    <input type="text" name="contact_name" value="{{$user['contact_name']}}" placeholder="Type contact name here" required/>
                    <span></span>
                </div>
            </div>

            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name"> Contact Number *</label>
                </div>
                <div class="view_elements">
                    <input type="text" name="contact_num" value="{{$user['contact_num']}}" placeholder="Type contact number here" required/>
                    <span></span>
                </div>
            </div>



            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name">Status *</label>
                </div>
                <div class="view_elements">
                    <select name="status" required>
                    <option value="">--Select Status--</option>
                                <option value="1" <?php if($user['status'] == 1){echo 'selected';}?>>Active</option>
                                <option value="0" <?php if($user['status'] == 0){echo 'selected';}?>>Inactive</option>
                    </select>
                    <span></span>
                </div>
            </div>
            <div class="view_items">
                <div class="view_labels">
                    <input type="submit" name="submit"/>
                    <input type="button" value="Cancel" />
                </div>
                <div class="view_elements">

                    <span></span>
                </div>
            </div>
        </fieldset>
    </form>

</div>


<script>
    function getCity(){
        $('#city_id').html('<option value="">--Select City--</option>');
        var country = document.getElementById('country_id').value;
        $.ajax({
        url: "{{url('getCity')}}"+'/'+country,
        type: 'GET',
        success: function(res) {
            $('#city_id').html(res);
        }
    });
        
    }
</script>

@include('admin.footer')