@include('admin.header')

<div id="activity_section" class="agent-profile">
	<div id="left_nav">
		@include('admin.leftnav')
	</div><!-- end #left_nav -->
	<div id="respons_section">
		<div class="form">
			
				{{csrf_field()}}

				<fieldset>
        			<legend>Profile Details</legend>


					<div id="response_msg" style="color:green">
						@if(Session::has('message'))
						<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {{session('message')}}</em></div>
						@endif
					</div>

				@if($agent->name)
					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Name: <span class="profile-det"><b>{{$agent->name}}</b></span></label>
						</div>
					</div>
				@endif

				@if($agent->email)
					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Email: <span class="profile-det">{{$agent->email}}</span></label>
						</div>
					</div>
				@endif

				@if($agent->image)

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Image: <span class="profile-det"><img src="{{asset('uploads/agents/'.$agent->image)}}" alt="" width="50"/></span> </label>
						</div>
					</div>
				@endif

				@if(isset($agent->type->type))
					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Agent Type:<span class="profile-det">{{isset($agent->type->type)?$agent->type->type:''}}</span></label>
						</div>
					</div>
				@endif


				@if($agent->company_name)
					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Company Name: <span class="profile-det">{{$agent->company_name}}</span></label>
						</div>
					</div>
				@endif

				@if($agent->company_logo)
					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Company Logo: <span  class="profile-det"><img src="{{asset('uploads/agents/'.$agent->company_logo)}}" alt="" width="50"/></span> </label>
						</div>
					</div>
				@endif

				@if($agent->company_logo)

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Company Reg. No.: <span class="profile-det">{{$agent->company_registration_num}}</span></label>
						</div>
					</div>
				@endif


				@if(isset($agent->city->name))
					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> City: <span class="profile-det">{{isset($agent->city->name)?ucfirst($agent->city->name):''}}</span></label>
						</div>
					</div>
				@endif

				@if($agent->address)
					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Address: <span class="profile-det">{{$agent->address}}</span></label>
						</div>
					</div>
				@endif

				@if($agent->contact_name)
					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Contact Name: <span class="profile-det">{{$agent->contact_name}}</span></label>
						</div>
					</div>
				@endif

				@if($agent->contact_num)
					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Contact Number: <span class="profile-det">{{$agent->contact_num}}</span></label>
						</div>
                    </div>
                @endif

                @if($agent->contact_num1)
					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Another Number: <span class="profile-det">{{$agent->contact_num1}}</span></label>
						</div>
                    </div>
                @endif


                    <div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Status: 
                                <span class="profile-det"><?php if($agent->status == 1){echo 'Active';}else{echo 'Inactive';}?></span></label>
                            </label>
						</div>
					</div>


				</fieldset>

			
		</div>
		<div class="right-profile">
			<div class="card hovercard">
				<div class="cardheader">

				</div>
				@if($agent->image)
					<div class="avatar">
						<img alt="" src="{{asset('uploads/agents/'.$agent->image)}}">
					</div>
				@endif
				<div class="info">
                    
                @if($agent->name)
					<div class="title">
                        <a target="_blank" href="">{{$agent->name}}</a>
					</div>
				@endif
						

				@if($agent->address)	
						<div class="address">
							{{$agent->address}}

						</div>
				@endif
					
				@if(isset($agent->type->type))
					<div class="company">
                        <span class="agent-type">
                            {{isset($agent->type->type)?ucfirst($agent->type->type):''}}
					    </span> 
							

					</div>
				@endif

				@if($agent->contact_num)
					<div class="contact">
							{{$agent->contact_num}}
					</div>
				@endif
				</div>
			</div>
        </div>
        
    </div>
    <div class="dash-right mt15">
        <div class="data-table">
            <div class="data-table-title">Agent Property List</div>
            <table id="dataTable">
                <thead>
                    <tr>
                    <!-- <th class="id"><input type="checkbox"></th> -->
                    <th>Title</th>
					<th>Agent Name</th>
					<th>Property Type</th>
					<th>City</th>
					<th class="action">Action</th>
                    <!-- <th class="id">Id</th> -->

                    </tr>
                </thead>
                <tbody>

                    @foreach($property as $p)

<tr>
<!-- <td><input type="checkbox"/></td> -->
<td>{{$p->title}}</td>
<td>{{isset($p->agent->name)?$p->agent->name:''}}</td>
<td>{{isset($p->type->name)?$p->type->name:''}}</td>

<td>{{isset($p->city->name)?$p->city->name:''}}</td>


<td> <a href="{{url('admin/property/status').'/'.$p->id}}" class="<?php if($p->status == 1){echo 'active';}else{echo 'inactive';}?>"><i class=""></i><?php if($p->status == 1){echo 'Active';}else{echo 'Inactive';}?></a><a href="{{url('admin/property/details').'/'.$p->id}}" class=""><i class=""></i>View</a></td>
</tr>



@endforeach


                </tbody>
            </table>
        </div>
    </div>
</div><!-- end #activity_section -->
@include('admin.footer')