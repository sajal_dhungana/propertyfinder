@include('includes.header')


<main role="main">
    <section class="faq-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="faq">
                        <h1 class="mb-4b subpage-main-title">Frequently Asked Questions</h1>
                        <div class="faq-desc mt-5">
                            <div class="">
                                <div class="col-md-12">
                                    <div class="">
                                    <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">

                                    @foreach($faq as $f)
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading{{$f->id}}">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse{{$f->id}}" aria-expanded="true" aria-controls="collapseOne3">
                                                        {{$f->question}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse{{$f->id}}" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="heading{{$f->id}}">
                                                <div class="panel-body">
                                                    <p>
                                                        {{$f->answer}}
                                                     </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach



                                        <!-- <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo3">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapseTwo3" aria-expanded="false" aria-controls="collapseTwo3">
                                                        How do I sign up to the program?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo3">
                                                <div class="panel-body">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros. </p>
                                                </div>
                                            </div>
                                        </div> -->
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section class="home-blog pb-5">
        <div class="container">
            <div class="related-post-title"><h2 class="mt-5">Related Blog</h2></div>
            
            <div id="carousel-example-multi" class="carousel slide carousel-multi-item v-2" data-ride="carousel">

                
                <div class="controls-top">
                <a class="btn-floating" href="#carousel-example-multi" data-slide="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                <a class="btn-floating" href="#carousel-example-multi" data-slide="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
                
            
            
                <ol class="carousel-indicators">
                <li data-target="#carousel-example-multi" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-multi" data-slide-to="1"></li>
                <li data-target="#carousel-example-multi" data-slide-to="2"></li>
                <li data-target="#carousel-example-multi" data-slide-to="3"></li>
                <li data-target="#carousel-example-multi" data-slide-to="4"></li>
                <li data-target="#carousel-example-multi" data-slide-to="5"></li>
                </ol>
                
            
                <div class="carousel-inner v-2" role="listbox">
                <div class="row mt-7">
                    <div class="carousel-item active">
                    <div class="col-12 col-md-3">
                        <div class="card mb-2">
                        <a href="blog-detail.php" class="image-box"><img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/img (36).jpg"
                            alt="Card image cap"></a>
                        <div class="card-body">
                            <h4 class="card-title"><a href="blog-detail.php">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                            card's content.</p>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <div class="col-12 col-md-3">
                        <div class="card mb-2">
                        <a href="blog-detail.php" class="image-box"><img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/img (34).jpg"
                            alt="Card image cap"></a>
                        <div class="card-body">
                            <h4 class="card-title"><a href="blog-detail.php">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                            card's content.</p>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <div class="col-12 col-md-3">
                        <div class="card mb-2">
                        <a href="blog-detail.php" class="image-box"><img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/img (38).jpg"
                            alt="Card image cap"></a>
                        <div class="card-body">
                            <h4 class="card-title"><a href="blog-detail.php">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                            card's content.</p>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <div class="col-12 col-md-3">
                        <div class="card mb-2">
                        <a href="blog-detail.php" class="image-box"><img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/img (29).jpg"
                            alt="Card image cap"></a>
                        <div class="card-body">
                            <h4 class="card-title"><a href="blog-detail.php">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                            card's content.</p>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <div class="col-12 col-md-3">
                        <div class="card mb-2">
                        <a href="blog-detail.php" class="image-box"><img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/img (30).jpg"
                            alt="Card image cap"></a>
                        <div class="card-body">
                            <h4 class="card-title"><a href="blog-detail.php">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></h4>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                            card's content.</p>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="carousel-item">
                    <div class="col-12 col-md-3">
                        <div class="card mb-2">
                        <a href="blog-detail.php" class="image-box"><img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/img (27).jpg"
                            alt="Card image cap"></a>
                        <div class="card-body">
                            <h4 class="card-title"><a href="blog-detail.php">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></h4>
                            <p class="card-text">lets have some fun so lets try this Some quick example text to build on the card title and make up the bulk of the
                            card's content.</p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
    </section> -->
</main>


@include('includes.footer')