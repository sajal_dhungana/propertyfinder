@include('agent.header')

<div id="activity_section" class="agent-profile">
	<div id="left_nav">
		@include('agent.leftnav')
	</div><!-- end #left_nav -->
	<div id="respons_section">

		<div class="form">
			<form method="post" action="{{url('agent/profile/update')}}" enctype="multipart/form-data">
				{{csrf_field()}}

				<input type="hidden" name="id" value="{{$id}}">
				<fieldset>
        			<legend>Profile Details</legend>


					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Name *</label>
						</div>
						<div class="view_elements">
							<input type="text" name="name" id="name" value="{{$user['name']}}"  placeholder="Type name here" required/>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Email *</label>
						</div>
						<div class="view_elements">
							<input type="email" name="email" id="email" value="{{$user['email']}}" placeholder="Type email here" required readonly/>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Image </label>
						</div>
						<div class="view_elements">
							<input type="file" name="image" id="image"/>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Agent Type *</label>
						</div>
						<div class="view_elements">
							<select name="type_id" id="type_id" required>
										<option value="">--Select Agent Type--</option>
									@foreach($agentType as $type)
										<option value="{{$type->id}}" <?php if($type->id == $user['type_id']){echo 'selected';}?>>{{$type->type}}</option>
									@endforeach

							</select>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Company Name *</label>
						</div>
						<div class="view_elements">
							<input type="text" name="company_name" id="company_name" value="{{$user['company_name']}}"  placeholder="Type Company Name here" required/>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Company Logo </label>
						</div>
						<div class="view_elements">
							<input type="file" name="company_logo" id="company_logo"/>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Company Registration Number *</label>
						</div>
						<div class="view_elements">
							<input type="text" name="company_registration_num" id="company_registration_num" placeholder="Company Registration Number" value="{{$user['company_registration_num']}}" required/>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> City *</label>
						</div>
						<div class="view_elements">
							<select name="city_id" id="city_id" required>
										<option value="">--Select City--</option>
									@foreach($city as $c)
										<option value="{{$c->id}}" <?php if($c->id == $user['city_id']){echo 'selected';}?>>{{$c->name}}</option>
									@endforeach

							</select>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Address *</label>
						</div>
						<div class="view_elements">
							<input type="text" name="address" id="address" placeholder="Address" value="{{$user['address']}}" required/>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Contact Name *</label>
						</div>
						<div class="view_elements">
							<input type="text" name="contact_name" id="contact_name" value="{{$user['contact_name']}}" placeholder="Contact Name" required/>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Contact Number *</label>
						</div>
						<div class="view_elements">
							<input type="number" name="contact_num" id="contact_num" value="{{$user['contact_num']}}" placeholder="Contact Number" required/>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<label for="form_name"> Contact Number 2 *</label>
						</div>
						<div class="view_elements">
							<input type="number" name="contact_num1" id="contact_num1" value="{{$user['contact_num1']}}" placeholder="Contact Number" required/>
							<span></span>
						</div>
					</div>

					<div class="view_items">
						<div class="view_labels">
							<input type="submit" name="Update" value="Update"/>
							<input type="button" value="Cancel"/>
						</div>
						<div class="view_elements">

							<span></span>
						</div>
					</div>


				</fieldset>

			</form>
		</div>
		<div class="right-profile">
			<div class="card hovercard">
				<div class="cardheader">

				</div>
				@if($user['image'])
					<div class="avatar">
						<img alt="" src="{{asset('uploads/agents/'.$user['image'])}}">
					</div>
				@else
					<div class="avatar">
						<img alt="" src="{{asset('default.jpg')}}">
					</div>
				@endif

				<div class="info">
					<div class="title">
						<a>{{$user['name']}}</a>
					</div>
						<?php
							$city = \App\CityModel::where('id',$user['city_id'])->first();
							
						?>

						<?php
							$type = \App\AgentTypeModel::where('id',$user['type_id'])->first();
							
						?>

					
						<div class="address">
						@if(isset($city))
							{{ucfirst($city->name)}}
						@endif

							@if($user['address'])
									- {{ucfirst($user['address'])}}
							@endif

						</div>
					

					<div class="company"><span class="agent-type">{{ucfirst($user['company_name'])}}
					</span> 
							@if(isset($type))
							- {{ucfirst($type->type)}}
							@endif

					</div>

					<div class="contact">
							@if($user['email'])
								{{$user['email']}}
							@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- end #activity_section -->
