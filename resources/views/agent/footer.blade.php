
</div><!-- end #wrapper -->
<div id="response_msg" style="color:green">
    @if(Session::has('message'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {{session('message')}}</em>
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true" onclick="removeMessage()">&times;</span>
        </button>
    </div>
    @endif
</div>
<script src="{{asset('dashboard/js/jquery-3.5.1.js')}}"></script>
<!-- <script src="{{asset('dashboard/js/jquery-1.11.2.min.js')}}"></script> -->
<script src="{{asset('dashboard/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('dashboard/ajax/ajax-function.js')}}"></script>
<script src="{{asset('dashboard/js/time_set.js')}}"></script>
<!-- <script  src="{{asset('dashboard/dataTable/media/js/jquery.dataTables.js')}}"></script> -->

<script src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="{{asset('dashboard/editor/ckeditor/ckeditor.js')}}"></script>
<script  src="{{asset('dashboard/js/main.js')}}"></script>

<script  src="{{asset('js/image-uploader.min.js')}}"></script>
<script>
     setTimeout(function() { $('#response_msg').fadeOut('fast'); }, 5000);

     function removeMessage(){
        $("#response_msg").remove();
     }

	$(document).ready( function () {
    $('#dataTable').DataTable();


} );

CKEDITOR.replace( 'description');

// Get the modal
</script>	
</body>
</html>
<?php //ob_end_flush(); ?>

