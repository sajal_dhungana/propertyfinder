@include('admin.header')

<div id="activity_section">
<div id="left_nav">
@include('admin.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('admin/country/create')}}" enctype="multipart/form-data">

{{csrf_field()}}
      <fieldset>
        <legend>Add Country</legend>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="name"  placeholder="Type name here" required/>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Short Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="sortname"  placeholder="Type short name here" required/>
                  <span></span>
              </div>
          </div>
      
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Google Map</label>
              </div>
              <div class="view_elements">
                  <textarea name="google_map" id="" cols="30" rows="7"></textarea>
              </div>
          </div>
  
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Add to Feature *</label>
              </div>
              <div class="view_elements">
                  <select name="add_to_feature" required>
                            <option value="">--Select Add to Feature--</option>
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Order *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="ordering"  placeholder="Type name here" min="1" />
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>



@include('admin.footer')