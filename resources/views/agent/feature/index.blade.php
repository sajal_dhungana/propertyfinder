@include('agent.header')

<div id="activity_section">
<div id="left_nav">
@include('agent.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($feature);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/agent/feature/create')}}">Add New</a>
</div>

<table id="dataTable">
<thead>
<tr>
<!-- <th class="id"><input type="checkbox"></th> -->
<th>Feature</th>
<th>Icon</th>
<th class="action">Action</th>
<!-- <th class="id">Id</th> -->

</tr>
</thead>
<tbody>


@foreach($feature as $f)

<tr>
<!-- <td><input type="checkbox"/></td> -->
<td>{{$f->name}}</td>
<td>{{isset($f->icon->name)?$f->icon->name:''}}</td>
<td><a href="{{url('agent/feature/edit').'/'.$f->id}}">Edit</a> <a href="{{url('agent/feature/delete').'/'.$f->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
</tr>



@endforeach

</tbody>
</table>
</div>
</div>
</div><!-- end #activity_section -->
