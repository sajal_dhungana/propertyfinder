@include('agent.header')

<div id="activity_section">
<div id="left_nav">
@include('agent.leftnav')
</div><!-- end #left_nav -->

<div id="respons_section">

<div class="form">
    <form method="post" action="{{url('agent/propertyType/create')}}" enctype="multipart/form-data">

    {{csrf_field()}}
        <fieldset>
            <legend>Add Property Type</legend>

            <div class="view_items">
                <div class="view_labels">
                    <label for="form_name"> Name *</label>
                </div>
                <div class="view_elements">
                    <input type="text" name="name" placeholder="Type name here" required/>
                </div>
            </div>
            <div class="view_items">
                <div class="view_labels">
                    <input type="submit" value="Save" name="submit"/>
                    <input type="button" value="Cancel"
                           />
                </div>
                <div class="view_elements">

                    <span></span>
                </div>
            </div>
        </fieldset>
    </form>
</div>

</div>



@include('agent.footer')