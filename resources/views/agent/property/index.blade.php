@include('agent.header')

<div id="activity_section">
<div id="left_nav">
@include('agent.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($property);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/agent/property/create')}}">Add New</a>
</div>
<div class="data-table">
    <table id="dataTable">
        <thead>
        <tr>
        <!-- <th class="id"><input type="checkbox"></th> -->
        <th>Title</th>
        <th>Agent Name</th>
        <th>Property Type</th>
        <th>City</th>
        <th class="action">Action</th>
        <!-- <th class="id">Id</th> -->

        </tr>
        </thead>
        <tbody>


        @foreach($property as $p)

        <tr>
        <!-- <td><input type="checkbox"/></td> -->
        <td>{{$p->title}}</td>
        <td>{{isset($p->agent->name)?$p->agent->name:''}}</td>
        <td>{{isset($p->type->name)?$p->type->name:''}}</td>

        <td>{{isset($p->city->name)?$p->city->name:''}}</td>


        <td><a href="{{url('agent/property/edit').'/'.$p->id}}">Edit</a> <a href="{{url('agent/property/delete').'/'.$p->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
        </tr>



        @endforeach

        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- end #activity_section -->

@include('agent.footer')
