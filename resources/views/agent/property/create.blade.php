@include('agent.header')

<script src="{{asset('dashboard/js/jquery-1.11.2.min.js')}}"></script>
<div id="activity_section">
<div id="left_nav">
@include('agent.leftnav')
</div><!-- end #left_nav -->

<script>
      $( document ).ready(function() {
          // $('.input-images').imageUploader();
          $('.input-images-1').imageUploader();
      });
      
</script>

<div id="respons_section">

<div class="form">
<form method="post" name="form-example-1" id="form-example-1" action="{{url('agent/property/create')}}" enctype="multipart/form-data">

        <input type="hidden" name="agent_id" value="<?php if(auth::user()->id){echo auth::user()->id;}?>">

{{csrf_field()}}
      <fieldset>
        <legend>Add Property</legend>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Title *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="title"  placeholder="Type title here" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Sub Title *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="subtitle"  placeholder="Type Sub Title here" required/>
                  <span></span>
              </div>
          </div>


        
            <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Image </label>
              </div>
              <div class="view_elements">
                  <input type="file" name="image" id="image"  placeholder="Upload Image"/>
                  <span></span>
              </div>
            </div>

            <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Description *</label>
              </div>
              <div class="view_elements">
                  <textarea name="description" id="description" rows="4"  placeholder="Property Description" required></textarea>
                  <span></span>
              </div>
            </div>


            <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Video Url </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="video_url" id="video_url" placeholder="Property Video Url"></textarea>
                  <span></span>
              </div>
            </div>



          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="sold" id="sold"/> Sold </label>
                  
              </div>
              
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="featured" id="featured"/> Featured </label>
                  
              </div>
              
          </div>
        


        <div class="input-images">
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Property Gallary </label>
              </div>
              <div class="view_elements">
                  <div class="input-images-1" style="padding-top: .5rem;"></div>
                  <span></span>
              </div>
          </div>
        </div>


          

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">City Name *</label>
              </div>
              <div class="view_elements">
                  <select name="city_id" id="city_id" required>
                            <option value="">--Select City--</option>
                      @foreach($city as $c)

                        <option value="{{$c->id}}">{{$c->name}}</option>
                      @endforeach
                       
                  </select>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Property Type *</label>
              </div>
              <div class="view_elements">
                  <select name="property_type_id" id="property_type_id" onchange="getPropertyType(this)" required>
                            <option value="">--Select Property Type--</option>

                        @foreach($type as $t)
                            <option value="{{$t->id}}" title="{{$t->name}}">{{$t->name}}</option>
                        @endforeach
                       
                  </select>
                  <span></span>
              </div> 
          </div>

          <div class="view_items" id="area" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> Area </label>
                        <div class="col-md-12">
                            <input type="number" placeholder="Ropani" name="ropani">
                            <input type="number" placeholder="Aana" name="aana">
                            <input type="number" placeholder="Paisa" name="paisa">
                            <input type="number" placeholder="Daam" name="daam">
                        </div>

                </div>
            </div>

          <div class="view_items" id="rooms" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Rooms </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_rooms" id="no_of_rooms" placeholder="Type No. of Rooms here" min="1"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items" id="bedrooms" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Bedrooms </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_bedrooms" id="no_of_bedrooms" placeholder="Type No. of Bedrooms here" min="1"/>
                  <span></span>
              </div>
          </div>


          <div class="view_items" id="bathrooms" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Bathrooms </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_bathrooms" id="no_of_bathrooms" placeholder="Type No. of Bathrooms here" min="1"/>
                  <span></span>
              </div>
          </div>

          
          <div class="view_items" id="kitchens" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Kitchens </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_kitchens" id="no_of_kitchens"  placeholder="Type No. of Kitchens here" min="1"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items" id="living_rooms" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Living Rooms </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_living_rooms" id="no_of_living_rooms"  placeholder="Type No. of Living Rooms here" min="1"/>
                  <span></span>
              </div>
          </div>

          <div class="view_items" id="floors" style="display:none">
              <div class="view_labels">
                  <label for="form_name"> No of Floors </label>
              </div>
              <div class="view_elements">
                  <input type="number" name="no_of_floors" id="no_of_floors"  placeholder="Type No. of Floors here" min="1"/>
                  <span></span>
              </div>
          </div>

          
           <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Road Size *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="road_size" step="0.001"  placeholder="Type Road Size in feet" min="1" required/>
                  <span></span>
              </div>
          </div>

          
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Road Type *</label>
              </div>
              <div class="view_elements">
                  <select name="road_type_id" id="road_type_id" required>
                            <option value="">--Select Road Type--</option>

                        @foreach($roadType as $rt)
                            <option value="{{$rt->id}}">{{$rt->name}}</option>
                        @endforeach
                       
                  </select>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Property Purpose *</label>
              </div>
              <div class="view_elements">
                  <select name="property_purpose_id" id="property_purpose_id" required>
                    <option value="">--Select Property Purpose--</option>
                    @foreach($purpose as $p)
                            
                            <option value="{{$p->id}}">{{$p->name}}</option>

                    @endforeach

                        
                       
                  </select>
                  <span></span>
              </div>
          </div>
      
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Price *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="price"  placeholder="Type Price here" min="1" required/>
                  <span></span>
              </div>
          </div>


          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_hospital" id="near_hospital"/> Near Hospital </label>
                  
              </div>
              
          </div>

                    <div class="view_items" style="display:none" id="hospital">
                        <div class="view_labels">
                            <label for="form_name">Hospital Name</label>
                        </div>
                        <div class="view_elements">
                            <input type="text" name="hospital_name"  placeholder="Nearby Hospital Name"/>
                            <span></span>
                        </div>
                    </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_school" id="near_school" /> Near School </label>
              </div>
             
          </div>

                    <div class="view_items" style="display:none" id="school">
                        <div class="view_labels">
                            <label for="form_name">School Name</label>
                        </div>
                        <div class="view_elements">
                            <input type="text" name="school_name"  placeholder="Nearby School Name"/>
                            <span></span>
                        </div>
                    </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_market" id="near_market"/> Near Market </label>
              </div>
         
          </div>
                    <div class="view_items" style="display:none" id="market">
                        <div class="view_labels">
                            <label for="form_name">Market Name</label>
                        </div>
                        <div class="view_elements">
                            <input type="text" name="market_name"  placeholder="Nearby Market Name"/>
                            <span></span>
                        </div>
                    </div>


          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_bank" id="near_bank"/> Near Bank </label>
              </div>
          
          </div>

                     <div class="view_items" style="display:none" id="bank">
                        <div class="view_labels">
                            <label for="form_name">Bank Name</label>
                        </div>
                        <div class="view_elements">
                            <input type="text" name="bank_name"  placeholder="Nearby Bank Name"/>
                            <span></span>
                        </div>
                    </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_bus_stop" id="near_bus_stop"/> Near Bus Stop </label>
              </div>
              
          </div>
                        <div class="view_items" style="display:none" id="bus_stop">
                        <div class="view_labels">
                            <label for="form_name">Bus Stop Name</label>
                        </div>
                        <div class="view_elements">
                            <input type="text" name="bus_stop_name"  placeholder="Nearby Bus Stop Name"/>
                            <span></span>
                        </div>
                      </div>


          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"><input type="checkbox" name="near_airport" id="near_airport" /> Near Airport </label>
              </div>
              
          </div>
                        <div class="view_items" style="display:none" id="airport">
                        <div class="view_labels">
                            <label for="form_name">Airport Name</label>
                        </div>
                        <div class="view_elements">
                            <input type="text" name="airport_name"  placeholder="Nearby Airport Name"/>
                            <span></span>
                        </div>
                      </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Latitude *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="latitude" step='0.00001' placeholder="Type Latitude here" min="1" required/>
                  <span></span>
              </div>
          </div>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Longitude *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="longitude" step='0.00001' placeholder="Type Longitude here" min="1" required/>
                  <span></span>
              </div>
          </div>

           <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Facebook Link </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="facebook_link"  placeholder="Facebook Link" />
                  <span></span>
              </div>
          </div>

           <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Linkedin Link </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="linkedin_link"  placeholder="Linkedin Link"/>
                  <span></span>
              </div>
          </div>

           <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Twitter Link </label>
              </div>
              <div class="view_elements">
                  <input type="text" name="twitter_link"  placeholder="Twitter Link"/>
                  <span></span>
              </div>
          </div>



          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>

<script>
    $(document).on('change', '#near_hospital', function() {
          if(this.checked) {
              $('#hospital').show();
          }else{
            $('#hospital').hide();
          }
    });

    $(document).on('change', '#near_school', function() {
          if(this.checked) {
              $('#school').show();
          }else{
            $('#school').hide();
          }
    });

    $(document).on('change', '#near_market', function() {
          if(this.checked) {
              $('#market').show();
          }else{
            $('#market').hide();
          }
    });


    $(document).on('change', '#near_bank', function() {
          if(this.checked) {
              $('#bank').show();
          }else{
            $('#bank').hide();
          }
    });

    $(document).on('change', '#near_bus_stop', function() {
          if(this.checked) {
              $('#bus_stop').show();
          }else{
            $('#bus_stop').hide();
          }
    });


    $(document).on('change', '#near_airport', function() {
          if(this.checked) {
              $('#airport').show();
          }else{
            $('#airport').hide();
          }
    });

   
    function getCity(){
                $('#city_id').html('<option value="">--Select City--</option>');
                var country = document.getElementById('country_id').value;
                $.ajax({
                url: "{{url('getCity')}}"+'/'+country,
                type: 'GET',
                success: function(res) {
                    $('#city_id').html(res);
                }
            });
        
    }

    function getPropertyType(a){
        var type = $('option:selected', a).attr('title');
        if(type == 'house'){
            $('#rooms').show();
            $('#bedrooms').show();
            $('#bathrooms').show();
            $('#kitchens').show();
            $('#living_rooms').show();
            $('#floors').show();
            $('#area').show();

        }else if(type == 'room'){
            $('#rooms').show();
            $('#bedrooms').show();
            $('#bathrooms').show();
            $('#kitchens').show();
            $('#living_rooms').show();

        }else if(type == 'flat'){
            $('#rooms').show();
            $('#bedrooms').show();
            $('#bathrooms').show();
            $('#kitchens').show();
            $('#living_rooms').show();
            $('#area').show();
           
        }else{
            $('#rooms').hide();
            $('#bedrooms').hide();
            $('#bathrooms').hide();
            $('#kitchens').hide();
            $('#living_rooms').hide();
            $('#floors').hide();
            $('#area').show();
        }

        
    }
</script>

@include('agent.footer')