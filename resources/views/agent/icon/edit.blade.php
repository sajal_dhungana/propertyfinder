@include('agent.header')

<div id="activity_section">
<div id="left_nav">
@include('agent.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('agent/icon/update')}}" enctype="multipart/form-data">

{{csrf_field()}}
        <input type="hidden" name="id" value="{{$icon['id']}}">
      <fieldset>
        <legend>Edit Feature Icon</legend>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Icon *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="name" value="{{$icon['name']}}"  placeholder="Type icon class here" required/> 
                  <span></span>
              </div>
          </div>
         
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1" <?php if($icon['status'] == 1){echo 'selected';}?>>Active</option>
                            <option value="0" <?php if($icon['status'] == 0){echo 'selected';}?>>Inactive</option>
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>



@include('agent.footer')