@include('agent.header')

<div id="activity_section">
<div id="left_nav">
@include('agent.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('agent/state/create')}}" enctype="multipart/form-data">

{{csrf_field()}}
      <fieldset>
        <legend>Add State</legend>

        <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Name *</label>
              </div>
              <div class="view_elements">
                  <input type="text" name="name"  placeholder="Type name here" required/>
                  <span></span>
              </div>
          </div>

          <!-- <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Country Name *</label>
              </div>
              <div class="view_elements">
                  <select name="country_id" required>
                        <option value="">--Select Country--</option>

                        @foreach($country as $c)

                                <option value="{{$c->id}}">{{$c->name}}</option>
                        @endforeach

                  </select>
                  <span></span>
              </div>
          </div> -->


          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Google Map</label>
              </div>
              <div class="view_elements">
                  <textarea name="google_map" id="" cols="30" rows="7"></textarea>
              </div>
          </div>
          
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                 
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>



@include('agent.footer')