@include('agent.header')

<div id="activity_section">
<div id="left_nav">
@include('agent.leftnav')
</div><!-- end #left_nav -->
<div id="respons_section">



<div id="list">

<div class="list_nav">
<?php
$count= count($district);
echo "<strong>$count Record(s) found</strong>";
?>
<a href="{{url('/agent/district/create')}}">Add New</a>
</div>

<table id="dataTable">
<thead>
<tr>
<!-- <th class="id"><input type="checkbox"></th> -->
<th>District</th>
<!-- <th>Country</th> -->
<th>State</th>
<th class="action">Action</th>
<!-- <th class="id">Id</th> -->

</tr>
</thead>
<tbody>


@foreach($district as $d)

<tr>
<!-- <td><input type="checkbox"/></td> -->
<td>{{$d->name}}</td>

<td>{{isset($d->state->name)?$d->state->name:''}}</td>
<td><a href="{{url('agent/district/edit').'/'.$d->id}}">Edit</a> <a href="{{url('agent/district/delete').'/'.$d->id}}" class="confirm-delete"><i class="fa fa-trash"></i>Delete</a></td>
</tr>



@endforeach

</tbody>
</table>
</div>
</div>
</div><!-- end #activity_section -->
