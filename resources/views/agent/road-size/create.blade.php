@include('agent.header')

<div id="activity_section">
<div id="left_nav">
@include('agent.leftnav')
</div><!-- end #left_nav -->



<div id="respons_section">

<div class="form">
<form method="post" action="{{url('agent/roadSize/create')}}" enctype="multipart/form-data">

{{csrf_field()}}
      <fieldset>
        <legend>Add Road Size</legend>

          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name"> Size *</label>
              </div>
              <div class="view_elements">
                  <input type="number" name="name" step="0.001"  placeholder="Type size here" required/> <label for="form_name" style="color:black"> Feet</label>
                  <span></span>
              </div>
          </div>
         
          <div class="view_items">
              <div class="view_labels">
                  <label for="form_name">Status *</label>
              </div>
              <div class="view_elements">
                  <select name="status" required>
                  <option value="">--Select Status--</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                  </select>
                  <span></span>
              </div>
          </div>
          <div class="view_items">
              <div class="view_labels">
                  <input type="submit" name="submit"/>
                  <input type="button" value="Cancel" />
              </div>
              <div class="view_elements">

                  <span></span>
              </div>
          </div>
      </fieldset>
    </form>

</div>



@include('agent.footer')