<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php if(isset($_GET['action'])): echo ucwords($_GET['action']); else: echo "HOME"; endif; ?> | AGENT</title>
<link rel="shortcut icon" href="{{asset('images/ghar-logo.png')}}">
<link rel="stylesheet" href="{{asset('dashboard/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/css/defult.css')}}">
<link rel="stylesheet" href="{{asset('dashboard/css/style.css')}}">

<!-- <link rel="stylesheet" href="{{asset('dashboard/dataTable/media/css/custom.dataTable.css')}}"> -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="{{asset('dashboard/media/font-awesome/css/font-awesome.min.css')}}">

<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link type="text/css" rel="stylesheet" href="{{asset('css/image-uploader.min.css')}}">


</head>

<body>
    <?php 
        $notification = \App\NotificationModel::where('agent_id',auth::user()->id)->get();
        $nCount = count($notification);
    ?>
<div id="wrapper">
	<div id="top_nap">
    	<div class="top_left"><p><a href="{{url('/')}}"><i class="fa fa-home">&nbsp;&nbsp;</i><img src="{{asset('images/ghar-logo.png')}}" alt="">&nbsp;&nbsp; Agent Panel</a></p></div>
        <div class="top_center"><p><i class="fa fa-clock-o ">&nbsp;&nbsp;&nbsp;</i><i id="time"></i></p></div>
        <div class="top_right">
            <ul>
                <li class="msg">
                    <a href="{{url('agent/notification/list/'.auth::user()->id)}}"><span class="messegeNo"><i class="fa fa-envelope-o" aria-hidden="true"></i><span>{{$nCount}}</span></span></a>
                </li>
                <li><a href="{{url('home')}}"><strong>@if(Auth::user()){{Auth::user()->name}}@endif</strong></a>

                <ul>
                    <li> <a href="{{url('/agent/profile').'/'.Auth::user()->id}}">Profile</a></li>
                    <li>
                         <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form-2').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form-2" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                    </form>
                    </li>
                    <li>
                        <a href="{{url('agent/password/edit').'/'.Auth::user()->id}}"><i class="fa fa-gears">&nbsp;</i> Change Password </a>
                    </li>
                </ul>
                </li>
                
            </ul>

        </div>
  </div><!-- div id top nap closed -->