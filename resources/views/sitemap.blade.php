<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Ghargharana | Sitemap</title>
    <base href="{{url('/')}}">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{asset('images/ghar-logo.png')}}">

    <!-- core CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap-style.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/slick-slider.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-combobox.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.min.css')}}">
    <!-- <script src="{{asset('js/jquery-1.12.0.min.js')}}"></script> -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

   <style>
       body {
    margin-left: 0;
    margin-top: 0;
    margin-right: 0;
    margin-bottom: 0;
    background-image: url('{{asset('images/pageBg.gif')}}');
    background-repeat: repeat-x;
    font-family: Tahoma,Geneva,sans-serif;
    background: #f8f8f8;
}
    </style>
</head>
<body>

<div class="sitemap-container">
    <div class="greyarea"></div>
    <div class="logo-container">
        <a href="{{url('/')}}" class="mbSiteLogo">
            <img src="{{asset('images/ghar-logo.png')}}" alt="Ghargharana logo">
        </a>
    </div>
    <div class="container">
        <div class="site-container-wrap">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('/')}}">Home</a>

                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Sitemap</li>
                </ol>
            </nav>
            <h1>Sitemap</h1>
            <div id="mainSitemap">
                <div class="row">
                    <div class="col-6">
                    
                        <div class="generalSL">
                            <h2>General Site Links</h2>
                            <ul>
                                <li>
                                    <a target="_blank" href="{{url('/')}}">Home</a>
                                </li>

                                <li>
                                    <a href="{{url('/careers')}}" target="_blank">Careers</a>
                                </li>
                                <li>
                                    <a href="{{url('/contact')}}" target="_blank">Contact Us</a>
                                </li>
                                <li>
                                    <a href="{{url('/faq')}}" target="_blank">FAQ</a>
                                </li>
                                <li>
                                    <a href="{{url('/policy')}}" target="_blank">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="{{url('/terms')}}" target="_blank">Terms & Conditions</a>
                                </li>
                                <li>
                                    <a href="{{url('/blogs')}}" target="_blank">Blogs</a>
                                </li>
                                
                                <li><a target="_blank" href="{{url('register')}}">Register Now</a></li>
                                <li><a target="_blank" href="{{url('login')}}">Login</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="searchPI">
                            <h2>Search for Properties in New</h2>
                            <ul>
                                <li><a href="{{url('/')}}" target="_blank">Search for Property</a></li>
                                
                                <li><a target="_blank" href="{{url('agent')}}">Real Estate Agents in Nepal</a></li>
                               
                            </ul>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="BSProperty">
                            <h2>Buying &amp; Selling Property</h2>
                            <ul>
                                <li><a target="_blank" href="{{url('buy')}}">Sell Property</a></li>
                                <li><a target="_blank" href="{{url('rent')}}">Rent Property</a></li>
                                <li><a href="<?php if(auth::user()){echo url('agent/property/create');}else{echo route('login');}?>" target="_blank">Post Property</a></li>

                               <!--  <li><a target="_blank" href="/Property-Rates-Trends">Property Rates &amp; Trends</a></li>
                                <li><a target="_blank" href="/buyers-guide/">Buyers Guide</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
    </div>
    <div id="footerWrap">
        <div class="copyright">All trademarks, logos and names are properties of their respective owners. All Rights Reserved. © Copyright 2020 Ghargharana Realstate Services Limited.</div>
    </div>
</div>

<!-- <script src="{{asset('js/jquery-1.12.0.min.js')}}"></script> -->
<script src="{{asset('js/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>

<script src="{{asset('js/slick.js')}}"></script>
<script src="{{asset('js/bootstrap-combobox.js')}}"></script>
<script src="{{asset('js/ajax.js')}}"></script>
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>