@include('includes.header')


<div class="main-content user-dashboard">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="left-block">
                    <div class="row">
                        <div class="col-sm-7 col-md-8">
                            <div class="user-info">
                                <h2>{{$agent['name']}}</h2>
                                <ul>
                                    <li><span>Email Address : </span>{{$agent['email']}}</li>
                                @if($agent['address'])
                                    <li><span> Address : </span>{{$agent['address']}}</li>
                                @endif


                                @if($agent['contact_num'])
                                    <li><span> Contact Number : </span>{{$agent['contact_num']}}</li>
                                @endif

                                @if($agent['contact_num1'])
                                    <li>{{$agent['contact_num1']}}</li>
                                @endif


                                </ul>
                            </div><!--end .user-info-->
                        </div><!--end .col-sm-8-->
                        <div class="col-sm-5 col-md-4">
                            <div class="user-profile-img">
                                <img src="{{asset('uploads/agents/'.$agent['image'])}}" alt="<?php $agent['name']; ?> photo">
                            </div><!--end .user-profile-img-->
                        </div><!--end .col-sm-4 col-md-3-->

                    </div><!--end .row-->
                </div><!--end .left-block-->
            </div><!--end .col-sm-5 col-md-6-->
            <div class="col-sm-4">
<!--                <div class="user-setting">-->
<!--                    <ul>-->
<!--                        <li><a href="#">Add Property</a></li>-->
<!--                        <li><a href="#">Edit Profile</a></li>-->
<!--                        <li><a href="#">Change Password</a></li>-->
<!--                    </ul>-->
<!--                </div><!--end .summery-->
            </div><!--end .col-sm-3 col-sm-3-->
        </div><!--end .row-->

        <div class="agent-property-list">
            <h2>My Properties</h2>
            <div class="property-list-table">
                <table class="table">
                    <thead>
                      <tr>
                        <th>SN</th>
                        <th>Property Name</th>
                        <th>Property Type</th>
                        <th>Property For</th>
                        <th>Status</th>
                          <th>Added Date</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                     
                       $sn = 1;
                    ?>

                    @foreach($property as $p)
                      <tr>
                        <td><?php echo $sn++; ?>. </td>
                        <td><a href="{{url('/agent/description').'/'.$p->id}}">{{$p->title}}</a></td>
                        <td>{{isset($p->type->name)?$p->type->name:''}}</td>
                        <td>{{isset($p->purpose->name)?$p->purpose->name:''}}</td>
                          <td><?php if($p->status == 1){echo 'Active';}else{echo 'Incative';}?></td>
                          <td><?php echo (date( 'd M, Y', strtotime($p->created_at))); ?></td>
                      </tr>


                    @endforeach
                    </tbody>
                  </table>
            </div><!--end .property-list-tab-->
        </div><!--end .agent-property-list-->
    </div><!--end .container-->
</div><!--end .main-content-->

@include('includes.footer')