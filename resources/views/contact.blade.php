@include('includes.header')
<?php  $socialSettings = \App\SocialSettingModel::first(); ?>

<main role="main">
    <section class="contact-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner-width white-color">
                        <h1 class="subpage-main-title white-color">Get In Touch</h1>
                        <div class="cont-desc">You have got questions about property,<br/>we have got answers on Ghargharana.</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="form-info">
        <div class="container">
            <div class="inner-width contact-shadow">
                <div class="row">
                    <div class="col-md-8 col-sm-12 pr-none">

                        <div id="response_msg" style="color:green">
                            @if(Session::has('message'))
                                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {{session('message')}}</em></div>
                            @endif
                        </div>

                        <form class="form-contact" action="{{url('contact')}}" method="post" enctype="multipart/form-data">

                            {{csrf_field()}}
                            <h2 class="title2">Send us a Message</h2>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="name">Name *:</label>
                                        <input type="text" name="name" class="form-control" id="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone *:</label>
                                        <input type="number" name="phone" class="form-control" id="phone" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="email">Email address *:</label>
                                        <input type="email" class="form-control" id="email" name="email" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="company">Company :</label>
                                        <input type="text" class="form-control" id="company" name="company">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="message">Message *:</label>
                                <textarea id="message" name="message" rows="10" cols="50" placeholder="Please type your message here..." required></textarea>
                            </div>
                            <button type="submit" class="contact-btn">Submit</button>
                        </form>
                    </div>
                    <div class="col-md-4 col-sm-12 pl-none">
                        <div class="contact-info">
                            <h1 class="title2 white-color">Contact Information</h1>
                            <div class="cont-desc">
                                <ul class="cont-info-list">
                                    <li>
                                        <i class="fas fa-globe-asia"></i>
                                        <div class="contact-address">1st Floor, New Baneshwor-31 <br/>Kathmandu, Nepal</div>
                                    </li>
                                    <li>
                                        <i class="fas fa-mobile" aria-hidden="true"></i>
                                        <div class="contact-phone">9808376133, 9851127022</div>
                                    </li>
                                    <li>
                                        <i class="far fa-envelope-open"></i>
                                        <div class="contact-email">info@ghargharana.com</div>
                                    </li>
                                </ul>
                               
                                
                            </div>
                            <div class="social-links text-center">
                                    <ul>
                                        <li><a href="{{$socialSettings['site_facebook']}}" class="fab fa-facebook-f" target="_blank"></a></li>
                                        <li><a href="{{$socialSettings['site_twitter']}}" class="fab fa-twitter" target="_blank"></a></li>
                                        <li><a href="{{$socialSettings['site_youtube']}}" class="fab fa-youtube" target="_blank"></a></li>
                                        <li><a href="{{$socialSettings['site_instagram']}}" class="fab fa-instagram" target="_blank"></a></li>
                                    </ul>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="maps">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d5941.666816554916!2d85.33258801611474!3d27.688365876044813!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1snaya%20baneshwor%20citizen%20bank!5e0!3m2!1sen!2snp!4v1607505696913!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>


@include('includes.footer')