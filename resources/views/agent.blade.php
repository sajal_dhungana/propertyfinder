@include('includes.header')



<section class="main-banner" style="background-image: url({{asset('images/banner-bg.jpg')}});">
    <div class="container">
        <div class="carousel-cap-wrap">
            <div class="search-form">
                <h2>Find the agent you prefer.</h2>
                <p>Search agents for sale and to rent.</p>
                <form action="{{url('searchAgent')}}" method="post">
                {{csrf_field()}} 
                    <div class="row">
                        

                            <div class="col-md-3 col-xs-3">
                                <div class="form-group">
                                    <select class="form-control" name="agent_type_id" id="agent_type_id">
                                        <option value="">--Agent Type--</option>

                                        @foreach($agentType as $t)
                                            <option value="{{$t->id}}">{{$t->type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
            

                            <div class="col-md-3 col-xs-3">
                                <div class="form-group">
                                    <select class="form-control" name="city_id" id="city_id">
                                        <option value="">--Location--</option>

                                        @foreach($city as $c)
                                            <option value="{{$c->id}}">{{$c->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3 col-xs-3">
                                <div class="form-group">
                                    <input type="text" name="agent_name" id="agent_name" placeholder="Agent Name">
                                </div>
                            </div>


                            <div class="col-md-3 col-xs-3">
                                <div class="form-group">
                                    <button type="submit" class="btn-button" name="search">Search</button>
                                    <!-- <button type="submit" class="btn-button" name="search_sale">For sale</button>
                                    <button type="submit" class="btn-button" name="search_rent">To rent</button> -->
                                </div>
                            </div>
                        
                    </div><!--end .row-->
            
                </form>
    
            </div><!--end .search-form-->

        </div><!--end .carousel-cap-wrap-->
    </div><!--end .container-->
</section>
<script>

    
</script>




<section class="list-feature">
    <h1 style="text-align: center;margin-top: 30px"> Agents List </h1>
    <div class="container">
        <div class="row">
            @foreach($agent as $a)
                <div class="col-md-3 col-sm-6">
                    <div class="search_property" id="search_property">
                        
                        <div class="feature-items">
                    
                            <div class="item">
                                <div class="item-wrap">
                                    <div class="item-img">
                                        <a href="{{asset('uploads/agents/'.$a->image)}}"><img src="{{asset('uploads/agents/'.$a->image)}}"></a>
                                        <span> {{isset($a->type->type)?$a->type->type:''}}
                                        </span>
                                    </div><!--end .item-img-->
                                    <div class="item-title">
                                        <p> </p>
                                    </div><!--end .item-title-->
                                    <div class="item-content">
                                        <p>
                                                {{ucfirst($a->name)}}
                                        </p>
                                        @if($a->contact_num)<p>Contact No. : {{$a->contact_num}}</p>@endif
                                    </div><!--end .item-content-->
                                    <div class="item-footer">
                                        <a href="{{url('/agent/description').'/'.$a->id}}">View Detail</a>
                                    </div><!--end .item-footer-->
                                </div><!--end .item-wrap-->
                            </div><!--end .item col-sm-3-->
                    
                        </div><!--end .feature-item-->
                        
                    </div>
                </div><!--end .col-sm-3-->
            @endforeach
        </div><!--end .row-->
    </div><!--end .container-->
</section><!--end .list-feature-->






    <section class="list-category">
        <div class="container">
            <div class="row">
                <div class="item col-md-3 col-sm-6">
                    <a href="#"><i class="fa fa-home" aria-hidden="true"></i></a>
                    <h3><a href="#">Sold house prices</a></h3>
                    <p>See what property in your local area sold for.</p>
                    <a href="#" class="signup">Search now</a>
                </div><!--end .item col-sm-3-->
                <div class="item col-md-3 col-sm-6">
                    <a href="#"> <i class="fa fa-building" aria-hidden="true"></i></a>
                    <h3><a href="#">Commercial property</a></h3>
                    <p>Search over 49,000 commercial properties to buy or rent.</p>
                    <a href="#" class="signup">Search now</a>
                </div><!--end .item col-sm-3-->
                <div class="item col-md-3 col-sm-6">
                    <a href="#" data-toggle="modal" data-target="#modalLoginRegister" class="popUp"><i class="fa fa-money" aria-hidden="true"></i></a>
                    <h3><a href="#" data-toggle="modal" data-target="#modalLoginRegister" class="popUp">Buy-to-let investors</a></h3>
                    <p>Sign up to receive potential investment and auction property.</p>
                    <a href="#" data-toggle="modal" data-target="#modalLoginRegister" class="popUp signup">Sign up now</a>
                </div><!--end .item col-sm-3-->
                <div class="col-md-3 col-sm-6 social-links">
                    <strong>Stay connected</strong>
                    <ul class="mt-4">
                        <li><a href="#" class="fa fa-facebook"></a></li>
                        <li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-youtube"></a></li>
                        <li><a href="#" class="fa fa-instagram"></a></li>
                    </ul>
                </div><!--end .col-sm-3-->
            </div><!--end .row-->
        </div><!--end .container-->
    </section><!--end .list-category-->

@include('includes.footer')