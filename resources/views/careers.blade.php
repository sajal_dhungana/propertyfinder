@include('includes.header')

<main role="main">
  <section class="banner-top" style="background-image: url('{{asset('images/bg_snippet.jpg')}}');background-size:cover;">
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-md-6 banner-left">
          <h1 class="title-3 fw-300 mb-2">Be a part of <b>Development</b></h1>
          <h3 class="title-light mb-0">Join us and become the leading person you want to be.</h4>
          <a href="#" class="mt-4 mb-2 btn banner-link">GET STARTED</a>
        </div>
        <div class="col-md-6 mb-6 banner-img">
          <img src="{{asset('images/website_blog.png')}}" alt="">
        </div>
      </div>
    </div>
  </section>
  <section class="career-text">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="career-text-wrap">
            <h3 class="big-title text-center">You @ Ghargharana</h3>
            <div class="career-text-desc">
            We are not looking for another brick in the wall. If you have what it takes and can give us compelling reasons that make you stand out from the rest (for all the right reasons, of course!), then we’d like to hear from you.
            </div>
            <div class="img-wrap">
              <img src="{{asset('images/careerBanner.jpg')}}" alt="Ghar Gharana career banner">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="reasons-with-us">
    <div class="container">
      <div class="row">
        <div class="col-12"><h3 class="big-title text-center">Reasons to work with us</h3></div>
        <div class="col-md-6 col-xs-12 reason-desc-wrap">
          <div class="reason-img"><img src="{{asset('images/peers.png')}}" alt="Fantastic Peers"></div>
          <div class="reason-desc">
            <h4 class="reason-title">Fantastic Peers</h4>
            <p>We are a team of dreaming doers who never settle. Our folks are humble and ambitious, tenacious and jolly, fast and aware.</p>
          </div>
        </div>
        <div class="col-md-6 col-xs-12 reason-desc-wrap">
          <div class="reason-img"><img src="{{asset('images/growth.png')}}" alt="Supersonic Growth"></div>
          <div class="reason-desc">
            <h4 class="reason-title">Supersonic Growth</h4>
            <p>When you don't compromise, you maximise. At Housing, you will be challenged every single day to become better than your best.</p>
          </div>
        </div>
        <div class="col-md-6 col-xs-12 reason-desc-wrap">
          <div class="reason-img"><img src="{{asset('images/change.png')}}" alt="Change the World"></div>
          <div class="reason-desc">
            <h4 class="reason-title">Change the World</h4>
            <p>We work to make the world a better place. In this epic journey, we are always looking for torchbearers</p>
          </div>
        </div>
        <div class="col-md-6 col-xs-12 reason-desc-wrap">
          <div class="reason-img"><img src="{{asset('images/fun.png')}}" alt="Fun Times"></div>
          <div class="reason-desc">
            <h4 class="reason-title">Fun Times</h4>
            <p>Surprise Parties, Weekend Cricket, Hackathons, Festival Celebrations, Wellness Camps... never a dull day.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="openPostion">
    <div class="container">
      <div class="row">
        <div class="col-12">

          <div id="response_msg" style="color:green">
            @if(Session::has('message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {{session('message')}}</em></div>
            @endif
          </div>

          <h3 class="big-title text-center">Join With Us</h3>
          <p class="text-center">Check them out here Open Positions. We'll be happy to hear from you.<br>
If you’re interested to work with us, just send across your resume to us by clicking on the button below.</p>
                    <!-- Button trigger modal -->
          <div class="text-center">
            <button type="button" class="btn btn-info text-center" data-toggle="modal" data-target="#applyNowModal">
              Apply Now
            </button>
          </div>
          <!-- Modal -->
          <div class="modal fade" id="applyNowModal" tabindex="-1" role="dialog" aria-labelledby="applyNowModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-center" id="applyNowModalLabel">Relationship Manager - Primary Sales (2)</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form action="{{url('careers')}}" method="post" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div class="modal-body">
                    
                      <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" aria-describedby="name" placeholder="Full Name" required>
                      </div>
                      <div class="form-group">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                      </div>
                      <div class="form-group">
                        <input type="number" class="form-control" id="phone" name="phone" placeholder="Phone" required>
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" id="position" name="position" placeholder="Position" required>
                      </div>
                      <div class="form-group">
                        <textarea class="form-control" id="tell_us" name="tell_us" rows="5" required>Tell us about yourself</textarea>
                      </div>
                      <div class="form-group">
                        <label for="cv">Add Your CV *</label>
                        <input type="file" class="form-control-file" id="cv" name="cv" required>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>

@include('includes.footer')