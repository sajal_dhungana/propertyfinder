@include('includes.header')

<main role="main">
    <section class="pricing-plan">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h1 class="subpage-main-title mb-5">Ghargharana Member Plan</h1>
                </div>
            </div>

            <div class="row">
                @foreach($planDetails as $p)
                    <div class="col-md-3 col-sm-6">
                        <div class="pricingTable">
                            <div class="pricingTable-header">
                                <h3 class="heading">{{isset($p->plan->name)?$p->plan->name:''}}</h3>
                                <span class="subtitle"></span>
                                <div class="price-value">{{isset($p->plan->price)?$p->plan->price:''}}
                                    <span class="currency">Rs.</span>
                                    <span class="month">/mo</span>
                                </div>
                            </div>

                            <ul class="pricing-content">

                                <?php
                    $features = explode(',',$p->plan_feature);
                    // dd($features);
                    foreach($features as $pf){

                        $feature = \App\PlanFeatureModel::where('id',$pf)->first();
                    ?>
                            <li>{{$feature->name}}</li>

                    <?php    
                    }
                ?>
                            </ul>
                            <a href="{{url('register')}}" class="price-signUp">sign up<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</main>

@include('includes.footer')