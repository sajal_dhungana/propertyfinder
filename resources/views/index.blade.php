@include('includes.header')

<style>


</style>

@include('includes.banner')

	<script>
function getCity(){
				$('#city_id').html('<option value="">--Select City--</option>');
				var country = document.getElementById('country_id').value;
				$.ajax({
				url: "{{url('getCity')}}"+'/'+country,
				type: 'GET',
				success: function(res) {
					$('#price_id').html(res);
				}
			});
		
	}

	function getMinMaxPrice(){
		$('#min_price').html('<option value="">--Min Price--</option>');
		$('#max_price').html('<option value="">--Max Price--</option>');
		var property_type_id = document.getElementById('property_type_id').value;
		$.ajax({
		url: "{{url('getMinMaxPrice')}}"+'/'+property_type_id,
		type: 'GET',
		success: function(res) {
			console.log(res);

				 for (var i = 0; i < res.length; i++) {
							  
							  $('#min_price').append("<option value="+res[i].min_price+">"+res[i].min_price+"</option>");
							  $('#max_price').append("<option value="+res[i].max_price+">"+res[i].max_price+"</option>");


							}
			// $('#price_id').html(res);
		}
	});
		
	}
</script>


<!-- start of Ad Slider -->
<section class="ads">
	<h1 style="text-align: center;margin-top: 30px">Featured Ads Ghar Gharana</h1>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="ads-gallery">
						<div class="owl-carousel owl-theme">

						@foreach($featured as $f)
							<div class="item">
								<a href="{{url('property/description/'.$f->id)}}">
									<img class="img-fluid" src="{{asset('uploads/property/'.$f->image)}}" alt="">
									<div class="ads-desc">
										<div class="feat-title">{{ucfirst($f->title)}} </div>
										<div class="uploader-name">
											@if(isset($f->agent->name))
																 
																	
												by	{{$f->agent->name}}
																	
											@endif

										</div>
										<div class="featured-type">
											<!-- 1,2 BHK -->
										</div>

										<div class="location">

											@if(isset($f->city->name))
																 
																	
																		{{ucfirst($f->city->name)}}
																	
															@endif
										</div>
										<i class="fa fa-eye ">{{$f->views}}</i>
										<div class="ads-feat-price">
											<div class="currency">₹</div>{{$f->price}}
										</div>
											
									</div>
								</a>
							</div>
						@endforeach

							
						</div>
						<!-- End of ads-gallery -->
					</div>
				</div>
			</div>
			<!-- End of row -->
		</div>
</section>



<!--/.Carousel Wrapper-->

<section class="mt-4">
	<div class="container">
	<div class="row">
                    <div class="col-12">
                        <div class="top-ads">
                        	@if(isset($adTop))
                            <div class="top-ads-left">
                            	<a href="{{$adTop['link']}}"><img src="{{asset('uploads/ad/'.$adTop['image'])}}" alt=""></a>
                            </div>
                            @endif
                            <!-- <div class="top-ads-right"><a href="#"><img src="{{asset('images/NMB-BOY-2020.gif')}}" alt=""></a></div> -->
                        </div>
                    </div>
                </div>
	</div>
</section>



	<section class="list-feature">
		<h1 style="text-align: center;margin-top: 30px"> Property List </h1>
		<div class="container">
			<div class="row">
				@foreach($property as $p)
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="search_property" id="search_property">	
							<div class="feature-items">
								<div class="item">
									<div class="item-wrap">
										<a href="{{url('/property/description').'/'.$p->id}}">
											<div class="item-img">
												<img src="{{asset('uploads/property/'.$p->image)}}">
												<?php  
														$time = strtotime($p->created_at);

														$postDate = date('M d Y', $time);
												?>
												<span> 
														@foreach($purpose as $pp)
																@if($pp->id == $p->property_purpose_id)

																	{{ucfirst($pp->name)}} 

																	<!-- <br>
																	Posted: {{$postDate}} -->
																@endif

														@endforeach
												</span>
												<div class="sold">
														@if($p->sold)<?php echo 'Used';?> @endif
												</div>
											</div>
											<div class="item-content">
												<p class="title">
													Rs. {{$p->price}} 
													<span>
														<i class="fa fa-eye ">{{$p->views}}</i>
														<i class="fa fa-external-link" aria-hidden="true"></i>
													</span>
												</p>
												<p class="land">
													@if($p->ropani || $p->aana || $p->paisa)
																	Land:
														<span>
															@if($p->ropani){{$p->ropani}} @endif

																@if($p->aana){{$p->aana}}
																@endif

																@if($p->paisa){{$p->paisa}} @endif

																@if($p->daam){{$p->daam}} @endif

														</span>

																		
																	
													@endif 

													@if($p->road_size)
													
														<span class="land-road">
														Road: 
															{{$p->road_size}} feet
														</span>
													@endif
												</p>

												<p class="road">
													{{$p->address}} 

													@if(isset($p->city->name))
															
															
																{{$p->city->name}}
															
													@endif
												</p>
												<p class="seller">
													@if(isset($p->agent->name))
																	
																	
													{{$p->agent->name}}
																	
											

													<span style="float:right;">ID {{$p->id}}
													</span>

													@endif

												</p>
														
											</div><!--end .item-wrap-->
										</a>
									</div><!--end .item col-sm-3-->
							   
								</div><!--end .feature-item-->
							</div>
						
					
				

					</div><!--end .row-->
				</div>
				@endforeach
			</div>

			<!-- Land for Sale -->
			<h1 style="text-align: center;margin-top: 30px"> Land For Sale</h1>
			<div class="row">
				@foreach($landForSale as $s)
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="search_property" id="search_property">
							<div class="feature-items">
							   	<div class="item">
									<div class="item-wrap">
										<a href="{{url('/property/description').'/'.$s->id}}">
											<div class="item-img">
												<img src="{{asset('uploads/property/'.$s->image)}}">
												<?php  
														$time = strtotime($s->created_at);

														$postDate = date('M d Y', $time);
													
												?>
												<span> 
													@foreach($purpose as $pp)
														@if($pp->id == $s->property_purpose_id)

														{{ucfirst($pp->name)}} 

																		<!-- <br>
																		Posted: {{$postDate}} -->
														@endif

													@endforeach
												</span>
												<div class="sold">
														@if($s->sold)<?php echo 'Used';?> @endif
												</div>
											</div>
											<div class="item-content">
												<p class="title">
													Rs. {{$s->price}} 
													<span>
														<i class="fa fa-eye ">{{$s->views}}</i>
													<i class="fa fa-external-link" aria-hidden="true"></i>
													</span>

												</p>
												<p class="land">
													@if($s->ropani || $s->aana || $s->paisa)
																Land:
														<span>
															@if($s->ropani){{$s->ropani}} @endif

																@if($s->aana){{$s->aana}}
																@endif

															@if($s->paisa){{$s->paisa}} @endif

															@if($s->daam){{$s->daam}} @endif

														</span>

																	
																
													@endif 

													@if($s->road_size)
														
														<span class="land-road">
														Road: 
														{{$s->road_size}} feet
														</span>
													@endif
												</p>

												<p class="road">
													{{$s->address}} 

													@if(isset($s->city->name))
															
															
																{{$s->city->name}}
															
													@endif
												</p>

												<p class="seller">
													@if(isset($s->agent->name))
																		
																			
													{{$s->agent->name}}
														<span style="float:right;">ID {{$s->id}}

														</span>
												
													@endif		

												</p>
													
											</div><!--end .item-wrap-->
										</a>
									</div><!--end .item col-sm-3-->
							   
								</div><!--end .feature-item-->
							</div>
						
					
				

						</div><!--end .row-->
					</div>
				@endforeach
			</div>

			<!--End Land for Sale -->

			<!-- Land for Rent -->
			<h1 style="text-align: center;margin-top: 30px">House For Sell </h1>
			<div class="row">
				@foreach($houseForSell as $hs)
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="search_property" id="search_property">
						<div class="feature-items">
							<div class="item">
								<a href="{{url('/property/description').'/'.$hs->id}}">
									<div class="item-wrap">
										<div class="item-img">
											<img src="{{asset('uploads/property/'.$hs->image)}}">
											<?php  
													$time = strtotime($hs->created_at);

													$postDate = date('M d Y', $time);
												
											?>
											<span> 
												@foreach($purpose as $pp)
														@if($pp->id == $hs->property_purpose_id)

															{{ucfirst($pp->name)}} 

															<!-- <br>
															Posted: {{$postDate}} -->
														@endif

												@endforeach
											</span>
											<div class="sold">
													@if($hs->sold)<?php echo 'Used';?> @endif
											</div>
										</div>
										<div class="item-content">
											<p class="title">
												Rs. {{$hs->price}} 
												<span>
													<i class="fa fa-eye ">{{$hs->views}}</i>
												<i class="fa fa-external-link" aria-hidden="true"></i>
												</span>
											</p>
											<p class="land">
												@if($hs->ropani || $hs->aana || $hs->paisa)
												Land:
												<span>
														@if($hs->ropani){{$hs->ropani}} @endif

														@if($hs->aana){{$hs->aana}}
														@endif

														@if($hs->paisa){{$hs->paisa}} @endif

														@if($hs->daam){{$hs->daam}} @endif

												</span>
												@endif 

												@if($hs->road_size)
														
													<span class="land-road">
														Road: 
														{{$hs->road_size}} feet
													</span>
												@endif
											</p>
											<p class="road">
												{{$hs->address}} 

												@if(isset($hs->city->name))
														
														
															{{$hs->city->name}}
														
												@endif
											</p>
											<p class="seller">
												@if(isset($hs->agent->name))
																		
																		
												{{$hs->agent->name}}
																		
												

												<span style="float:right;">ID {{$hs->id}}

												</span>

												@endif
															
											</p>
										</div><!--end .item-wrap-->
									</div><!--end .item col-sm-3-->
								</a>
							</div><!--end .feature-item-->
						</div>
						
					
				

				</div><!--end .row-->
			</div>
				@endforeach
		</div>

			<!--End Land for Rent -->

				<!-- Land for Rent -->
			<h1 style="text-align: center;margin-top: 30px"> House For Rent </h1>
			<div class="row">
				@foreach($houseForRent as $hr)
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="search_property" id="search_property">
						
							<div class="feature-items">
							   
									<div class="item">
										<div class="item-wrap">
											<a href="{{url('/property/description').'/'.$hr->id}}">
												<div class="item-img">
													<img src="{{asset('uploads/property/'.$hr->image)}}">

													  <?php  
															$time = strtotime($hr->created_at);

															$postDate = date('M d Y', $time);
														
														?>
														<span> 
																@foreach($purpose as $pp)
																		@if($pp->id == $hr->property_purpose_id)

																			{{ucfirst($pp->name)}} 

																			<!-- <br>
																			Posted: {{$postDate}} -->
																		@endif

																@endforeach


														</span>

														<div class="sold">
																@if($hr->sold)<?php echo 'Used';?> @endif
														</div>
												</div>
										   
												<div class="item-content">
													<p class="title">
														Rs. {{$r->price}} 
															<span>
																<i class="fa fa-eye ">{{$r->views}}</i>
															<i class="fa fa-external-link" aria-hidden="true"></i>
															</span>

														


													</p>
													
													<p class="land">
														@if($hr->ropani || $hr->aana || $hr->paisa)
															Land:
																<span>
																		@if($hr->ropani){{$hr->ropani}} @endif

																		@if($hr->aana){{$hr->aana}}
																		@endif

																		@if($hr->paisa){{$hr->paisa}} @endif

																		@if($hr->daam){{$hr->daam}} @endif

																</span>

																
															
																@endif 

																@if($hr->road_size)
																	
																<span class="land-road">
																Road: 
																{{$hr->road_size}} feet
																</span>
																@endif
													</p>

													<p class="road">
														{{$hr->address}} 

														@if(isset($hr->city->name))
																
																
																	{{$hr->city->name}}
																
														@endif
													</p>

													<p class="seller">
													@if(isset($hr->agent->name))
																		
																			
													{{$hr->agent->name}}
																			
													

													<span style="float:right;">ID {{$hr->id}}
													</span>

													@endif
																
													</p>
													
												</div><!--end .item-wrap-->
											</a>
										</div><!--end .item col-sm-3-->
							   
									</div><!--end .feature-item-->
							</div>
						
					
				

					</div><!--end .row-->
				</div>
				@endforeach 
			</div>

			<!--End Land for Rent -->


		</div><!--end .container-->
	</section><!--end .list-feature-->
	<section class="featured-agencies mb-5">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1 style="text-align: center;margin-top: 30px"> Featured Agencies </h1>
					<div class="agents-gallery mt-5">
						<div class="owl-carousel owl-theme">

						@foreach($agency as $a)
							<div class="img-wrap agenciesTooltip" id="agenciesTooltip_{{$a->id}}" data-placement="top" data-html="true" title="<div class='tooltip-wrap'><div class='tooltip-img-wrap'><img src='{{asset('uploads/agency/'.$a->image)}}'/></div><div class='content-wrap'><div class='agency-name'>{{$a->name}}</div><div class='location'><i class='fas fa-map-marker-alt'></i> {{$a->location}}</div><div class='contact'><i class='fas fa-phone-alt'></i> {{$a->phone}}</div></div></div>" >
									<img class="img-fluid" src="{{asset('uploads/agency/'.$a->image)}}" alt="">
								</div>
						@endforeach
							
						</div>
					</div>	
					<!--end .list-feature-->
				</div><!--end col-12 -->
			</div><!--end .row -->
		</div><!--end .container -->
	</section><!--end .featured-agencies -->

@include('includes.footer')