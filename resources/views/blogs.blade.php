@include('includes.header')
<!-- 
    <div class="banner-top">
            <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-md-6 banner-left">
                <h1 class="title-3 fw-300">Property in <b>Kathmandu Valley</b></h1>
                <h3 class="title-light">Why Kathmandu’s land prices continue to skyrocket</h4>
                <a href="#" class="mt-4 mb-2 btn banner-link">Read more</a>
                </div>
                <div class="col-md-6 mb-6 banner-img">
                <img src="{{asset('images/website_blog.png')}}" alt="">
                </div>
            </div>
            </div>
        </div>
 -->
<main role="main">
    <div class="featuredBlogCarousel">
        <div class="owl-carousel owl-theme">

        @foreach($featured as $f)
            <div class="item">
                <div class="banner-top blog-banner">
                    <div class="container">
                        <div class="row d-flex align-items-center">
                            <div class="col-md-8 col-sm-6 banner-left">
                            <h1 class="title-3 fw-300"><b>{{$f->title}}</b></h1>
                            <h3 class="title-light"><?php echo substr($f->description,0,200).'...';?></h4>
                            <a href="{{url('blog/details/'.$f->id)}}" class="mt-4 mb-2 btn banner-link">Read more</a>
                            </div>
                            <div class="col-md-4 col-sm-6 mb-6 banner-img">
                            <img src="{{asset('uploads/blogs/'.$f->image)}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        </div>
    </div>

    <section class="blog-list">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-md-12"><h1 class="subpage-main-title">Read our more articles</h1></div>
                
                @foreach($blogs as $b)
                    <div class="col-12 col-md-3">
                        <div class="card mb-2">
                            <a href="{{url('blog/details/'.$b->id)}}" class="image-box"><img class="card-img-top" src="{{asset('uploads/blogs/'.$b->image)}}"
                            alt="Card image cap"></a>
                            <div class="card-body">
                            <h4 class="card-title"><a href="{{url('blog/details/'.$b->id)}}"><?php echo substr($b->title,0,50).'...';?></a></h4>
                            <div class="card-text-wrap">
                                <div class="card-text"><?php echo substr($b->description,0,200).'...';?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                    
            </div>
            <!-- <div class="row">
                <div class="col-md-12 mt-4">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-end">
                            <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </nav>
                </div>
            </div> -->
        </div>

        <?php echo $blogs->render(); ?>
    </section>
</main>

@include('includes.footer')