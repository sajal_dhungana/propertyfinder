@include('includes.header')
<div class="main-content pt-0">
        <div class="inner-content">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="top-ads">

                        @if(isset($adTop))
                            <div class="top-ads-left"><a href="{{$adTop['link']}}"><img src="{{asset('uploads/ad/'.$adTop['image'])}}" alt=""></a>
                            </div>
                        @endif
                            <!-- <div class="top-ads-right"><a href="#"><img src="{{asset('images/NMB-BOY-2020.gif')}}" alt=""></a></div> -->
                        </div>
                    </div>
                </div>

@include('includes.banner')


    <script>
function getCity(){
                $('#city_id').html('<option value="">--Select City--</option>');
                var country = document.getElementById('country_id').value;
                $.ajax({
                url: "{{url('getCity')}}"+'/'+country,
                type: 'GET',
                success: function(res) {
                    $('#price_id').html(res);
                }
            });
        
    }

    function getMinMaxPrice(){
        $('#price_id').html('<option value="">--Minimum Price--</option>');
        var property_type_id = document.getElementById('property_type_id').value;
        $.ajax({
        url: "{{url('getMinMaxPrice')}}"+'/'+property_type_id,
        type: 'GET',
        success: function(res) {
            console.log(res);

                 for (var i = 0; i < res.length; i++) {
                              
                              $('#min_price').append("<option value="+res[i].min_price+">"+res[i].min_price+"</option>");
                              $('#max_price').append("<option value="+res[i].max_price+">"+res[i].max_price+"</option>");


                            }
            // $('#price_id').html(res);
        }
    });
        
    }
</script>

<div class="row">
    <div class="col-md-9">
        <section class="list-feature">
            <h1 style="text-align: center;margin-top: 30px"> Property List </h1>
        
            <div class="row">
            @foreach($property as $p)
                <div class="col-sm-4">
                
                    <div class="feature-items">
                
                        <div class="item">
                            <div class="item-wrap">
                                <a href="{{url('/property/description').'/'.$p->id}}">
                                    <div class="item-img">
                                        <img src="{{asset('uploads/property/'.$p->image)}}">

                                        <?php   $time = strtotime($p->created_at);

                                                    $postDate = date('M d Y', $time);
                                            
                                        ?>
                                        <span> 
                                                @foreach($purpose as $pp)
                                                        @if($pp->id == $p->property_purpose_id)

                                                            {{ucfirst($pp->name)}} 
                                                            <!-- <br>
                                                            Posted: {{$postDate}} -->
                                                        @endif

                                                @endforeach


                                        </span>

                                        <div class="sold">
                                                @if($p->sold)<?php echo 'Used';?> @endif
                                        </div>
                                    </div><!--end .item-img-->

                                    <div class="item-content">
                                        <p class="title">
                                        
                                            Rs. {{$p->price}}

                                            <span>
                                            <i class="fa fa-external-link" aria-hidden="true"></i>
                                            </span>
                                            
                                        </p>

                                        <p class="land">
                                            @if($p->ropani || $p->aana || $p->paisa)
                                                    Land:
                                                    <span>
                                                                @if($p->ropani){{$p->ropani}} @endif

                                                                @if($p->aana){{$p->aana}}
                                                                @endif

                                                                @if($p->paisa){{$p->paisa}} @endif

                                                                @if($p->daam){{$p->daam}} @endif

                                                        </span>

                                                        
                                                    
                                                @endif 

                                                @if($p->road_size)
                                                    
                                                        <span class="land-road">
                                                        Road: 
                                                            {{$p->road_size}} feet
                                                        </span>
                                                @endif
                                        </p>

                                        <p class="road">
                                            {{$p->address}} 

                                            @if(isset($p->city->name))
                                                
                                                    
                                                        {{$p->city->name}}
                                                    
                                            @endif
                                        </p>

                                        <p class="seller">
                                            @if(isset($p->agent->name))
                                                
                                                    
                                                        {{$p->agent->name}}
                                                    
                                            @endif

                                            <span style="float:right;">ID {{$p->id}}</span>
                                        </p>

                                    </div><!--end .item-footer-->
                                </a>
                            </div><!--end .item-wrap-->
                    
                
                        </div><!--end .feature-item-->
                    </div>
                
                </div>
            @endforeach
            </div><!--end .row-->
        
        </section><!--end .list-feature-->

    </div>

    <aside class="col-md-3 main-sidebar">
        <aside class="recommended-properties">
            <h3>Recent Added</h3>
            @foreach($recent as $r)

            <div class="item">
                    <div class="item-img">
                        <a href="{{url('property/description/'.$r->id)}}"><img src="{{asset('uploads/property/'.$r->image)}}" alt="{{$r->title}}"></a>
                        <div class="sim-property-title"><a href="{{url('property/description/'.$r->id)}}"> {{$r->title}} </a></div>
                    </div><!--end .item-img-->
                    <div class="item-content">
                        <p><span>Rs. {{$r->price}}</span><span class="location">@if(isset($r->city->name))
                                                
                                                
                                                    {{ucfirst($r->city->name)}}
                                                
                                        @endif</span></p>
                    </div><!--end .item-content-->
                </div><!--end .item-->
            @endforeach
        </aside><!--end .-->
    </aside>

       </div><!--end .row-->
            </div><!--end .container-->
        </div><!--end .-->
    </div>






    

@include('includes.footer')