<?php
	$agency = \App\AgencyModel::where('status',1)->get();
?>
<section class="featured-agencies mb-5">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1 style="text-align: center;margin-top: 30px"> Featured Agencies </h1>
					<div class="agents-gallery mt-5">
						<div class="owl-carousel owl-theme">

						@foreach($agency as $a)
							<div class="item">
								<div class="img-wrap agenciesTooltip" id="agenciesTooltip_{{$a->id}}"data-placement="top">
									<img class="img-fluid" src="{{asset('uploads/agency/'.$a->image)}}" alt="">
								</div>
								
							</div>
						@endforeach
							
						</div>
					</div>	
					<!--end .list-feature-->
				</div><!--end col-12 -->
			</div><!--end .row -->
		</div><!--end .container -->
	</section><!--end .featured-agencies -->