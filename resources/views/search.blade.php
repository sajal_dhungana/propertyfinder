@include('includes.header')

@include('includes.banner')
	

	<script>
function getCity(){
				$('#city_id').html('<option value="">--Select City--</option>');
				var country = document.getElementById('country_id').value;
				$.ajax({
				url: "{{url('getCity')}}"+'/'+country,
				type: 'GET',
				success: function(res) {
					$('#price_id').html(res);
				}
			});
		
	}

	function getMinMaxPrice(){
		$('#min_price').html('<option value="">--Min Price--</option>');
		$('#max_price').html('<option value="">--Max Price--</option>');
		var property_type_id = document.getElementById('property_type_id').value;
		$.ajax({
		url: "{{url('getMinMaxPrice')}}"+'/'+property_type_id,
		type: 'GET',
		success: function(res) {
			console.log(res);

				 for (var i = 0; i < res.length; i++) {
							  
							  $('#min_price').append("<option value="+res[i].min_price+">"+res[i].min_price+"</option>");
							  $('#max_price').append("<option value="+res[i].max_price+">"+res[i].max_price+"</option>");


							}
			// $('#price_id').html(res);
		}
	});
		
	}
</script>




<section class="list-feature">
	<h1 style="text-align: center;margin-top: 30px"> Property List </h1>
	<div class="container">
		<div class="row">
			@foreach($property as $p)
				<div class="col-sm-3">
					<div class="search_property">
						
							<div class="feature-items">
								<div class="item">
									<div class="item-wrap">
										<div class="item-img">
											<a href="{{asset('uploads/property/'.$p->image)}}"><img src="{{asset('uploads/property/'.$p->image)}}"></a>

												<?php  
													$time = strtotime($p->created_at);

													$postDate = date('M d Y', $time);
												
											?>
											<span> 
													@foreach($purpose as $pp)
															@if($pp->id == $p->property_purpose_id)

																{{ucfirst($pp->name)}} 

																<!-- <br>
																Posted: {{$postDate}} -->
															@endif

													@endforeach


											</span>

												<div class="sold">
														@if($p->sold)<?php echo 'Used';?> @endif
												</div>
										</div>
												
										<div class="item-content">
												<p class="title">
													<a href="{{url('/property/description').'/'.$p->id}}">Rs. {{$p->price}} 
														<span>
														<i class="fa fa-external-link" aria-hidden="true"></i>
														</span>

													</a>


												</p>
											
												<p class="land">
													@if($p->ropani || $p->aana || $p->paisa)
														Land:
															<span>
																	@if($p->ropani){{$p->ropani}} @endif

																	@if($p->aana){{$p->aana}}
																	@endif

																	@if($p->paisa){{$p->paisa}} @endif

																	@if($p->daam){{$p->daam}} @endif

															</span>

															
														
													@endif 

												@if($p->road_size)
														Road: 
														<span>
															{{$p->road_size}} feet
														</span>
												@endif
												</p>

												<p class="road">
													{{$p->address}} 

													@if(isset($p->city->name))
															
															
																{{$p->city->name}}
															
													@endif
												</p>

												<p class="seller">
													
												</p>
											
										</div><!--end .content-->

									</div><!--end .item col-sm-3-->
									
								</div><!--end .feature-item-->
							</div>
						
						
					</div>

				</div>
			@endforeach
		</div><!--end .row-->
	</div>
		
</section>

@include('includes.footer')