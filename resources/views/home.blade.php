
@include('agent.header')

    <div id="activity_section">
        <div id="left_nav">
        @include('agent.leftnav')
        </div><!-- end #left_nav -->
        <div id="respons_section">

        @include('agent.response')

             	@if(Session::has('message'))
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {{session('message')}}</em></div>
@endif
        </div>
    </div><!-- end #activity_section -->
    

    @include('agent.footer')

