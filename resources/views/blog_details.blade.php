@include('includes.header')

<main role="main">
    <section class="blog-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="blog-info">
                        <h1 class="blog-title mb-4">{{$blog->title}}</h1>
                        <img src="{{asset('uploads/blogs/'.$blog->image)}}" class="blog-image" alt="blog main image">
                        <div class="post-social mt-3">
                            <div class="post-summary">
                                <div class="posted-by">{{isset($blog->admin->name)?ucfirst($blog->admin->name):''}}</div>
                                <div class="posted-date mt-1"><?php   $time = strtotime($blog->created_at);

                                                    echo $postDate = date('M d Y', $time);
                                            
                                                ?></div>
                            </div>
                            <div class="social-links">
                                <!-- <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="mailto:info@ghargharana.com"><i class="fa fa-envelope"></i></a> -->
                            </div>
                        </div>
                        <div class="post-desc mt-4">
                            <?php echo $blog->description; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="related-blog-content pb-5">
    <div class="container">
        <div><h2 class="subpage-main-title">Related Blog</h2></div>
        <!--/.Indicators-->
      
       
          <div class="row">
            <div class="col-12 mb-4">
                <div class="blogsCarousel">
                    <div class="owl-carousel owl-theme">
                    @foreach($allBlogs as $all)
                        <div class="item card mb-2">
                            <a href="{{url('blog/details/'.$all->id)}}" class="image-box"><img class="card-img-top" src="{{asset('uploads/blogs/'.$all->image)}}"
                            alt="Card image cap"></a>
                            <div class="card-body">
                                <h4 class="card-title"><a href="{{url('blog/details/'.$all->id)}}">{{$all->title}}</a></h4>
                                <div class="card-text"><?php echo substr($all->description,0,100).'...';?></div>
                            </div>
                        </div>
                    @endforeach
                        
                    </div>
                </div>
            </div>
            
          </div>
        
    </div>
  </section>
</main>

@include('includes.footer')