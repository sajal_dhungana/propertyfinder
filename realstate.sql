-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2020 at 05:48 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `realstate`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `status`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 0, 'sajal', 'dhunganasajal@gmail.com', '$2y$10$HLSOZ9i/0J4H8P8Ec78lA.Odw6IycghbcqD1JhXwZzQHH3K/l514C', 'euuXqpHxzGAN1tEgqcDwExFz6ZG3FF8bCLSkw9gfRyubU5LJFdmlZvXt9jkN', NULL, '2020-12-03 18:02:56');

-- --------------------------------------------------------

--
-- Table structure for table `admin_notification`
--

CREATE TABLE `admin_notification` (
  `id` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `title` text,
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_notification`
--

INSERT INTO `admin_notification` (`id`, `agent_id`, `admin_id`, `property_id`, `link`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 3, NULL, 8, 'http://localhost/real-state/public/property/description/8', 'A big room at samakhusi', 'A big room at samakhusi has been Created.', '2020-12-16 13:37:57', '2020-12-16 13:37:57');

-- --------------------------------------------------------

--
-- Table structure for table `advertisement`
--

CREATE TABLE `advertisement` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advertisement`
--

INSERT INTO `advertisement` (`id`, `image`, `link`, `page`, `section`, `status`, `created_at`, `updated_at`) VALUES
(1, '1608116706Ratopati.gif', 'https://www.youtube.com/', 'property_description', 'side_buttom', 1, '2020-12-16 16:48:33', '2020-12-16 18:05:06'),
(2, '1608184361prabhu.gif', 'https://www.facebook.com/', 'property_description', 'side_top', 1, '2020-12-17 12:52:41', '2020-12-17 12:52:41'),
(3, '1608186242Online-khabar.gif', 'https://www.onlinekhabar.com/', 'home', 'top', 1, '2020-12-17 13:24:02', '2020-12-17 13:24:02'),
(4, '1608188860Online-khabar.gif', 'https://www.onlinekhabar.com/', 'buy', 'top', 1, '2020-12-17 14:07:40', '2020-12-17 14:07:40'),
(5, '1608188916Online-khabar.gif', 'https://www.onlinekhabar.com/', 'rent', 'top', 1, '2020-12-17 14:08:36', '2020-12-17 14:08:36');

-- --------------------------------------------------------

--
-- Table structure for table `agency`
--

CREATE TABLE `agency` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agency`
--

INSERT INTO `agency` (`id`, `name`, `email`, `phone`, `location`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'My Agency', 'dhunganasajal@gmail.com', '9849364867', 'kathmandu', '1608198972agent-logo2.jpg', 1, '2020-12-17 16:56:12', '2020-12-17 16:56:12'),
(3, 'Premium Agency', 'myagency@gmail.com', '09849364888', 'hetauda', '1608200348agent-logo1.jpg', 1, '2020-12-17 17:19:08', '2020-12-17 17:19:08');

-- --------------------------------------------------------

--
-- Table structure for table `agent_type`
--

CREATE TABLE `agent_type` (
  `id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agent_type`
--

INSERT INTO `agent_type` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Owner', 0, NULL, NULL),
(2, 'Sales Agent', 0, NULL, NULL),
(3, 'Letting Agent', 0, NULL, NULL),
(4, 'Sales Letting Agent', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` text,
  `description` text,
  `featured` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `image`, `title`, `description`, `featured`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, '16076646714.jpg', 'updated-Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 1, 1, 1, 1, '2020-12-11 12:31:11', '2020-12-14 16:38:45'),
(2, '1607925562+2.jpg', 'Blog', 'A blog (a truncation of \"weblog\")[1] is a discussion or informational website published on the World Wide Web consisting of discrete, often informal diary-style text entries (posts). Posts are typically displayed in reverse chronological order, so that the most recent post appears first, at the top of the web page. Until 2009, blogs were usually the work of a single individual,[citation needed] occasionally of a small group, and often covered a single subject or topic. In the 2010s, \"multi-author blogs\" (MABs) emerged, featuring the writing of multiple authors and sometimes professionally edited. MABs from newspapers, other media outlets, universities, think tanks, advocacy groups, and similar institutions account for an increasing quantity of blog traffic. The rise of Twitter and other \"microblogging\" systems helps integrate MABs and single-author blogs into the news media. Blog can also be used as a verb, meaning to maintain or add content to a blog.\r\n\r\nThe emergence and growth of blogs in the late 1990s coincided with the advent of web publishing tools that facilitated the posting of content by non-technical users who did not have much experience with HTML or computer programming. Previously, a knowledge of such technologies as HTML and File Transfer Protocol had been required to publish content on the Web, and early Web users therefore tended to be hackers and computer enthusiasts. In the 2010s, the majority are interactive Web 2.0 websites, allowing visitors to leave online comments, and it is this interactivity that distinguishes them from other static websites.[2] In that sense, blogging can be seen as a form of social networking service. Indeed, bloggers not only produce content to post on their blogs but also often build social relations with their readers and other bloggers.[3] However, there are high-readership blogs which do not allow comments.', 1, 1, NULL, 1, '2020-12-14 12:59:22', '2020-12-14 15:50:37'),
(3, '1607939307l2.jpg', 'Test Blog', '<p>CKEditor by default does not give the option to upload the image. If someone is looking to give this option then keep reading this article. It needs to add a Laravel route, write a code for image upload, and some JavaScript to your application. First, to enable the image upload option you need to call CKEditor in the following way.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>CKEditor by default does not give the option to upload the image. If someone is looking to give this option then keep reading this article. It needs to add a Laravel route, write a code for image upload, and some JavaScript to your application. First, to enable the image upload option you need to call CKEditor in the following way.</p>', 1, 1, 1, NULL, '2020-12-14 16:48:27', '2020-12-14 16:48:27');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `tell_us` text,
  `about` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cv` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `name`, `email`, `phone`, `position`, `tell_us`, `about`, `created_at`, `updated_at`, `cv`, `status`) VALUES
(1, 'sajal dhungana', 'dhunganasajal@gmail.com', 9849364867, 'Senior Web Developer', 'About me', NULL, '2020-12-13 11:38:31', '2020-12-13 17:54:10', '1607834311resume.docx', 0);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `country_id`, `state_id`, `district_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 'hetauda', NULL, NULL, 2, 1, '2020-11-23 13:20:09', '2020-11-23 13:20:09'),
(4, 'birgunj', NULL, NULL, 2, 1, '2020-12-16 15:32:08', '2020-12-16 15:32:08');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `property_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `comment_user_id` bigint(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `comment` text,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `company` text,
  `message` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `phone`, `email`, `company`, `message`, `created_at`, `updated_at`) VALUES
(1, 'sajal dhungana', 9849364867, 'dhunganasajal@gmail.com', 'My Own Company', 'Hello, can we meet sometime...', '2020-12-09 17:29:19', '2020-12-09 17:29:19');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sortname` varchar(30) DEFAULT NULL,
  `google_map` text,
  `phone_code` varchar(255) DEFAULT NULL,
  `add_to_feature` tinyint(4) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `sortname`, `google_map`, `phone_code`, `add_to_feature`, `ordering`, `status`, `created_at`, `updated_at`) VALUES
(2, 'nepal', 'np', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3722458.3274410325!2d51.69465033744524!3d24.33906459624353!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e48dfb1ab12bd%3A0x33d32f56c0080aa7!2sUnited+Arab+Emirates!5e0!3m2!1sen!2sae!4v1495983440945\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', NULL, 1, 1, 1, '2020-11-03 12:28:28', '2020-11-03 14:07:38');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `provider_id` varchar(255) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `name`, `country_id`, `state_id`, `status`, `created_at`, `updated_at`) VALUES
(2, 'makwanpur', NULL, 2, 1, '2020-11-10 17:04:53', '2020-11-10 17:53:12'),
(3, 'chitwan', NULL, 2, 1, '2020-12-16 15:41:04', '2020-12-16 15:41:04');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `question` text,
  `answer` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`, `created_at`, `updated_at`) VALUES
(3, 'How does the Ghargharana work?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros.', '2020-12-09 15:14:50', '2020-12-09 15:19:33'),
(4, 'How do I signup to the program?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros.', '2020-12-09 15:20:25', '2020-12-09 15:20:25'),
(5, 'How do I earn in this program?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nisl lorem, dictum id pellentesque at, vestibulum ut arcu. Curabitur erat libero, egestas eu tincidunt ac, rutrum ac justo. Vivamus condimentum laoreet lectus, blandit posuere tortor aliquam vitae. Curabitur molestie eros.', '2020-12-09 15:20:55', '2020-12-09 15:20:55');

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE `feature` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon_id` int(11) DEFAULT NULL,
  `multiple_tags` text,
  `details` text,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feature`
--

INSERT INTO `feature` (`id`, `name`, `icon_id`, `multiple_tags`, `details`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Bedrooms', 1, NULL, NULL, 0, '2020-11-10 13:05:55', '2020-11-10 13:21:25'),
(2, 'kitchen', 2, NULL, NULL, 1, '2020-11-10 13:27:43', '2020-11-10 13:27:43'),
(3, 'Bathrooms', 3, NULL, NULL, 1, '2020-11-10 13:28:31', '2020-11-10 13:28:31'),
(4, 'living room', 2, NULL, NULL, 1, '2020-11-29 14:12:40', '2020-11-29 14:12:40');

-- --------------------------------------------------------

--
-- Table structure for table `feature_icon`
--

CREATE TABLE `feature_icon` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feature_icon`
--

INSERT INTO `feature_icon` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'fa fa-bed', 0, '2020-11-10 12:18:40', '2020-11-10 12:23:20'),
(2, 'fa fa-cutlery', 1, '2020-11-10 12:19:03', '2020-11-10 12:19:03'),
(3, 'fa fa-bathtub', 1, '2020-11-10 13:28:12', '2020-11-10 13:28:12'),
(4, 'fa-room', 1, '2020-11-29 14:53:18', '2020-11-29 14:53:18');

-- --------------------------------------------------------

--
-- Table structure for table `feature_type`
--

CREATE TABLE `feature_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feature_type`
--

INSERT INTO `feature_type` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(5, 'Checked', NULL, NULL, NULL),
(6, 'Multiple Tags', NULL, NULL, NULL),
(7, 'Details', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `title` text,
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `agent_id`, `admin_id`, `property_id`, `link`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 6, 'http://localhost/real-state/public/property/description/6', NULL, NULL, '2020-12-16 12:10:47', '2020-12-16 12:10:47'),
(2, 3, 1, 1, 'http://localhost/real-state/public/property/description/1', NULL, NULL, '2020-12-16 12:19:38', '2020-12-16 12:19:38'),
(3, 3, 1, 2, 'http://localhost/real-state/public/property/description/2', NULL, NULL, '2020-12-16 12:21:08', '2020-12-16 12:21:08'),
(4, 3, 1, 6, 'http://localhost/real-state/public/property/description/6', 'A big land at Maipi', 'Your property A big land at Maipi has been Deactivated', '2020-12-16 12:45:01', '2020-12-16 12:45:01');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE `plan` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`id`, `name`, `price`, `status`, `created_at`, `updated_at`) VALUES
(4, 'Free', 0, 1, '2020-12-15 12:33:23', '2020-12-15 16:28:31'),
(5, 'Bronze', 20, 1, '2020-12-15 12:33:37', '2020-12-15 16:28:20'),
(6, 'Silver', 30, 1, '2020-12-15 16:27:59', '2020-12-15 16:27:59');

-- --------------------------------------------------------

--
-- Table structure for table `plans_features`
--

CREATE TABLE `plans_features` (
  `id` int(11) NOT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `plan_feature` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plans_features`
--

INSERT INTO `plans_features` (`id`, `plan_id`, `plan_feature`, `created_at`, `updated_at`) VALUES
(3, 4, '2,3', '2020-12-15 16:20:06', '2020-12-15 16:20:06'),
(4, 5, '4,5', '2020-12-15 16:20:15', '2020-12-15 16:20:15');

-- --------------------------------------------------------

--
-- Table structure for table `plan_feature`
--

CREATE TABLE `plan_feature` (
  `id` int(11) NOT NULL,
  `name` text,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plan_feature`
--

INSERT INTO `plan_feature` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(2, '50GB Disk Space', 1, '2020-12-15 14:14:06', '2020-12-15 14:14:06'),
(3, '50 Email Accounts', 1, '2020-12-15 14:14:23', '2020-12-15 14:14:23'),
(4, '60GB Disk Space', 1, '2020-12-15 14:14:39', '2020-12-15 14:14:39'),
(5, '60 Email Accounts', 1, '2020-12-15 14:14:52', '2020-12-15 14:14:52');

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE `price` (
  `id` int(11) NOT NULL,
  `min_price` double DEFAULT NULL,
  `max_price` double DEFAULT NULL,
  `property_type_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price`
--

INSERT INTO `price` (`id`, `min_price`, `max_price`, `property_type_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 9000, 10000, 1, 1, '2020-11-11 17:16:34', '2020-11-23 15:32:02'),
(3, 10000, 11000, 1, 1, '2020-11-11 17:32:16', '2020-11-23 15:32:16');

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE `property` (
  `id` int(11) NOT NULL,
  `views` int(11) DEFAULT NULL,
  `sold` tinyint(4) DEFAULT '0',
  `featured` tinyint(4) DEFAULT '0',
  `code` varchar(255) DEFAULT NULL,
  `title` text,
  `image` varchar(255) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `property_type_id` int(11) DEFAULT NULL,
  `property_purpose_id` int(11) DEFAULT NULL,
  `property_for` varchar(60) DEFAULT NULL,
  `no_of_rooms` int(11) DEFAULT NULL,
  `no_of_bedrooms` int(11) DEFAULT NULL,
  `no_of_bathrooms` int(11) DEFAULT NULL,
  `no_of_kitchens` int(11) DEFAULT NULL,
  `no_of_living_rooms` int(11) DEFAULT NULL,
  `area` double DEFAULT NULL,
  `ropani` int(11) DEFAULT NULL,
  `aana` int(11) DEFAULT NULL,
  `paisa` int(11) DEFAULT NULL,
  `daam` int(11) DEFAULT NULL,
  `no_of_floors` int(11) DEFAULT NULL,
  `road_type_id` int(11) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `road_size` double DEFAULT NULL,
  `near_hospital` tinyint(4) DEFAULT '0',
  `near_school` tinyint(4) DEFAULT '0',
  `near_market` tinyint(4) DEFAULT '0',
  `near_bank` tinyint(4) DEFAULT '0',
  `near_bus_stop` tinyint(4) DEFAULT '0',
  `near_taxi_stand` tinyint(4) DEFAULT '0',
  `near_airport` tinyint(4) DEFAULT '0',
  `hospital_name` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `market_name` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bus_stop_name` varchar(255) DEFAULT NULL,
  `taxi_stand_name` varchar(255) DEFAULT NULL,
  `airport_name` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `map_location` text,
  `image_type` varchar(255) DEFAULT NULL,
  `description` text,
  `key_features` varchar(255) DEFAULT NULL,
  `disclaimer_title` text,
  `disclaimer` text,
  `ip` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `floor_plan` varchar(255) DEFAULT NULL,
  `verified` varchar(255) DEFAULT NULL,
  `vcode` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `facebook_link` text,
  `linkedin_link` text,
  `twitter_link` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `views`, `sold`, `featured`, `code`, `title`, `image`, `latitude`, `longitude`, `subtitle`, `agent_id`, `country_id`, `state_id`, `city_id`, `address`, `street`, `property_type_id`, `property_purpose_id`, `property_for`, `no_of_rooms`, `no_of_bedrooms`, `no_of_bathrooms`, `no_of_kitchens`, `no_of_living_rooms`, `area`, `ropani`, `aana`, `paisa`, `daam`, `no_of_floors`, `road_type_id`, `currency`, `price`, `road_size`, `near_hospital`, `near_school`, `near_market`, `near_bank`, `near_bus_stop`, `near_taxi_stand`, `near_airport`, `hospital_name`, `school_name`, `market_name`, `bank_name`, `bus_stop_name`, `taxi_stand_name`, `airport_name`, `video_url`, `map_location`, `image_type`, `description`, `key_features`, `disclaimer_title`, `disclaimer`, `ip`, `count`, `floor_plan`, `verified`, `vcode`, `status`, `created_at`, `updated_at`, `facebook_link`, `linkedin_link`, `twitter_link`) VALUES
(1, 9, 0, 1, NULL, 'A big room at samakhusi chowk', '16048969342.jpg', 27.7273, 85.3175, 'suitable for single person', 1, 2, NULL, 2, NULL, NULL, 1, 1, 'rent', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 7500, 0, 1, 0, 0, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-11-04 14:44:07', '2020-12-17 14:31:05', NULL, NULL, NULL),
(2, 94, 0, 1, NULL, 'A flat at Basundhara', '16048969342.jpg', 27.7425, 85.3343, 'Available for family', 3, 2, NULL, 3, NULL, NULL, 2, 1, 'rent', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 20000, 10, 1, 1, 0, 1, 1, 0, 1, NULL, 'V.S. Niketan', 'Bishal Bazar', 'Nabil Bank', 'Grandy Bus Stop', NULL, 'Tribhuwan International Airport', NULL, NULL, NULL, 'Limited offer ! Free 3 Services\r\nBuy   Sale   Exchange   Repair\r\nComes with sales bill/company agreement paper, buy back gurantee\r\nCall/viber : 9813546342, 9840383849, 9803986393\r\nTax clear upto date 2077/78\r\nExchange also available (any car/bike/scoty)', NULL, 'Be safe. Beware of fraud & scams', '.Ghar Gharana is NOT involved in transaction of any goods/services listed in the website. It is only platform to share information. You are directly contacting the person who has posted the advertisement and you agree not to hold Hamrobazar.com responsible for their act in any circumstances.\r\n\r\nWe strongly encourage you to take necessary precaution. Avoid advance payment, check goods before purchasing, inst', NULL, NULL, NULL, NULL, NULL, 1, '2020-11-04 16:44:50', '2020-12-17 14:31:09', NULL, NULL, NULL),
(3, 54, 0, 0, NULL, 'A Well Furnished Flat at baneshwor', '16066245491.jpg', 27.33, 85.22, 'Suitable for family', 3, 2, NULL, 3, NULL, NULL, 4, 2, 'sell', 10, 3, 4, 1, 2, NULL, 2, 2, 2, 2, 1, 2, NULL, 40000, 1, 1, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-11-06 16:28:44', '2020-12-17 14:51:59', NULL, NULL, NULL),
(4, 10, 0, 1, NULL, 'A big room at samakhusi', '16062819311.jpg', 24.33, 26.77, 'Available for family', 1, NULL, NULL, 3, NULL, NULL, 1, 1, NULL, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 10000, 1, 1, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2020-11-25 12:25:31', '2020-12-15 18:11:42', NULL, NULL, NULL),
(5, 18, 1, 0, NULL, 'test image', NULL, 34.22, 36.77, 'Suitable for family', 1, NULL, NULL, 3, NULL, NULL, 1, 1, NULL, 2, 2, 2, 2, 2, NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, 543434, 1, 0, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-11-25 15:17:09', '2020-12-17 14:31:01', NULL, NULL, NULL),
(6, 20, 0, 1, NULL, 'A big land at Maipi', '16067300913.jpg', 23.44, 45.66, 'Suitable for mall', 1, NULL, NULL, 3, NULL, NULL, 5, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 3, 4, NULL, 2, NULL, 5000000, 1, 0, 0, 0, 0, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://www.youtube.com/embed/z1trSVkN5WQ', NULL, NULL, '11 ropani 4 anna orange farming land sale in Sarda municipality-9, Namo buddha, Kavrepalanchok district. Five hundred orange trees and One mud house are in the property. We annually sales orange around 3 lakhs.\r\nContact Number:\r\n9808268522\r\n9851117712', NULL, NULL, 'Be safe. Beware of fraud & scams.\r\nHamrobazar.com is NOT involved in transaction of any goods/services listed in the website. It is only platform to share information. You are directly contacting the person who has posted the advertisement and you agree not to hold Hamrobazar.com responsible for their act in any circumstances.\r\n\r\nWe strongly encourage you to take necessary precaution. Avoid advance payment, check goods before purchasing, instead of cash use mobile wallets (IME Pay, Esewa, Khalti, Connect Ips, etc) or bank transfer for payments. For more read Safety Tips', NULL, NULL, NULL, NULL, NULL, 0, '2020-11-30 16:54:51', '2020-12-17 14:30:55', NULL, NULL, NULL),
(7, 6, 0, 1, NULL, 'A Flat at Nagarkot', '16075061271.jpg', 22.35, 80.55, 'Available for husband and wife', 3, NULL, NULL, 3, NULL, NULL, 2, 1, NULL, 5, 2, 1, 1, 1, NULL, 2, 3, 4, 5, NULL, 2, NULL, 5000, 20, 1, 0, 0, 0, 0, 0, 1, 'Om Hospital', NULL, NULL, NULL, NULL, NULL, 'Tribhuwan Airport', 'https://www.youtube.com/embed/z1trSVkN5WQ', NULL, NULL, 'Limited offer ! Free 3 Services Buy Sale Exchange Repair Comes with sales bill/company agreement paper, buy back gurantee Call/viber : 9813546342, 9840383849, 9803986393 Tax clear upto date 2077/78 Exchange also available (any car/bike/scoty)', NULL, 'Be safe. Beware of fraud & scams', '.Ghar Gharana is NOT involved in transaction of any goods/services listed in the website. It is only platform to share information. You are directly contacting the person who has posted the advertisement and you agree not to hold Hamrobazar.com responsible for their act in any circumstances. We strongly encourage you to take necessary precaution. Avoid advance payment, check goods before purchasing, inst', NULL, NULL, NULL, NULL, NULL, 1, '2020-12-09 16:28:47', '2020-12-09 16:44:09', 'https://www.facebook.com/', 'https://www.linkedin.com/', 'https://twitter.com/'),
(8, 8, 1, 0, NULL, 'A big room at samakhusi', '16081006771.jpg', 22.45, 80.22, 'suitable for two persons', 3, NULL, NULL, 3, NULL, NULL, 1, 1, NULL, 5, 5, 5, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 20, 30, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://www.youtube.com/embed/z1trSVkN5WQ', NULL, NULL, 'This is our property', NULL, 'Be safe. Beware of fraud & scams', 'property disclaimer description', NULL, NULL, NULL, NULL, NULL, 0, '2020-12-16 13:37:57', '2020-12-17 14:21:17', 'https://www.facebook.com/', 'https://www.linkedin.com/', 'https://twitter.com/');

-- --------------------------------------------------------

--
-- Table structure for table `property_feature`
--

CREATE TABLE `property_feature` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `property_type_id` int(11) DEFAULT NULL,
  `feature_type` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_feature`
--

INSERT INTO `property_feature` (`id`, `name`, `property_type_id`, `feature_type`, `created_at`, `updated_at`) VALUES
(3, '1,2', 1, 'Checked', '2020-11-02 17:55:11', '2020-11-23 14:30:47'),
(4, '2,3', 2, 'Multiple Tags', '2020-11-10 15:31:45', '2020-11-10 16:04:02'),
(5, '1,2,3,4', 4, 'Checked', '2020-11-11 11:51:49', '2020-11-29 14:53:55');

-- --------------------------------------------------------

--
-- Table structure for table `property_gallary`
--

CREATE TABLE `property_gallary` (
  `property_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_gallary`
--

INSERT INTO `property_gallary` (`property_id`, `image`, `created_at`, `updated_at`) VALUES
(4, '16062907991.jpg', '2020-11-25 14:53:19', '2020-11-25 14:53:19'),
(4, '16062907992.jpg', '2020-11-25 14:53:19', '2020-11-25 14:53:19'),
(3, '16066245491.jpg', '2020-11-29 11:35:49', '2020-11-29 11:35:49'),
(3, '16066245492.jpg', '2020-11-29 11:35:49', '2020-11-29 11:35:49'),
(3, '16066245504.jpg', '2020-11-29 11:35:50', '2020-11-29 11:35:50'),
(7, '16075061273.jpg', '2020-12-09 16:28:47', '2020-12-09 16:28:47'),
(7, '16075061284.jpg', '2020-12-09 16:28:48', '2020-12-09 16:28:48');

-- --------------------------------------------------------

--
-- Table structure for table `property_purpose`
--

CREATE TABLE `property_purpose` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_purpose`
--

INSERT INTO `property_purpose` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'rent', 1, NULL, NULL),
(2, 'sell', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `property_type`
--

CREATE TABLE `property_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_type`
--

INSERT INTO `property_type` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'room', 0, NULL, NULL),
(2, 'flat', 0, NULL, NULL),
(4, 'house', 0, '2020-11-02 16:13:28', '2020-11-06 15:04:28'),
(5, 'land', 0, '2020-11-06 13:35:59', '2020-11-23 13:02:32'),
(6, 'shop', 0, '2020-12-14 12:18:19', '2020-12-14 12:18:19');

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `id` int(11) NOT NULL,
  `property_id` int(11) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `reply` text,
  `reply_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `road_size`
--

CREATE TABLE `road_size` (
  `id` int(11) NOT NULL,
  `name` double DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `road_size`
--

INSERT INTO `road_size` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 20, 1, '2020-11-10 16:21:44', '2020-11-10 16:21:44'),
(2, 30, 1, '2020-11-10 16:21:53', '2020-11-10 16:21:53');

-- --------------------------------------------------------

--
-- Table structure for table `road_type`
--

CREATE TABLE `road_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `road_type`
--

INSERT INTO `road_type` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'gravel', 1, NULL, '2020-11-23 13:33:28'),
(2, 'pitch', 1, NULL, NULL),
(3, 'slab', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `site_title` varchar(255) DEFAULT NULL,
  `site_address` varchar(255) DEFAULT NULL,
  `site_email` varchar(255) DEFAULT NULL,
  `site_phone` varchar(255) DEFAULT NULL,
  `site_mobile` varchar(255) DEFAULT NULL,
  `site_fax` varchar(255) DEFAULT NULL,
  `site_open_time` varchar(255) DEFAULT NULL,
  `site_close_time` varchar(255) DEFAULT NULL,
  `site_footer_text` text,
  `site_person_name` varchar(255) DEFAULT NULL,
  `site_person_number` varchar(255) DEFAULT NULL,
  `site_person_email` varchar(255) DEFAULT NULL,
  `site_embed_map` text,
  `site_facebook` varchar(255) DEFAULT NULL,
  `site_twitter` varchar(255) DEFAULT NULL,
  `site_linkedin` varchar(255) DEFAULT NULL,
  `site_instagram` varchar(255) DEFAULT NULL,
  `site_viber` varchar(255) DEFAULT NULL,
  `site_youtube` varchar(255) DEFAULT NULL,
  `site_msg_title` text,
  `site_msg_name` varchar(255) DEFAULT NULL,
  `site_msg_message` text,
  `site_setting` varchar(255) DEFAULT NULL,
  `site_msg_photo` varchar(255) DEFAULT NULL,
  `site_logo` varchar(255) DEFAULT NULL,
  `site_favicon` varchar(255) DEFAULT NULL,
  `site_whatsapp` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `site_title`, `site_address`, `site_email`, `site_phone`, `site_mobile`, `site_fax`, `site_open_time`, `site_close_time`, `site_footer_text`, `site_person_name`, `site_person_number`, `site_person_email`, `site_embed_map`, `site_facebook`, `site_twitter`, `site_linkedin`, `site_instagram`, `site_viber`, `site_youtube`, `site_msg_title`, `site_msg_name`, `site_msg_message`, `site_setting`, `site_msg_photo`, `site_logo`, `site_favicon`, `site_whatsapp`, `created_at`, `updated_at`) VALUES
(1, 'Ghar Gharana', 'Baneshwor, Kathmandu', 'realstate@gmail.com', '9849364867', '9849364867', '9849364867', '14:00', '18:00', 'Real State Footer', 'Sajal Dhungana', '9849364867', 'dhunganasajal@gmail.com', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d226659.20929933494!2d84.89800953382925!3d27.420960545218545!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb497ced46c917%3A0xafb8902c7a4532ab!2sHetauda!5e0!3m2!1sen!2snp!4v1604912426735!5m2!1sen!2snp\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>', NULL, NULL, NULL, NULL, NULL, NULL, 'message title', 'message name', 'message details', NULL, '16049133511.jpg', '16049133512.jpg', '1604913351c_back.jpg', NULL, '2020-11-09 16:15:51', '2020-11-09 16:28:09');

-- --------------------------------------------------------

--
-- Table structure for table `social_setting`
--

CREATE TABLE `social_setting` (
  `id` int(11) NOT NULL,
  `site_facebook` varchar(255) DEFAULT NULL,
  `site_twitter` varchar(255) DEFAULT NULL,
  `site_linkedin` varchar(255) DEFAULT NULL,
  `site_instagram` varchar(255) DEFAULT NULL,
  `site_viber` varchar(255) DEFAULT NULL,
  `site_youtube` varchar(255) DEFAULT NULL,
  `site_whatsapp` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_setting`
--

INSERT INTO `social_setting` (`id`, `site_facebook`, `site_twitter`, `site_linkedin`, `site_instagram`, `site_viber`, `site_youtube`, `site_whatsapp`, `created_at`, `updated_at`) VALUES
(1, 'https://www.facebook.com/', 'http://twitter.com/', 'https://www.linkedin.com/', 'http://instagram.com', 'http://viber.com/', 'https://www.youtube.com/', 'https://web.whatsapp.com/', '2020-11-09 16:58:38', '2020-11-09 16:58:38');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `google_map` text,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `name`, `country_id`, `google_map`, `status`, `created_at`, `updated_at`) VALUES
(2, 'bagmati pradesh-3', 2, '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3722458.3274410325!2d51.69465033744524!3d24.33906459624353!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e48dfb1ab12bd%3A0x33d32f56c0080aa7!2sUnited+Arab+Emirates!5e0!3m2!1sen!2sae!4v1495983440945\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 0, '2020-11-03 13:28:31', '2020-11-10 16:50:45'),
(3, 'sudur paschim', NULL, NULL, 1, '2020-11-10 16:47:17', '2020-11-10 16:47:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_registration_num` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_established_date` date DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_num` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_num1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `plan_id`, `name`, `email`, `image`, `type_id`, `company_name`, `company_logo`, `company_registration_num`, `company_established_date`, `country_id`, `city_id`, `address`, `contact_name`, `contact_num`, `contact_num1`, `status`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'sajal dhungana', 'dhunganasajal@gmail.com', NULL, 2, NULL, NULL, NULL, NULL, NULL, 2, NULL, 'sajal', NULL, '9845679803', 1, '$2y$10$/4joPhZ3QOB0yCmqq8a1s.kJeU6dzB3nuTMeEzz8wFkkiKzNko9by', NULL, '2020-11-01 15:40:32', '2020-12-10 15:23:31'),
(3, NULL, 'sajal dhungana', 'sajal7341@gmail.com', '16063785981.png', 2, 'Software Company', '1606129042+2.jpg', 'a54657x', NULL, NULL, 3, 'ktm', 'aparichit', '9849364867', '9849364868', 1, '$2y$10$MIG8.CWkDk1qrpOKpkavOeZaUeDh1SuYMoj8VYWYdmHAoelnOuPUq', 'AlzfmhWHH4cBvpmKlikkX4L6cFQWI9qjs38Ws1apaZPPEbZv0xWKFTY9tLEN', '2020-11-22 16:42:14', '2020-11-26 15:16:38'),
(4, NULL, 'anup', 'anup@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '$2y$10$8f.8/RWpLUxtYEwokKi/1ueE/rWv4CvoiWjjYfMnbu/aTixLGmQQ6', '2xwsJMnZslXq6RvLHGJ26w4RFfmleRhpxb0ivGozV27b0Evv1t8HfwXeEHST', '2020-12-15 17:02:05', '2020-12-15 17:02:05'),
(5, NULL, 'bibek thapa', 'bibek@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '$2y$10$WWIGSCQOIe1nF/hPEfUNvuE/RWljIqN1uccorGR70Nycpets2LW2K', 'O3OhRYpVaysdhhGuvBRDvDdzZXoPOYWrx6SHcR3hoXN7CswPz4oMGQYv9roh', '2020-12-15 17:05:13', '2020-12-15 17:05:13'),
(6, NULL, 'sajal dhungana', 'amit@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '$2y$10$a5.pJaG9uBjq.qJQS0ySuutG5aTgKuJ4sbViGtl2205zKMrTAA7VO', NULL, '2020-12-15 17:08:31', '2020-12-15 17:08:31'),
(7, 4, 'sajal dhungana', 'apple@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '$2y$10$9lI/ExWsaY.AzHCwMYiQL.Aqa2mue2uO6NS/5YbAmwJkBX98Z4Mb2', NULL, '2020-12-15 17:09:39', '2020-12-15 17:09:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_notification`
--
ALTER TABLE `admin_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisement`
--
ALTER TABLE `advertisement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agent_type`
--
ALTER TABLE `agent_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature_icon`
--
ALTER TABLE `feature_icon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature_type`
--
ALTER TABLE `feature_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plans_features`
--
ALTER TABLE `plans_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan_feature`
--
ALTER TABLE `plan_feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_feature`
--
ALTER TABLE `property_feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_purpose`
--
ALTER TABLE `property_purpose`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_type`
--
ALTER TABLE `property_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reply`
--
ALTER TABLE `reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `road_size`
--
ALTER TABLE `road_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `road_type`
--
ALTER TABLE `road_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_setting`
--
ALTER TABLE `social_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_notification`
--
ALTER TABLE `admin_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `advertisement`
--
ALTER TABLE `advertisement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `agency`
--
ALTER TABLE `agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `agent_type`
--
ALTER TABLE `agent_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `feature_icon`
--
ALTER TABLE `feature_icon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `feature_type`
--
ALTER TABLE `feature_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `plan`
--
ALTER TABLE `plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `plans_features`
--
ALTER TABLE `plans_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `plan_feature`
--
ALTER TABLE `plan_feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `price`
--
ALTER TABLE `price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `property_feature`
--
ALTER TABLE `property_feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `property_purpose`
--
ALTER TABLE `property_purpose`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `property_type`
--
ALTER TABLE `property_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `reply`
--
ALTER TABLE `reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `road_size`
--
ALTER TABLE `road_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `road_type`
--
ALTER TABLE `road_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `social_setting`
--
ALTER TABLE `social_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
