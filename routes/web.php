<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });


//facebook login check
 Route::get('/auth/redirect/{provider}/{id}', 'SocialController@redirect');
  Route::get('/callback/{provider}', 'SocialController@callback');

 //city property
 Route::get('city/{id}/property', 'FrontendController@getCityProperty'); 

//comment
Route::post('comment', 'FrontendController@comment');
Route::post('reply', 'HomeController@reply');


Route::get('/', 'FrontendController@index');
Route::post('/search', 'FrontendController@search');
Route::get('/property/description/{id}', 'FrontendController@propertyDescription');

Route::get('/getMinMaxPrice/{id}', 'FrontendController@getMinMaxPrice');

Route::get('/agent', 'FrontendController@agent');
Route::post('/searchAgent', 'FrontendController@searchAgent');
Route::get('/agent/description/{id}', 'FrontendController@agentDescription');

Route::get('/buy', 'FrontendController@buy');
Route::get('/rent', 'FrontendController@rent');

Route::get('/careers', 'FrontendController@careers');
Route::post('/careers', 'FrontendController@saveApplicants');
Route::get('/contact', 'FrontendController@contact');
Route::get('/sitemap', 'FrontendController@sitemap');
Route::get('/policy', 'FrontendController@policy');
Route::get('/terms', 'FrontendController@terms');
Route::get('/blogs', 'FrontendController@blogs');
Route::get('/faq', 'FrontendController@faq');

Route::post('/contact', 'FrontendController@saveContact');
Route::get('/blog/details/{id}', 'FrontendController@blogDetails');

Route::get('/plan', 'FrontendController@plan');




Auth::routes();


//agent

//message
Route::get('/agent/notification/list/{id}', 'HomeController@showMessages');
Route::get('/agent/notification/details/{id}', 'HomeController@showMessageDetails');


//property gallary
Route::get('getPropertyGallary/{id}','Agent\PropertyController@getPropertyGallary');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/agent/profile/{id}', 'HomeController@profile');
Route::get('/agent/password/edit/{id}', 'HomeController@edit');
Route::post('/agent/password/update', 'HomeController@update');
Route::post('/agent/profile/update', 'HomeController@updateProfile');


//property type
// Route::get('/agent/propertyType/list', 'Agent\PropertyTypeController@index');
// Route::get('/agent/propertyType/create', 'Agent\PropertyTypeController@create');
// Route::post('/agent/propertyType/create', 'Agent\PropertyTypeController@store');
// Route::get('/agent/propertyType/delete/{id}', 'Agent\PropertyTypeController@delete');
// Route::get('/agent/propertyType/edit/{id}', 'Agent\PropertyTypeController@edit');
// Route::post('/agent/propertyType/update', 'Agent\PropertyTypeController@update');

//property feature
// Route::get('/agent/propertyFeature/list', 'Agent\PropertyFeatureController@index');
// Route::get('/agent/propertyFeature/create', 'Agent\PropertyFeatureController@create');
// Route::post('/agent/propertyFeature/create', 'Agent\PropertyFeatureController@store');
// Route::get('/agent/propertyFeature/delete/{id}', 'Agent\PropertyFeatureController@delete');
// Route::get('/agent/propertyFeature/edit/{id}', 'Agent\PropertyFeatureController@edit');
// Route::post('/agent/propertyFeature/update', 'Agent\PropertyFeatureController@update');

//property
Route::get('/agent/property/list', 'Agent\PropertyController@index');
Route::get('/agent/property/create', 'Agent\PropertyController@create');
Route::post('/agent/property/create', 'Agent\PropertyController@store');
Route::get('/agent/property/delete/{id}', 'Agent\PropertyController@delete');
Route::get('/agent/property/edit/{id}', 'Agent\PropertyController@edit');
Route::post('/agent/property/update', 'Agent\PropertyController@update');

//country
// Route::get('/agent/country/list', 'Agent\CountryController@index');
// Route::get('/agent/country/create', 'Agent\CountryController@create');
// Route::post('/agent/country/create', 'Agent\CountryController@store');
// Route::get('/agent/country/delete/{id}', 'Agent\CountryController@delete');
// Route::get('/agent/country/edit/{id}', 'Agent\CountryController@edit');
// Route::post('/agent/country/update', 'Agent\CountryController@update');

//state
// Route::get('/agent/state/list', 'Agent\StateController@index');
// Route::get('/agent/state/create', 'Agent\StateController@create');
// Route::post('/agent/state/create', 'Agent\StateController@store');
// Route::get('/agent/state/delete/{id}', 'Agent\StateController@delete');
// Route::get('/agent/state/edit/{id}', 'Agent\StateController@edit');
// Route::post('/agent/state/update', 'Agent\StateController@update');

//district
// Route::get('/agent/district/list', 'Agent\DistrictController@index');
// Route::get('/agent/district/create', 'Agent\DistrictController@create');
// Route::post('/agent/district/create', 'Agent\DistrictController@store');
// Route::get('/agent/district/delete/{id}', 'Agent\DistrictController@delete');
// Route::get('/agent/district/edit/{id}', 'Agent\DistrictController@edit');
// Route::post('/agent/district/update', 'Agent\DistrictController@update');

Route::get('/getState/{id}', 'Agent\DistrictController@getState');

//city
// Route::get('/agent/city/list', 'Agent\CityController@index');
// Route::get('/agent/city/create', 'Agent\CityController@create');
// Route::post('/agent/city/create', 'Agent\CityController@store');
// Route::get('/agent/city/delete/{id}', 'Agent\CityController@delete');
// Route::get('/agent/city/edit/{id}', 'Agent\CityController@edit');
// Route::post('/agent/city/update', 'Agent\CityController@update');

Route::get('/getDistrict/{id}', 'Agent\CityController@getDistrict');


//road type
// Route::get('/agent/roadType/list', 'Agent\RoadTypeController@index');
// Route::get('/agent/roadType/create', 'Agent\RoadTypeController@create');
// Route::post('/agent/roadType/create', 'Agent\RoadTypeController@store');
// Route::get('/agent/roadType/delete/{id}', 'Agent\RoadTypeController@delete');
// Route::get('/agent/roadType/edit/{id}', 'Agent\RoadTypeController@edit');
// Route::post('/agent/roadType/update', 'Agent\RoadTypeController@update');

//road size
// Route::get('/agent/roadSize/list', 'Agent\RoadSizeController@index');
// Route::get('/agent/roadSize/create', 'Agent\RoadSizeController@create');
// Route::post('/agent/roadSize/create', 'Agent\RoadSizeController@store');
// Route::get('/agent/roadSize/delete/{id}', 'Agent\RoadSizeController@delete');
// Route::get('/agent/roadSize/edit/{id}', 'Agent\RoadSizeController@edit');
// Route::post('/agent/roadSize/update', 'Agent\RoadSizeController@update');

//icon
// Route::get('/agent/icon/list', 'Agent\FeatureIconController@index');
// Route::get('/agent/icon/create', 'Agent\FeatureIconController@create');
// Route::post('/agent/icon/create', 'Agent\FeatureIconController@store');
// Route::get('/agent/icon/delete/{id}', 'Agent\FeatureIconController@delete');
// Route::get('/agent/icon/edit/{id}', 'Agent\FeatureIconController@edit');
// Route::post('/agent/icon/update', 'Agent\FeatureIconController@update');

//feature
// Route::get('/agent/feature/list', 'Agent\FeatureController@index');
// Route::get('/agent/feature/create', 'Agent\FeatureController@create');
// Route::post('/agent/feature/create', 'Agent\FeatureController@store');
// Route::get('/agent/feature/delete/{id}', 'Agent\FeatureController@delete');
// Route::get('/agent/feature/edit/{id}', 'Agent\FeatureController@edit');
// Route::post('/agent/feature/update', 'Agent\FeatureController@update');

//price
// Route::get('/agent/price/list', 'Agent\PriceController@index');
// Route::get('/agent/price/create', 'Agent\PriceController@create');
// Route::post('/agent/price/create', 'Agent\PriceController@store');
// Route::get('/agent/price/delete/{id}', 'Agent\PriceController@delete');
// Route::get('/agent/price/edit/{id}', 'Agent\PriceController@edit');
// Route::post('/agent/price/update', 'Agent\PriceController@update');

//admin
Route::get('/admin', function () {
    return view('admin.login');
});


//change password
Route::get('/admin/password/edit/{id}', 'AdminController@changePassword');
Route::post('/admin/password/update', 'AdminController@updatePassword');


//property gallary
Route::get('adminPropertyGallary/{id}','PropertyController@getPropertyGallary');

Route::post('/admin', 'AdminLoginController@index');
Route::get('/admin/dashboard', 'AdminController@index')->name('admin.dashboard');

Route::post('/admin/logout', 'AdminController@logout')->name('admin-logout');


//property type
Route::get('/admin/propertyType/list', 'PropertyTypeController@index');
Route::get('/admin/propertyType/create', 'PropertyTypeController@create');
Route::post('/admin/propertyType/create', 'PropertyTypeController@store');
Route::get('/admin/propertyType/delete/{id}', 'PropertyTypeController@delete');
Route::get('/admin/propertyType/edit/{id}', 'PropertyTypeController@edit');
Route::post('/admin/propertyType/update', 'PropertyTypeController@update');

//advertisements
Route::get('/admin/ad/list', 'AdvertisementController@index');
Route::get('/admin/ad/create', 'AdvertisementController@create');
Route::post('/admin/ad/create', 'AdvertisementController@store');
Route::get('/admin/ad/delete/{id}', 'AdvertisementController@delete');
Route::get('/admin/ad/edit/{id}', 'AdvertisementController@edit');
Route::post('/admin/ad/update', 'AdvertisementController@update');

//property feature
Route::get('/admin/propertyFeature/list', 'PropertyFeatureController@index');
Route::get('/admin/propertyFeature/create', 'PropertyFeatureController@create');
Route::post('/admin/propertyFeature/create', 'PropertyFeatureController@store');
Route::get('/admin/propertyFeature/delete/{id}', 'PropertyFeatureController@delete');
Route::get('/admin/propertyFeature/edit/{id}', 'PropertyFeatureController@edit');
Route::post('/admin/propertyFeature/update', 'PropertyFeatureController@update');

//property
Route::get('/admin/property/list', 'PropertyController@index');
Route::get('admin/property/status/{id}','PropertyController@changeStatus');
Route::get('/admin/property/details/{id}', 'PropertyController@propertyDetail');
// Route::get('/admin/property/create', 'PropertyController@create');
// Route::post('/admin/property/create', 'PropertyController@store');
// Route::get('/admin/property/delete/{id}', 'PropertyController@delete');
// Route::get('/admin/property/edit/{id}', 'PropertyController@edit');
// Route::post('/admin/property/update', 'PropertyController@update');

//careers
Route::get('admin/applicants/list', 'CareerController@index');
Route::get('admin/applicants/details/{id}', 'CareerController@details');
Route::post('admin/applicant/status','CareerController@changeStatus');
//country
Route::get('/admin/country/list', 'CountryController@index');
Route::get('/admin/country/create', 'CountryController@create');
Route::post('/admin/country/create', 'CountryController@store');
Route::get('/admin/country/delete/{id}', 'CountryController@delete');
Route::get('/admin/country/edit/{id}', 'CountryController@edit');
Route::post('/admin/country/update', 'CountryController@update');

//faq
Route::get('/admin/faq/list', 'FaqController@index');
Route::get('/admin/faq/create', 'FaqController@create');
Route::post('/admin/faq/create', 'FaqController@store');
Route::get('/admin/faq/delete/{id}', 'FaqController@delete');
Route::get('/admin/faq/edit/{id}', 'FaqController@edit');
Route::post('/admin/faq/update', 'FaqController@update');

//agency
Route::get('/admin/agency/list', 'AgencyController@index');
Route::get('/admin/agency/create', 'AgencyController@create');
Route::post('/admin/agency/create', 'AgencyController@store');
Route::get('/admin/agency/delete/{id}', 'AgencyController@delete');
Route::get('/admin/agency/edit/{id}', 'AgencyController@edit');
Route::post('/admin/agency/update', 'AgencyController@update');

//blogs
Route::get('/admin/blogs/list', 'BlogController@index');
Route::get('/admin/blogs/create', 'BlogController@create');
Route::post('/admin/blogs/create', 'BlogController@store');
Route::get('/admin/blogs/delete/{id}', 'BlogController@delete');
Route::get('/admin/blogs/edit/{id}', 'BlogController@edit');
Route::post('/admin/blogs/update', 'BlogController@update');

//state
Route::get('/admin/state/list', 'StateController@index');
Route::get('/admin/state/create', 'StateController@create');
Route::post('/admin/state/create', 'StateController@store');
Route::get('/admin/state/delete/{id}', 'StateController@delete');
Route::get('/admin/state/edit/{id}', 'StateController@edit');
Route::post('/admin/state/update', 'StateController@update');

//district
Route::get('/admin/district/list', 'DistrictController@index');
Route::get('/admin/district/create', 'DistrictController@create');
Route::post('/admin/district/create', 'DistrictController@store');
Route::get('/admin/district/delete/{id}', 'DistrictController@delete');
Route::get('/admin/district/edit/{id}', 'DistrictController@edit');
Route::post('/admin/district/update', 'DistrictController@update');

Route::get('/getState/{id}', 'DistrictController@getState');

//city
Route::get('/admin/city/list', 'CityController@index');
Route::get('/admin/city/create', 'CityController@create');
Route::post('/admin/city/create', 'CityController@store');
Route::get('/admin/city/delete/{id}', 'CityController@delete');
Route::get('/admin/city/edit/{id}', 'CityController@edit');
Route::post('/admin/city/update', 'CityController@update');

Route::get('/getDistrict/{id}', 'CityController@getDistrict');

//agent
Route::get('/admin/agent/list', 'UserController@index');
Route::get('admin/agent/status/{id}','UserController@changeStatus');
Route::get('/admin/agent/details/{id}', 'UserController@agentDetails');
// Route::get('/admin/agent/create', 'UserController@create');
// Route::post('/admin/agent/create', 'UserController@store');
// Route::get('/admin/agent/delete/{id}', 'UserController@delete');
// Route::get('/admin/agent/edit/{id}', 'UserController@edit');
// Route::post('/admin/agent/update', 'UserController@update');

Route::get('/getCity/{id}', 'UserController@getCity');

//setting
Route::get('/admin/general-setting/list', 'SettingController@gIndex');
Route::get('/admin/general-setting/create', 'SettingController@gCreate');
Route::post('/admin/general-setting/create', 'SettingController@gStore');
Route::get('/admin/general-setting/delete/{id}', 'SettingController@gDelete');
Route::get('/admin/general-setting/edit/{id}', 'SettingController@gEdit');
Route::post('/admin/general-setting/update', 'SettingController@gUpdate');

//setting
Route::get('/admin/social-media-setting/list', 'SettingController@sIndex');
Route::get('/admin/social-media-setting/create', 'SettingController@sCreate');
Route::post('/admin/social-media-setting/create', 'SettingController@sStore');
Route::get('/admin/social-media-setting/delete/{id}', 'SettingController@sDelete');
Route::get('/admin/social-media-setting/edit/{id}', 'SettingController@sEdit');
Route::post('/admin/social-media-setting/update', 'SettingController@sUpdate');

//road type
Route::get('/admin/roadType/list', 'RoadTypeController@index');
Route::get('/admin/roadType/create', 'RoadTypeController@create');
Route::post('/admin/roadType/create', 'RoadTypeController@store');
Route::get('/admin/roadType/delete/{id}', 'RoadTypeController@delete');
Route::get('/admin/roadType/edit/{id}', 'RoadTypeController@edit');
Route::post('/admin/roadType/update', 'RoadTypeController@update');

//road size
// Route::get('/admin/roadSize/list', 'RoadSizeController@index');
// Route::get('/admin/roadSize/create', 'RoadSizeController@create');
// Route::post('/admin/roadSize/create', 'RoadSizeController@store');
// Route::get('/admin/roadSize/delete/{id}', 'RoadSizeController@delete');
// Route::get('/admin/roadSize/edit/{id}', 'RoadSizeController@edit');
// Route::post('/admin/roadSize/update', 'RoadSizeController@update');

//icon
Route::get('/admin/icon/list', 'FeatureIconController@index');
Route::get('/admin/icon/create', 'FeatureIconController@create');
Route::post('/admin/icon/create', 'FeatureIconController@store');
Route::get('/admin/icon/delete/{id}', 'FeatureIconController@delete');
Route::get('/admin/icon/edit/{id}', 'FeatureIconController@edit');
Route::post('/admin/icon/update', 'FeatureIconController@update');

//feature
Route::get('/admin/feature/list', 'FeatureController@index');
Route::get('/admin/feature/create', 'FeatureController@create');
Route::post('/admin/feature/create', 'FeatureController@store');
Route::get('/admin/feature/delete/{id}', 'FeatureController@delete');
Route::get('/admin/feature/edit/{id}', 'FeatureController@edit');
Route::post('/admin/feature/update', 'FeatureController@update');

//price
Route::get('/admin/price/list', 'PriceController@index');
Route::get('/admin/price/create', 'PriceController@create');
Route::post('/admin/price/create', 'PriceController@store');
Route::get('/admin/price/delete/{id}', 'PriceController@delete');
Route::get('/admin/price/edit/{id}', 'PriceController@edit');
Route::post('/admin/price/update', 'PriceController@update');

//plan
Route::get('/admin/plan/list', 'PlanController@index');
Route::get('/admin/plan/create', 'PlanController@create');
Route::post('/admin/plan/create', 'PlanController@store');
Route::get('/admin/plan/delete/{id}', 'PlanController@delete');
Route::get('/admin/plan/edit/{id}', 'PlanController@edit');
Route::post('/admin/plan/update', 'PlanController@update');

//plan feature
Route::get('/admin/plan/feature/list', 'PlanFeatureController@index');
Route::get('/admin/plan/feature/create', 'PlanFeatureController@create');
Route::post('/admin/plan/feature/create', 'PlanFeatureController@store');
Route::get('/admin/plan/feature/delete/{id}', 'PlanFeatureController@delete');
Route::get('/admin/plan/feature/edit/{id}', 'PlanFeatureController@edit');
Route::post('/admin/plan/feature/update', 'PlanFeatureController@update');

//features to plan
Route::get('/admin/plans/features/list', 'PlansFeaturesController@index');
Route::get('/admin/plans/features/create', 'PlansFeaturesController@create');
Route::post('/admin/plans/features/create', 'PlansFeaturesController@store');
Route::get('/admin/plans/features/delete/{id}', 'PlansFeaturesController@delete');
Route::get('/admin/plans/features/edit/{id}', 'PlansFeaturesController@edit');
Route::post('/admin/plans/features/update', 'PlansFeaturesController@update');


//message
Route::get('/admin/notification/list', 'AdminController@showMessages');
Route::get('/admin/notification/details/{id}', 'AdminController@showMessageDetails');


