function showUser(value, table_name, select_table_field, response_id){
   //alert(value);
    $.ajax({
        method:'POST',
        url:'ajax/getGameDataAjax.php',
        data:{value:value, table_name:table_name, select_table_field:select_table_field}
    }).done(function( html ) {
        $('#' + response_id).html(html);
    }).fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
    });
}