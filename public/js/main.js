jQuery(function($) {'use strict',
    //navbar icon toggle
    $('.navbar-toggle').click(function(){
        $(this).toggleClass("change");
    });

    $('.showHide').click(function(){
        $('.add-note-show').toggleClass("show");
    });
    $('.clickToShow').click(function(){
        var $this = $(this);
        $this.toggleClass('SeeMore2');
        if($this.hasClass('SeeMore2')){
            $this.text('Less Search');
        } else {
            $this.text('More Search');
        }
        $('.inner-search-form .hide').toggleClass("show");
    });

    $(window).on('load', function(){
         // $('.slider-for').slick({
         //   slidesToShow: 1,
         //   slidesToScroll: 1,
         //   arrows: false,
         //   fade: true,
         //   asNavFor: '.slider-nav'
         // });
         // $('.slider-nav li').slick({
         //   slidesToShow: 1,
         //   slidesToScroll: 1,
         //   asNavFor: '.slider-for',
         //   focusOnSelect: true
         // });

        // $('.slider-nav li').click(function(){
        //     $('.slider-nav li').removeClass("change");
        //     $(this).addClass("change");
        // });

 //        $('a[data-slide]').click(function(e) {
 //   e.preventDefault();
 //   var slideno = $(this).data('slide');
 //   $('.slider-nav').slick('slickGoTo', slideno - 1);
 // });
    });
    //Smooth scroll on click
    $(document).on('click', '.faq-block a[href^="#"]', function(e) {
        // target element id
        var id = $(this).attr('href');

        // target element
        var $id = $(id);
        if ($id.length === 0) {
            return;
        }

        // prevent standard hash navigation (avoid blinking in IE)
        e.preventDefault();

        // top position relative to the document
        var pos = $(id).offset().top - 80;

        // animated top scrolling
        $('body, html').animate({scrollTop: pos});
    });

    $(function(){
        $('.combobox').combobox();
    });

    $('.ads-gallery .owl-carousel').owlCarousel({
        autoplay: true,
        autoplayTimeout: 6000,
        loop: true,
        animateOut: "fadeOut",
        margin: 0,
        stagePadding: 0,
        drag: true,
        // pagination: true,
        // mouseDrag: false,
        // pullDrag: false,
        // freeDrag: false,
        // touchDrag: false,
        // dotsEach: true,
        dots : false,
        nav: true,
        navText: [
            '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ],
        items: 1,
        responsive:{
          0:{
              items:1
          },
          475:{
              items:1
          },
          675:{
              items:2
          },
          991:{
              items :3
          },
          1200:{
              items:3
          }
      }
    });

});

$('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
    var next = $(this).next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
  
    for (var i=0;i<4;i++) {
      next=next.next();
      if (!next.length) {
        next=$(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
    }
  });