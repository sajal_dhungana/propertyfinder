<?php
	require_once("../connect/connect.php");
	require_once("../function.php");
	require_once("../includes/degree.php");
	require_once("../includes/grade.php");
	require_once("../includes/faculty.php");
	require_once("../includes/subject.php");
	require_once("../includes/chapter.php");
	require_once("../includes/chapterContent.php");
	require_once("../includes/salution.php");
	
?>
<?php
if(isset($_POST['pageId']) && !empty($_POST['pageId']) && isset($_POST['action'])){
  $id=$_POST['pageId'];
}else{
   $id='0';
}
$action=$_POST['action'];
$pageLimit=PAGE_PER_NO*$id;
$query="SELECT * FROM `degree_level` ORDER BY `id` ASC
LIMIT $pageLimit, ".PAGE_PER_NO;
$res=$Connect->runQuery($query);
$count=$Connect->numRows($res);
$table_row='';
if($count > 0){
while($row=$Connect->fetchArray($res)){
   				$table_row.= '<tr>';
				$table_row.= '<td><input type="checkbox"/></td>';
				$table_row.= "<td>".$row['name']."</td>";
				$table_row.= "<td>".$row['title']."</td>";
				$table_row.= "<td>";
				$table_row.=(($row['active'])==1)?'Yes':'No'."</td>";
				$table_row.= '<td><a href="index.php?action=set-'.$action.'&id='.$row['id'].'">Edit / View</a></td>';
				$table_row.= "<td>".$row['id']."</td>";
				$table_row.= "</tr>";
}
}else{
    $table_row='No Data Found';
}
echo $table_row;
?>
