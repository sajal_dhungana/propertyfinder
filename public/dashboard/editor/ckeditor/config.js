/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';


    // The following value defines which File Browser connector and Quick Upload
// "uploader" to use. It is valid for the default implementaion and it is here
// just to make this configuration file cleaner.
// It is not possible to change this value using an external file or even
// inline when creating the editor instance. In that cases you must set the
// values of LinkBrowserURL, ImageBrowserURL and so on.
// Custom implementations should just ignore it.
    upload_location=window.location.origin+'/sites/arrange-properties/uploads/kcfinder/';
    //alert(upload_location);
    config.uiColor = '';
    config.allowedContent=true;
    // ...
    config.filebrowserBrowseUrl = upload_location+'browse.php?opener=ckeditor&type=files';
    config.filebrowserImageBrowseUrl = upload_location+'browse.php?opener=ckeditor&type=images';
    config.filebrowserFlashBrowseUrl = upload_location+'browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = upload_location+'upload.php?opener=ckeditor&type=files';
    config.filebrowserImageUploadUrl = upload_location+'upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = upload_location+'upload.php?opener=ckeditor&type=flash';
// ...


//config.filebrowserBrowseUrl = 'http://localhost/format/admin/editor/ckeditor/plugins/imgbrowse/imgbrowse.html';
    //config.filebrowserImageBrowseUrl = 'http://localhost/format/admin/editor/ckeditor/plugins/Filemanager-master/index.html' ;
    config.toolbar =
        [

            [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt', 'AutoCorrect'  ] },
            { name: 'insert', items: ['Image', 'Mathjax', 'Table', 'HorizontalRule', 'SpecialChar', 'Youtube', 'SpecialChar'] },
            '/',																					// Line break - next group will be placed in new line.
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
            { name: 'links', items: [ 'Link', 'Unlink'] },

            '/',
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
            { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
            { name: 'document', items: [ 'Source'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
        ];
};