/*function showUser(str, id, tN, tF, se){
        $.ajax({
            type:'POST',
            url:'ajax/getDataAjax.php',
            data:{q:str, tableName:id, tableField: tN, selected: tF},
            success:function(html){
                $('#show_data').html(html);
            }
        });
    }*/



jQuery(function($) {'use strict',

    showUser('what','', '','');


    $(function() {
        $('#dataTable').DataTable();
    });

    $(".confirm-delete").click(function(){
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    });

    function initMenu() {
        $('.sidebar-menu .treeview-menu').hide();
        $('.sidebar-menu .treeview-menu:first').show();
        $('.sidebar-menu .treeview a').click(
            function() {
                var checkElement = $(this).next();
                if((checkElement.is('.treeview-menu')) && (checkElement.is(':visible'))) {
                    $(this).removeClass('selected');
                    $(this).children("i.fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-left");
                    $(this).next('ul').slideUp('1000');
                    return false;
                }
                if((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
                    checkElement.slideDown('1000');
                    $(this).children("i.fa-angle-left").removeClass("fa-angle-left").addClass("fa-angle-down");
                    $(this).addClass('selected');
                    return false;
                }
            }
        );
    }
    initMenu();
});
