<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureTypeModel extends Model
{
    protected $table = 'feature_type';
}
