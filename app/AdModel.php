<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdModel extends Model
{
    protected $table = 'advertisement';
}
