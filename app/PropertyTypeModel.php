<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyTypeModel extends Model
{
    protected $table = 'property_type';
}
