<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlansFeaturesModel extends Model
{
    protected $table = 'plans_features';

    public function plan(){
    	return $this->belongsTo('App\PlanModel','plan_id');
    }

    public function feature(){
    	return $this->belongsTo('App\PlanFeatureModel','plan_feature');
    }
}
