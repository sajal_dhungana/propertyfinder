<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoadSizeModel extends Model
{
    protected $table = 'road_size';
}
