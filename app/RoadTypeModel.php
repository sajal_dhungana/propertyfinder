<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoadTypeModel extends Model
{
    protected $table = 'road_type';
}
