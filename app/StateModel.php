<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StateModel extends Model
{
    protected $table = 'state';

    public function country(){
        return $this->belongsTo('App\CountryModel','country_id');
    }
}
