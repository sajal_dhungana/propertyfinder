<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyModel extends Model
{
    protected $table = 'property';

    public function agent(){
        return $this->belongsTo('App\User','agent_id');
    }

    public function type(){
        return $this->belongsTo('App\PropertyTypeModel','property_type_id');
    }

    public function country(){
        return $this->belongsTo('App\CountryModel','country_id');
    }

    public function city(){
        return $this->belongsTo('App\CityModel','city_id');
    }

     public function purpose(){
        return $this->belongsTo('App\PropertyPurposeModel','property_purpose_id');
    }

     public function roadSize(){
        return $this->belongsTo('App\RoadSizeModel','road_size_id');
    }

    public function roadType(){
        return $this->belongsTo('App\RoadTypeModel','road_type_id');
    }
}
