<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureIconModel extends Model
{
    protected $table = 'feature_icon';
}
