<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

class AdminModel extends Authenticatable
{
	use Notifiable;

    protected $table = 'admin';
    protected $guard = 'admin';

     protected $fillable = [
        'name', 'email', 'password','status'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
