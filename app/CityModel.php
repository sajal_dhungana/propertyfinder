<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityModel extends Model
{
    protected $table = 'city';

    public function country(){
        return $this->belongsTo('App\CountryModel','country_id');
    }

    public function state(){
        return $this->belongsTo('App\StateModel','state_id');
    }

    public function district(){
        return $this->belongsTo('App\DistrictModel','district_id');
    }
}
