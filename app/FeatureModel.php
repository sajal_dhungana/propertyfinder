<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureModel extends Model
{
    protected $table = 'feature';

    public function icon(){
        return $this->belongsTo('App\FeatureIconModel','icon_id');
    }
}
