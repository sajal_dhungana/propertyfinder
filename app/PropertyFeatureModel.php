<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyFeatureModel extends Model
{
    protected $table = 'property_feature';

    public function propertyType(){
        return $this->belongsTo('App\PropertyTypeModel','property_type_id');
    }

    public function featureType(){
        return $this->belongsTo('App\FeatureTypeModel','feature_type_id');
    }

    // public function features(){
    //     return $this->hasMany('App\FeatureModel','name');
    // }
}
