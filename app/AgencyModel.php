<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyModel extends Model
{
    protected $table = 'agency';
}
