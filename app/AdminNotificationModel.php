<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminNotificationModel extends Model
{
    protected $table = 'admin_notification';
}
