<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanModel extends Model
{
    protected $table = 'plan';
}
