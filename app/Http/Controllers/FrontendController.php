<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PropertyModel;
use App\CountryModel;
use App\PropertyTypeModel;
use App\CityModel; 
use App\PropertyPurposeModel;

use App\User;
use App\PriceModel;

use App\PropertyGallaryModel;

use App\AgentTypeModel;

use App\CommentModel;

use stdClass;

use App\ReplyModel;
use App\FaqModel;

use App\ContactModel;
use App\CareerModel;
use App\BlogModel;
use App\PlansFeaturesModel;

use App\AdModel;
use App\AgencyModel;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    public function plan(){
        $planDetails = PlansFeaturesModel::all();
        return view('plan',compact('planDetails'));
    }

    public function blogDetails($id){
        $blog = BlogModel::where('id',$id)->first();

        $allBlogs = BlogModel::where('status',1)->get();
        return view('blog_details',compact('blog','allBlogs'));
    }
    
    public function careers(){
        return view('careers');
    }

    public function contact(){
        return view('contact');
    }

    public function saveApplicants(Request $req){
        

        $applicant = new CareerModel();
        if ($req->file('cv')) 
        {    
            $file = $req->file('cv');          
            

            $ext = $file->getClientOriginalExtension();
            if($ext == 'doc' || $ext == 'docx' || $ext == 'pdf'){
                $name = time().$file->getClientOriginalName();
                $file->move('uploads/careers',$name);           
                $applicant->cv = $name;
            }else{
            Session::flash('message','Invalid file extension.');
            return redirect('careers');
            }
            
        }

        $applicant->name = $req->name;
        $applicant->email = $req->email;
        $applicant->phone = $req->phone;
        $applicant->position = $req->position;
        $applicant->tell_us = $req->tell_us;

        $applicant->save();

        Session::flash('message','Application Send Successfully.');
        return redirect('careers');
    }

    public function saveContact(Request $req){
        
        $contact = new ContactModel();
        $contact->name = $req->name;
        $contact->phone = $req->phone;
        $contact->email = $req->email;
        $contact->company = $req->company;
        $contact->message = $req->message;
        $contact->save();

        Session::flash('message','Message Send Successfully.');
        return redirect('contact');
    }

    public function sitemap(){
        return view('sitemap');
    }

    public function policy(){
        return view('policy');
    }

     public function terms(){
        return view('terms');
    }

     public function blogs(){
        $featured = BlogModel::where('featured',1)->get();

        $blogs = BlogModel::where('status',1)->paginate(8);
        return view('blogs',compact('featured','blogs'));
    }

    public function faq(){
        $faq = FaqModel::all();
       return view('faq',compact('faq')); 
    }


    public function getCityProperty($id){

        $property = PropertyModel::where('city_id',$id)->get();
        $city = CityModel::all();

        $propertyType = PropertyTypeModel::all();

        $purpose = PropertyPurposeModel::all();


        return view('search',compact('property','propertyType','city','purpose'));
    }

     public function comment(Request $req){
        $comment = new CommentModel();
        $comment->property_id = $req->property_id;
        $comment->comment_user_id = $req->comment_user_id;
        $comment->name = $req->name;
        $comment->comment = $req->comment;
        $comment->save();

        Session::forget('getInfo');

        Session::flash('message','Comment Posted Successfully.');
        return redirect('property/description/'.$req->property_id);
    }

    public function agent(){
        $agent = User::where('status',1)->get();
        $city = CityModel::all();

        $agentType = AgentTypeModel::all();

        return view('agent',compact('agent','city','agentType'));
    }

    public function buy(){

        $recent = PropertyModel::where('status',1)->orderBy('created_at', 'desc')->paginate(5);       

        //sell property
        $property = PropertyModel::whereHas('purpose', function($q){
                    $q->where('name', 'LIKE','%'.'sell'.'%');
                })->where('status',1)->orderBy('created_at','desc')->get();

        $city = CityModel::all();

        $propertyType = PropertyTypeModel::all();

        $purpose = PropertyPurposeModel::all();

        $adTop = AdModel::where('page','home')->where('section','top')->where('status',1)->first();

        $adTop = AdModel::where('page','buy')->where('section','top')->where('status',1)->first();

        return view('buy_rent',compact('property','propertyType','city','purpose','recent','adTop'));
    }

    public function rent(){

             $recent = PropertyModel::orderBy('created_at', 'desc')->paginate(5);

            
            $property = PropertyModel::whereHas('purpose', function($q){
                        $q->where('name','LIKE','%'.'rent'.'%')->orderBy('created_at','desc');
                    })->where('status',1)->orderBy('created_at','desc')->get();

            $city = CityModel::all();

            $propertyType = PropertyTypeModel::all();

            $purpose = PropertyPurposeModel::all();

             $adTop = AdModel::where('page','rent')->where('section','top')->where('status',1)->first();

            return view('buy_rent',compact('property','propertyType','city','purpose','recent','adTop'));
    }

    public function getMinMaxPrice($id){
        $prices = PriceModel::where('property_type_id',$id)->get();

        return $prices;
    }
    
    public function propertyDescription($id){
        $property = PropertyModel::where('id',$id)->get();


        if($property){
            $property = $property[0];
        
            $similar = PropertyModel::where('city_id',$property->city_id)->where('id','!=',$id)->orderBy('created_at','DESC')->take(4)->get();
        

            $agent = User::find($property->agent_id);

            $gallary = PropertyGallaryModel::where('property_id',$id)->get();
        }

        $comments = CommentModel::where('property_id',$id)->get();

        $p = PropertyModel::find($id);
        $p->views = $p->views + 2;
        $p->save();

        $adTop = AdModel::where('page','property_description')->where('section','side_top')->where('status',1)->first();

        $adButtom = AdModel::where('page','property_description')->where('section','side_buttom')->where('status',1)->first();

        return view('property_description',compact('property','agent','gallary','comments','similar','adTop','adButtom'));
    }

    public function agentDescription($id){
        
         $agent = User::find($id);
         $property = PropertyModel::where('agent_id',$id)->get();
        return view('agent_description',compact('agent','property'));
    }

    public function index(){
        $property = PropertyModel::where('status',1)->get();
        $country = CountryModel::all();
        $city = CityModel::all();

        $propertyType = PropertyTypeModel::all();

        $purpose = PropertyPurposeModel::all();

       $landForSale = PropertyModel::whereHas('purpose', function($query) {
            $query->where('name','LIKE','%'.'sell'.'%');
        })
        ->whereHas('type', function($query) {
            $query->where('name','LIKE','%'.'land'.'%');
        })
        ->where('status',1)->get();

        $houseForRent = PropertyModel::whereHas('purpose', function($query) {
            $query->where('name','LIKE','%'.'rent'.'%');
        })
        ->whereHas('type', function($query) {
            $query->where('name','LIKE','%'.'house'.'%');
        })
        ->where('status',1)->get();

         $houseForSell = PropertyModel::whereHas('purpose', function($query) {
            $query->where('name','LIKE','%'.'sell'.'%');
        })
        ->whereHas('type', function($query) {
            $query->where('name','LIKE','%'.'house'.'%');
        })
        ->where('status',1)->get();


        $featured = PropertyModel::where('featured',1)->where('status',1)->get();

        $adTop = AdModel::where('page','home')->where('section','top')->where('status',1)->first();

        $agency = AgencyModel::where('status',1)->get();

        return view('index',compact('property','country','propertyType','city','purpose','landForSale','houseForSell','houseForRent','featured','adTop','agency'));
    }

    public function searchAgent(Request $req){
     
        $city = CityModel::all();

        $agentType = AgentTypeModel::all();


        $type_id = $req->agent_type_id;
        $city_id = $req->city_id;
        $name = $req->agent_name;

        if($type_id && $city_id && $name){
            $agent = User::where('type_id',$type_id)->where('city_id',$city_id)->where('name','LIKE','%'.$name.'%')->where('status',1)->get();
             return view('agent',compact('agent','city','agentType'));
        } 

        if($type_id && $city_id && !$name){
            $agent = User::where('type_id',$type_id)->where('city_id',$city_id)->where('status',1)->get();
             return view('agent',compact('agent','city','agentType'));
        }

        if($type_id && $name && !$city_id){
            $agent = User::where('type_id',$type_id)->where('name','LIKE','%'.$name.'%')->where('status',1)->get();
             return view('agent',compact('agent','city','agentType'));

        }

        if($city_id && $name && !$type_id){
            $agent = User::where('city_id',$city_id)->where('name','LIKE','%'.$name.'%')->where('status',1)->get();
             return view('agent',compact('agent','city','agentType'));
        }

        if($type_id && !$city_id && !$name){
            $agent = User::where('type_id',$type_id)->where('status',1)->get();
             return view('agent',compact('agent','city','agentType'));
        }

        if(!$type_id && $city_id && !$name){
            $agent = User::where('city_id',$city_id)->where('status',1)->get();
             return view('agent',compact('agent','city','agentType'));
        }

        if(!$type_id && !$city_id && $name){
            $agent = User::where('name','LIKE','%'.$name.'%')->where('status',1)->get();
             return view('agent',compact('agent','city','agentType'));
        }

        $agent = User::where('status',1)->get();

        return view('agent',compact('agent','city','agentType'));
    }

    public function search(Request $req){

       
        $city = CityModel::all();

        $propertyType = PropertyTypeModel::all();

        $purpose = PropertyPurposeModel::all();


        $property_purpose_id = $req->property_purpose_id;
        $property_type_id = $req->property_type_id;
        $city_id = $req->city_id;
        $min_price = $req->min_price;
        $max_price = $req->max_price;


        $property = DB::table('property');

        if($property_purpose_id){
            $property->where('property_purpose_id',$property_purpose_id);
        }

        if($property_type_id){
            $property->where('property_type_id',$property_type_id);
        }

        if($city_id){
            $property->where('city_id',$city_id);
        }

        if($min_price){
            $property->where('price','>=',$min_price);
        }

        if($max_price){
            $property->where('price','<=',$max_price);
        }

            $property = $property->where('status',1)->get();

                 
        return view('search',compact('property','propertyType','city','purpose'));
    }
}
