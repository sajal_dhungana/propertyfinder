<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;

use App\CountryModel;

use Auth;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function index(){
        $country = CountryModel::all();
        return view('admin.country.index',compact('country'));
    }

    public function create(){
        return view('admin.country.create');
    }

    public function store(Request $req){
        
        $country = new CountryModel();
        $country->name = $req->name;

        
        $country->sortname = $req->sortname;
        $country->google_map = $req->google_map;
        $country->add_to_feature = $req->add_to_feature;
        $country->ordering = $req->ordering;
        $country->status = $req->status;
        $country->save();

        Session::flash('message','Successfully saved.');
        return redirect('admin/country/list');
    }

    public function delete($id){
        CountryModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/country/list');
    }

    public function edit($id){
        $country= CountryModel::find($id);

        return view('admin.country.edit',compact('country'));
    }

    public function update(Request $req){
       
        $country = CountryModel::find($req->id);
        $country->name = $req->name;
        
        $country->sortname = $req->sortname;
        $country->google_map = $req->google_map;
        $country->add_to_feature = $req->add_to_feature;
        $country->ordering = $req->ordering;
        $country->status = $req->status;
        $country->save(); 

        Session::flash('message','Successfully updated.');
        return redirect('admin/country/list');

    }
}
