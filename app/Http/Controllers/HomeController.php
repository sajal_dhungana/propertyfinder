<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;

use App\User;

use App\AgentTypeModel;
use App\CityModel;
use Auth;

use App\CommentModel;

use App\ReplyModel;

use App\NotificationModel;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showMessages($id){
        $notification = NotificationModel::where('agent_id',$id)->orderBy('created_at','DESC')->get();

        return view('agent.notifications',compact('notification'));
    }

    public function showMessageDetails($id){
        $notification = NotificationModel::find($id);
        
        return view('agent.notification_details',compact('notification'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('agent/profile/'.auth::user()->id);
    }

    public function edit($id){
        return view('agent.password.edit',compact('id'));
    }

    public function update(Request $req){
       
        $user = User::find($req->id);
        $user->password = bcrypt($req->password);
        $user->save();

        Auth::guard()->logout();

        return redirect('/');

    }

    public function profile($id){

        if($id == auth::user()->id){
            $user = User::find($id);
             $agentType = AgentTypeModel::all();
             $city = CityModel::all();
            return view('agent.profile.details',compact('user','agentType','city','id'));
        }else{

            Session::flash('message','You cannot access other user profile');
            return redirect('agent/profile/'.auth::user()->id);
        }
    }

    public function updateProfile(Request $req){
            $id = $req->id;

        
            $user = User::find($id);
            $user->name = $req->name;
            $user->type_id = $req->type_id;
            $user->company_name = $req->company_name;
            $user->company_registration_num = $req->company_registration_num;
            $user->city_id = $req->city_id;
            $user->address = $req->address;
            $user->contact_name = $req->contact_name;
            $user->contact_num = $req->contact_num;
            $user->contact_num1 = $req->contact_num1;


             if ($req->file('company_logo')) 
        {    
            $file = $req->file('company_logo');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/agents',$name);           
            $user->company_logo = $name;
        }

              if ($req->file('image')) 
        {    
            $file = $req->file('image');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/agents',$name);           
            $user->image = $name;
        }

            $user->save();

            Session::flash('message','Successfully updated.');
            return redirect('agent/profile/'.auth::user()->id);
        
    }

   

    public function reply(Request $req){
        $reply = new ReplyModel();
        $reply->property_id = $req->property_id;
        $reply->comment_id = $req->comment_id;
        $reply->reply_user_id = $req->reply_user_id;
        $reply->reply = $req->reply;
        $reply->save();

        Session::flash('message','Successfully replied.');
        return redirect('property/description/'.$req->property_id);
    }
}
