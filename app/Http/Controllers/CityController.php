<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;

use App\CountryModel;
use App\StateModel;
use App\DistrictModel;

use App\CityModel;

use Auth;

class CityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function getDistrict($id){
        $districts = DistrictModel::where('country_id',$id)->get();
        
        foreach($districts as $d){ ?>
                <option value="<?php echo $d->id;?>"><?php echo $d->name;?></option>
      <?php  }
    }

    public function index(){
        $city = CityModel::all();
        return view('admin.city.index',compact('city'));
    }

    public function create(){
        $country = CountryModel::all();
        $district = DistrictModel::all();
        return view('admin.city.create',compact('country','district'));
    }

    public function store(Request $req){

        $city = new CityModel();
        $city->name = $req->name;

        
        // $city->country_id = $req->country_id;
        $city->district_id = $req->district_id;
        $city->status = $req->status;
        $city->save();

        Session::flash('message','Successfully saved.');
        return redirect('admin/city/list');
    }

    public function delete($id){
        CityModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/city/list');
    }

    public function edit($id){
        $city= CityModel::find($id);

        $country = CountryModel::all();
        // $district = DistrictModel::where('country_id',$city['country_id'])->get();

        $district = DistrictModel::all();
        return view('admin.city.edit',compact('city','country','district'));
    }

    public function update(Request $req){
       
        $city = CityModel::find($req->id);
        $city->name = $req->name;

        
        // $city->country_id = $req->country_id;
        $city->district_id = $req->district_id;
        $city->status = $req->status;
        $city->save(); 

        Session::flash('message','Successfully updated.');
        return redirect('admin/city/list');

    }
}
