<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\PlanFeatureModel;

class PlanFeatureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function index(){
    	$feature = PlanFeatureModel::all();
    	return view('admin.planFeature.index',compact('feature'));
    }

    public function create(){
    	return view('admin.planFeature.create');
    }

    public function store(Request $req){
    	$feature = new PlanFeatureModel();
    	$feature->name = $req->name;
    	$feature->status = $req->status;
    	$feature->save();

    	Session::flash('message','Successfully saved.');
        return redirect('admin/plan/feature/list');
    }

    public function edit($id){
    	$feature = PlanFeatureModel::find($id);
    	return view('admin.planFeature.edit',compact('feature'));
    }

    public function update(Request $req){
    	$id = $req->id;
    	$feature = PlanFeatureModel::find($id);
    	$feature->name = $req->name;
    	$feature->status = $req->status;
    	$feature->save();

    	Session::flash('message','Successfully updated.');
        return redirect('admin/plan/feature/list');

    }

    public function delete($id){
    	PlanFeatureModel::where('id',$id)->delete();
    	Session::flash('message','Successfully deleted.');
        return redirect('admin/plan/feature/list');
    }

}
