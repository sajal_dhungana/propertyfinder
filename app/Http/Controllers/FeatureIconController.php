<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use App\FeatureIconModel;

use Auth;

class FeatureIconController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }
    
    public function index(){
        $icon = FeatureIconModel::all();
        return view('admin.icon.index',compact('icon'));
    }

    public function create(){
        return view('admin.icon.create');
    }

    public function store(Request $req){
        
        $size = new FeatureIconModel();
        $size->name = $req->name;
        $size->status = $req->status;
        $size->save();

        Session::flash('message','Successfully created.');
        return redirect('admin/icon/list');
    }

    public function edit($id){
        $icon = FeatureIconModel::find($id);
        return view('admin.icon.edit',compact('icon'));
    }

    public function update(Request $req){
        $icon = FeatureIconModel::find($req->id); 
        $icon->name = $req->name;
        $icon->status = $req->status;
        $icon->save();

        Session::flash('message','Successfully updated.');
        return redirect('admin/icon/list');
    }

    public function delete($id){
        FeatureIconModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/icon/list');
    }
}
