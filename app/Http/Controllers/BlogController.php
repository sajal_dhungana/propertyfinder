<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\BlogModel;

use Auth;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function index(){
        $blogs = BlogModel::all();
        return view('admin.blog.index',compact('blogs'));
    }

    public function create(){
    	return view('admin.blog.create');
    }

    public function store(Request $req){
        
    	$blog = new BlogModel();

    	if ($req->file('image')) 
        {    
            $file = $req->file('image');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/blogs',$name);           
            $blog->image = $name;
        }

        $blog->title = $req->title;
        $blog->description = $req->description;
        $blog->status = $req->status;

        if($req->featured){
            $blog->featured = 1;
        }

        $blog->created_by = Auth::guard('admin')->user()->id;

    	$blog->save();

    	Session::flash('message','Blog Created Successfully.');
        return redirect('admin/blogs/list');
    }

    public function edit($id){
    	$blog = BlogModel::find($id);
        return view('admin.blog.edit',compact('blog'));
    }

    public function update(Request $req){
    	$id = $req->id;
    	$blog = BlogModel::find($id);
    	if ($req->file('image')) 
        {    
            $file = $req->file('image');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/blogs',$name);           
            $blog->image = $name;
        }

        $blog->title = $req->title;
        $blog->description = $req->description;
        $blog->status = $req->status;

        if($req->featured){
            $blog->featured = 1;
        }else{
            $blog->featured = 0;
        }

        $blog->updated_by = Auth::guard('admin')->user()->id;
    	$blog->save();

    	Session::flash('message','Blog Updated Successfully.');
        return redirect('admin/blogs/list');


    }

    public function delete($id){
    	BlogModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/blogs/list');
    }


}
