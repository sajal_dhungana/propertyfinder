<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\PropertyModel;
use App\CountryModel;
use App\User;
use App\PropertyTypeModel;
use App\CityModel;
use App\RoadTypeModel;

use App\RoadSizeModel;

use App\PropertyPurposeModel;

use App\PropertyGallaryModel;

use App\NotificationModel;

use Auth;

use stdClass;

class PropertyController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function propertyDetail($id){
        $property = PropertyModel::where('id',$id)->get();
        if(!$property->isEmpty()){
            $property = $property[0];

            $gallary = PropertyGallaryModel::where('property_id',$property->id)->get();
            $count = count($gallary);
        }
        return view('admin.property.details',compact('property','gallary','count'));
    }

    public function changeStatus($id){
        $property = PropertyModel::find($id);
            if($property['status'] == 1){
                $property->status = 0;
            }else{
                $property->status = 1;
            }
        $property->save();

            $property_id = $property->id;
            $agent_id = $property->agent_id;
            $title = $property->title;
            $status = $property->status;

            $notification = new NotificationModel();
            $notification->admin_id = Auth::guard('admin')->user()->id;
            $notification->agent_id = $agent_id;
            $notification->property_id = $property_id;
            $notification->link = url('property/description/'.$property_id);
            $notification->title = $title;

            if($status == 1){
                $notification->description = 'Your property '.$title.' has been Activated';
            }else{
                $notification->description = 'Your property '.$title.' has been Deactivated';
            }
            $notification->save();

    Session::flash('message','Property Status Changed Successfully.');
        return redirect('admin/property/list');
    }

    public function index(){
        $property = PropertyModel::all();
        return view('admin.property.index',compact('property'));
    }

    public function create(){
        $country = CountryModel::all();
        $user = User::all();
        $type = PropertyTypeModel::all();
        $roadType = RoadTypeModel::all();

        $roadSize = RoadSizeModel::all();

        $purpose = PropertyPurposeModel::all();

        $city = CityModel::all();
        return view('admin.property.create',compact('country','user','type','roadType','roadSize','purpose','city'));
    }

    public function store(Request $req){
       
        $property = new PropertyModel();
        $property->title = $req->title;

        
        $property->subtitle = $req->subtitle;

         $property->description = $req->description;
        $property->disclaimer = $req->disclaimer;
        $property->video_url = $req->video_url;

        if($req->sold){
            $property->sold = 1;
        }

        if($req->featured){
            $property->featured = 1;
        }



        $property->agent_id = $req->agent_id;
        // $property->country_id = $req->country_id;
        $property->city_id = $req->city_id;
        $property->property_type_id = $req->property_type_id;
        // $property->property_for = $req->property_for;
        $property->property_purpose_id = $req->property_purpose_id;

        $property->price = $req->price;


        if ($req->file('image')) 
        {    
            $file = $req->file('image');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/property',$name);           
            $property->image = $name;
        }

        $property->latitude = $req->latitude;
        $property->longitude = $req->longitude;

        if($req->ropani){
            $property->ropani = $req->ropani;
        }
           
        if($req->aana){
            $property->aana = $req->aana;
        }
        
        if($req->paisa){
            $property->paisa = $req->paisa;
        }
        
        if($req->daam){
            $property->daam = $req->daam;
        }
        


        $property->no_of_rooms = $req->no_of_rooms;
        $property->no_of_bedrooms = $req->no_of_bedrooms;
        $property->no_of_bathrooms = $req->no_of_bathrooms;
        $property->no_of_kitchens = $req->no_of_kitchens;
        $property->no_of_living_rooms = $req->no_of_living_rooms;
        $property->no_of_floors = $req->no_of_floors;

        


        $property->road_size_id = $req->road_size_id;
        $property->road_type_id = $req->road_type_id;

         $property->hospital_name = $req->hospital_name;
        $property->school_name = $req->school_name;
        $property->market_name = $req->market_name;
        $property->bank_name = $req->bank_name;
        $property->bus_stop_name = $req->bus_stop_name;
        $property->airport_name = $req->airport_name;

        if($req->near_hospital){
            $property->near_hospital = 1;
        }
        if($req->near_school){
            $property->near_school = 1;
        }
        if($req->near_market){
            $property->near_market = 1;
        }
        if($req->near_bank){
            $property->near_bank = 1;
        }
        if($req->near_bus_stop){
            $property->near_bus_stop = 1;
        }
       
        if($req->near_airport){
            $property->near_airport = 1;
        }
        $property->status = $req->status;
        $property->save();

         $property_id = $property->id;

         $images = $req->file('images');

         if($images){

            foreach($images as $img){
                $gallary = new PropertyGallaryModel();

                 // $file = $req->file('image');          
                $name = time().$img->getClientOriginalName();
                $img->move('uploads/property',$name);           
                $gallary->image = $name;
                $gallary->property_id = $property_id;
                $gallary->save();
                
            }

        }

        Session::flash('message','Successfully saved.');
        return redirect('admin/property/list');
    }

    public function delete($id){
        PropertyModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/property/list');
    }

     public function getPropertyGallary($id){

        $gallary = PropertyGallaryModel::where('property_id',$id)->get();

        $images = [];
        $count = 1;
        foreach($gallary as $g){
            $image = new stdClass();
            $image->id= $count;
            $image->src = asset('uploads/property/'.$g->image);


            $count++;
            $images[] = $image;
        }
        return $images;
    }

    public function edit($id){

        $id = $id; 

        $country = CountryModel::all();
        $user = User::all();
        $type = PropertyTypeModel::all();

        $property = PropertyModel::find($id);

        $roadType = RoadTypeModel::all();

        $roadSize = RoadSizeModel::all();

        $purpose = PropertyPurposeModel::all();


        $city = CityModel::all();
        return view('admin.property.edit',compact('country','user','type','property','city','roadType','roadSize','purpose','id'));
    }

    public function update(Request $req){
        
        $images = $req->file('images');

        $property = PropertyModel::find($req->id);
        $property->title = $req->title;

        if ($req->file('image')) 
        {    
            $file = $req->file('image');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/property',$name);           
            $property->image = $name;
        }
        
        $property->subtitle = $req->subtitle;

         $property->description = $req->description;
        $property->disclaimer = $req->disclaimer;
        $property->video_url = $req->video_url;

        if($req->sold){
            $property->sold = 1;
        }else{
            $property->sold = 0;
        }

        if($req->featured){
            $property->featured = 1;
        }else{
            $property->featured = 0;
        }

        $property->agent_id = $req->agent_id;
        // $property->country_id = $req->country_id;
        $property->city_id = $req->city_id;
        $property->property_type_id = $req->property_type_id;
        // $property->property_for = $req->property_for;
        $property->property_purpose_id = $req->property_purpose_id;
        $property->price = $req->price;

        $property->latitude = $req->latitude;
        $property->longitude = $req->longitude;

        if($req->ropani){
            $property->ropani = $req->ropani;
        }
           
        if($req->aana){
            $property->aana = $req->aana;
        }
        
        if($req->paisa){
            $property->paisa = $req->paisa;
        }
        
        if($req->daam){
            $property->daam = $req->daam;
        }

        $property->no_of_rooms = $req->no_of_rooms;
        $property->no_of_bedrooms = $req->no_of_bedrooms;
        $property->no_of_bathrooms = $req->no_of_bathrooms;
        $property->no_of_kitchens = $req->no_of_kitchens;
        $property->no_of_living_rooms = $req->no_of_living_rooms;
        $property->no_of_floors = $req->no_of_floors;

        


        $property->road_size_id= $req->road_size_id;
        $property->road_type_id = $req->road_type_id;


          $property->hospital_name = $req->hospital_name;
        $property->school_name = $req->school_name;
        $property->market_name = $req->market_name;
        $property->bank_name = $req->bank_name;
        $property->bus_stop_name = $req->bus_stop_name;
        $property->airport_name = $req->airport_name;
        


        if($req->near_hospital){
            $property->near_hospital = 1;
        }else{
            $property->near_hospital = 0;
        }

        if($req->near_school){
            $property->near_school = 1;
        }else{
            $property->near_school = 0;
        }

        if($req->near_market){
            $property->near_market = 1;
        }else{
            $property->near_market = 0;
        }

        if($req->near_bank){
            $property->near_bank = 1;
        }else{
            $property->near_bank = 0;
        }

        if($req->near_bus_stop){
            $property->near_bus_stop = 1;
        }else{
            $property->near_bus_stop = 0;
        }

      

        if($req->near_airport){
            $property->near_airport = 1;
        }else{
            $property->near_airport = 0;
        }

        $property->status = $req->status;

        $property->save();

        PropertyGallaryModel::where('property_id',$req->id)->delete();

        


         if($images){

            foreach($images as $img){
                $gallary = new PropertyGallaryModel();
         
                $name = time().$img->getClientOriginalName();
                $img->move('uploads/property',$name);           
                $gallary->image = $name;
                $gallary->property_id = $req->id;
                $gallary->save();
                
            }
        }

        Session::flash('message','Successfully updated.');
        return redirect('admin/property/list');

    }
}
