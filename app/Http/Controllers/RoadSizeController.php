<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\RoadSizeModel;

use Auth;

class RoadSizeController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function index(){
        $size = RoadSizeModel::all();
        return view('admin.road-size.index',compact('size'));
    }

    public function create(){
        return view('admin.road-size.create');
    }

    public function store(Request $req){
        
        $size = new RoadSizeModel();
        $size->name = $req->name;
        $size->status = $req->status;
        $size->save();

        Session::flash('message','Successfully created.');
        return redirect('admin/roadSize/list');
    }

    public function edit($id){
        $size = RoadSizeModel::find($id);
        return view('admin.road-size.edit',compact('size'));
    }

    public function update(Request $req){
        $size = RoadSizeModel::find($req->id); 
        $size->name = $req->name;
        $size->status = $req->status;
        $size->save();

        Session::flash('message','Successfully updated.');
        return redirect('admin/roadSize/list');
    }

    public function delete($id){
        RoadSizeModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/roadSize/list');
    }
}
