<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Session;

use App\AdminModel;

use Auth;

class AdminLoginController extends Controller
{
    public function index(Request $req){
        

        if (\Auth::guard('admin')->attempt(['email' => $req->email, 'password' => $req->password])) {
            return redirect('admin/dashboard');
        }

            return view('admin.login');
    }
}
