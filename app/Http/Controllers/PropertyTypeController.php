<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PropertyTypeModel;

use Session;

use Auth;

class PropertyTypeController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function index(){
        $types = PropertyTypeModel::all();
        // dd($types);
        return view('admin.property-type.index',compact('types'));
    }

    public function create(){
        return view('admin.property-type.create');
    }

    public function store(Request $req){
        $type = new PropertyTypeModel();
        $type->name = $req->name;
        $type->save();

        Session::flash('message','Successfully saved.');
        return redirect('admin/propertyType/list');
    }

    public function delete($id){
        PropertyTypeModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/propertyType/list');
    }

    public function edit($id){
        $type= PropertyTypeModel::find($id);

        return view('admin.property-type.edit',compact('type'));
    }

    public function update(Request $req){
       
        $type = PropertyTypeModel::find($req->id);
        $type->name = $req->name;

        $type->save(); 

        Session::flash('message','Successfully updated.');
        return redirect('admin/propertyType/list');

    }
}
