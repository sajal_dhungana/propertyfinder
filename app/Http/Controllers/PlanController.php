<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\PlanModel;

class PlanController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function index(){
    	$plan = PlanModel::all();
    	return view('admin.plan.index',compact('plan'));
    }

    public function create(){
    	return view('admin.plan.create');
    }

    public function store(Request $req){
    	$plan = new PlanModel();
    	$plan->name = $req->name;
        $plan->price = $req->price;
    	$plan->status = $req->status;
    	$plan->save();

    	Session::flash('message','Successfully saved.');
        return redirect('admin/plan/list');
    }

    public function edit($id){
    	$plan = PlanModel::find($id);
    	return view('admin.plan.edit',compact('plan'));
    }

    public function update(Request $req){
    	$id = $req->id;
    	$plan = PlanModel::find($id);
    	$plan->name = $req->name;
        $plan->price = $req->price;
    	$plan->status = $req->status;
    	$plan->save();

    	Session::flash('message','Successfully updated.');
        return redirect('admin/plan/list');
    }

    public function delete($id){
    	PlanModel::where('id',$id)->delete();
    	Session::flash('message','Successfully deleted.');
        return redirect('admin/plan/list');
    }
}
