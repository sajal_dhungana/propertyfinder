<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use App\RoadTypeModel;

use Auth;

class RoadTypeController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');

    }
    
    public function index(){
        $roadType = RoadTypeModel::all();
        return view('admin.road-type.index',compact('roadType'));
    }

    public function create(){
        return view('admin.road-type.create');
    }

    public function store(Request $req){
        
        $type = new RoadTypeModel();
        $type->name = $req->name;
        $type->status = $req->status;
        $type->save();

        Session::flash('message','Successfully created.');
        return redirect('admin/roadType/list');
    }

    public function edit($id){
        $roadType = RoadTypeModel::find($id);
        return view('admin.road-type.edit',compact('roadType'));
    }

    public function update(Request $req){
        $type = RoadTypeModel::find($req->id); 
        $type->name = $req->name;
        $type->status = $req->status;
        $type->save();

        Session::flash('message','Successfully updated.');
        return redirect('admin/roadType/list');
    }

    public function delete($id){
        RoadTypeModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/roadType/list');
    }
}
