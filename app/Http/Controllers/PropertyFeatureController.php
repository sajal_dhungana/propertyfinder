<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PropertyTypeModel;
use App\PropertyFeatureModel;

use App\FeatureTypeModel;
use App\FeatureIconModel;

use App\FeatureModel;

use Illuminate\Support\Facades\Session;

use Auth;

class PropertyFeatureController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');

    }
    
    public function index(){
        $features = PropertyFeatureModel::all();
        return view('admin.property-feature.index',compact('features'));
    }

    public function create(){
        $propertyFeature = PropertyFeatureModel::all();
        $types = PropertyTypeModel::all();
        $featureType = FeatureTypeModel::all();
        

        $features = FeatureModel::all();
        return view('admin.property-feature.create',compact('types','featureType','propertyFeature','features'));
    }

    public function store(Request $req){

        $name = implode(',',$req->name);
       
        $feature = new PropertyFeatureModel();
        $feature->name = $name;
        $feature->property_type_id = $req->property_type_id;
        $feature->feature_type = $req->feature_type;
        $feature->save();

        Session::flash('message','Successfully saved.');
        return redirect('admin/propertyFeature/list');
    }

    public function delete($id){
        PropertyFeatureModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/propertyFeature/list');
    }

    public function edit($id){
        $propertyFeature = PropertyFeatureModel::all();
        $types = PropertyTypeModel::all();
        $featureType = FeatureTypeModel::all();
         $features = FeatureModel::all();

        $feature= PropertyFeatureModel::find($id);

        return view('admin.property-feature.edit',compact('feature','types','propertyFeature','featureType','features'));
    }

    public function update(Request $req){
        $name = implode(',',$req->name);

        $feature = PropertyFeatureModel::find($req->id);
       $feature->name = $name;
        $feature->property_type_id = $req->property_type_id;
        $feature->feature_type = $req->feature_type;
        $feature->save();

        Session::flash('message','Successfully updated.');
        return redirect('admin/propertyFeature/list');

    }
}
