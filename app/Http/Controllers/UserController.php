<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\User;
use App\CountryModel;
use App\AgentTypeModel;
use App\CityModel;
use App\PropertyModel;

use Auth;
class UserController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function agentDetails($id){
        $agent = User::where('id',$id)->get();
        if(!$agent->isEmpty()){
            $agent = $agent[0];

        $property = PropertyModel::where('agent_id',$id)->get();
        }
        return view('admin.agent.details',compact('agent','property'));

    }

    public function changeStatus($id){
        $user = User::find($id);
            if($user['status'] == 1){
                $user->status = 0;
            }else{
                $user->status = 1;
            }
        $user->save();

    Session::flash('message','Agent Status Changed Successfully.');
        return redirect('admin/agent/list');
    }

    public function getCity($id){
        $city = CityModel::where('country_id',$id)->get();
        
        foreach($city as $c){ ?>
                <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
      <?php  }
    }
    public function index(){
        $user = User::all();
        $country = CountryModel::all();
        return view('admin.agent.index',compact('country','user'));
    }

    public function create(){
        $country = CountryModel::all();
        $type = AgentTypeModel::all();
        return view('admin.agent.create',compact('country','type'));
    }

    public function store(Request $req){
        

        $userExist = User::where('email',$req->email)->get();
        if(count($userExist)){
            Session::flash('message','Agent Already Registered.');
            return redirect('admin/agent/list');
        }
        $user = new User();
        $user->name = $req->name;

        
        $user->email = $req->email;
        $user->password = bcrypt($req->password);
        $user->type_id = $req->type_id;

       
        $user->company_name = $req->company_name;
        $user->company_registration_num = $req->company_registration_num;
        $user->company_established_date = $req->company_established_date;
        $user->city_id = $req->city_id;
        $user->contact_name = $req->contact_name;
        $user->contact_num = $req->contact_num;
        $user->status = $req->status;
        $user->save();

        Session::flash('message','Successfully saved.');
        return redirect('admin/agent/list');
    }

    public function delete($id){
        User::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/agent/list');
    }

    public function edit($id){
        $user= User::find($id);

        $country = CountryModel::all();
        $type = AgentTypeModel::all();

        $city = CityModel::where('country_id',$user['country_id'])->get();
        return view('admin.agent.edit',compact('user','country','type','city'));
    }

    public function update(Request $req){
       
        $user = User::find($req->id);
        $user->name = $req->name;

        
        $user->email = $req->email;
        if($req->password){
            $user->password = bcrypt($req->password);
        }
        
        $user->type_id = $req->type_id;

       
        $user->company_name = $req->company_name;
        $user->company_registration_num = $req->company_registration_num;
        $user->company_established_date = $req->company_established_date;
        $user->city_id = $req->city_id;
        $user->contact_name = $req->contact_name;
        $user->contact_num = $req->contact_num;
        $user->status = $req->status;
        $user->save(); 

        Session::flash('message','Successfully updated.');
        return redirect('admin/agent/list');

    }
}
