<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;

use App\PropertyTypeModel;
use App\PropertyFeatureModel;

use App\FeatureTypeModel;
use App\FeatureIconModel;

use App\FeatureModel;

use App\Http\Controllers\Controller;


use Illuminate\Support\Facades\Session;

class PropertyFeatureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $features = PropertyFeatureModel::all();
        return view('agent.property-feature.index',compact('features'));
    }

    public function create(){
        $propertyFeature = PropertyFeatureModel::all();
        $types = PropertyTypeModel::all();
        $featureType = FeatureTypeModel::all();
        

        $features = FeatureModel::all();
        return view('agent.property-feature.create',compact('types','featureType','propertyFeature','features'));
    }

    public function store(Request $req){

        $name = implode(',',$req->name);
       
        $feature = new PropertyFeatureModel();
        $feature->name = $name;
        $feature->property_type_id = $req->property_type_id;
        $feature->feature_type = $req->feature_type;
        $feature->save();

        Session::flash('message','Successfully saved.');
        return redirect('agent/propertyFeature/list');
    }

    public function delete($id){
        PropertyFeatureModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('agent/propertyFeature/list');
    }

    public function edit($id){
        $propertyFeature = PropertyFeatureModel::all();
        $types = PropertyTypeModel::all();
        $featureType = FeatureTypeModel::all();
         $features = FeatureModel::all();

        $feature= PropertyFeatureModel::find($id);

        return view('agent.property-feature.edit',compact('feature','types','propertyFeature','featureType','features'));
    }

    public function update(Request $req){
        $name = implode(',',$req->name);

        $feature = PropertyFeatureModel::find($req->id);
       $feature->name = $name;
        $feature->property_type_id = $req->property_type_id;
        $feature->feature_type = $req->feature_type;
        $feature->save();

        Session::flash('message','Successfully updated.');
        return redirect('agent/propertyFeature/list');

    }
}
