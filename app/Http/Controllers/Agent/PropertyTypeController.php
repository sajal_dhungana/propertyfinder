<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;

use App\PropertyTypeModel;

use Session;

use App\Http\Controllers\Controller;

class PropertyTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $types = PropertyTypeModel::all();
        // dd($types);
        return view('agent.property-type.index',compact('types'));
    }

    public function create(){
        return view('agent.property-type.create');
    }

    public function store(Request $req){
        $type = new PropertyTypeModel();
        $type->name = $req->name;
        $type->save();

        Session::flash('message','Successfully saved.');
        return redirect('agent/propertyType/list');
    }

    public function delete($id){
        PropertyTypeModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('agent/propertyType/list');
    }

    public function edit($id){
        $type= PropertyTypeModel::find($id);

        return view('agent.property-type.edit',compact('type'));
    }

    public function update(Request $req){
       
        $type = PropertyTypeModel::find($req->id);
        $type->name = $req->name;

        $type->save(); 

        Session::flash('message','Successfully updated.');
        return redirect('agent/propertyType/list');

    }
}
