<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\PriceModel;
use App\PropertyTypeModel;

use App\Http\Controllers\Controller;

class PriceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $price = PriceModel::all();
        return view('agent.price.index',compact('price'));
    }

    public function create(){
        $propertyType = PropertyTypeModel::all();
        return view('agent.price.create',compact('propertyType'));
    }

    public function store(Request $req){
        
        $price = new PriceModel();
        $price->property_type_id = $req->property_type_id;

        
        $price->min_price = $req->min_price;
        $price->max_price = $req->max_price;
        $price->status = $req->status;
    
        $price->save();

        Session::flash('message','Successfully saved.');
        return redirect('agent/price/list');
    }

    public function delete($id){
        PriceModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('agent/price/list');
    }

    public function edit($id){
        $price= PriceModel::find($id);
        $propertyType = PropertyTypeModel::all();

        return view('agent.price.edit',compact('price','propertyType'));
    }

    public function update(Request $req){
       
        $price = PriceModel::find($req->id);
        $price->property_type_id = $req->property_type_id;

        
        $price->min_price = $req->min_price;
        $price->max_price = $req->max_price;
        $price->status = $req->status;
    
        $price->save(); 

        Session::flash('message','Successfully updated.');
        return redirect('agent/price/list');

    }
}
