<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\DistrictModel;

use App\CountryModel;
use App\StateModel;

use App\Http\Controllers\Controller;

class DistrictController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getState($id){
        $states = StateModel::where('country_id',$id)->get();
        
        foreach($states as $s){ ?>
                <option value="<?php echo $s->id;?>"><?php echo $s->name;?></option>
      <?php  }
    }
    public function index(){
        $district = DistrictModel::all();
        return view('agent.district.index',compact('district'));
    }

    public function create(){
        $country = CountryModel::all();
        $state = StateModel::all();
        return view('agent.district.create',compact('country','state'));
    }

    public function store(Request $req){
        $district = new DistrictModel();
        $district->name = $req->name;

        
        // $district->country_id = $req->country_id;
        $district->state_id = $req->state_id;
        $district->status = $req->status;
        $district->save();

        Session::flash('message','Successfully saved.');
        return redirect('agent/district/list');
    }

    public function delete($id){
        DistrictModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('agent/district/list');
    }

    public function edit($id){
        $district= DistrictModel::find($id);

        $country = CountryModel::all();
        // $state = StateModel::where('country_id',$district['country_id'])->get();

        $state = StateModel::all();
        return view('agent.district.edit',compact('district','country','state'));
    }

    public function update(Request $req){
       
        $district = DistrictModel::find($req->id);
        $district->name = $req->name;

        
        $district->country_id = $req->country_id;
        $district->state_id = $req->state_id;
        $district->status = $req->status;
        $district->save(); 

        Session::flash('message','Successfully updated.');
        return redirect('agent/district/list');

    }
}
