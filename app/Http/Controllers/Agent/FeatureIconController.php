<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use App\FeatureIconModel;

use App\Http\Controllers\Controller;

class FeatureIconController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $icon = FeatureIconModel::all();
        return view('agent.icon.index',compact('icon'));
    }

    public function create(){
        return view('agent.icon.create');
    }

    public function store(Request $req){
        
        $size = new FeatureIconModel();
        $size->name = $req->name;
        $size->status = $req->status;
        $size->save();

        Session::flash('message','Successfully created.');
        return redirect('agent/icon/list');
    }

    public function edit($id){
        $icon = FeatureIconModel::find($id);
        return view('agent.icon.edit',compact('icon'));
    }

    public function update(Request $req){
        $icon = FeatureIconModel::find($req->id); 
        $icon->name = $req->name;
        $icon->status = $req->status;
        $icon->save();

        Session::flash('message','Successfully updated.');
        return redirect('agent/icon/list');
    }

    public function delete($id){
        FeatureIconModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('agent/icon/list');
    }
}
