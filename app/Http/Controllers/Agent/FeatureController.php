<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
use App\FeatureModel;
use App\FeatureIconModel;

use App\Http\Controllers\Controller;

class FeatureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $feature = FeatureModel::all();
        return view('agent.feature.index',compact('feature'));
    }

    public function create(){
        $icon = FeatureIconModel::all();
        return view('agent.feature.create',compact('icon'));
    }

    public function store(Request $req){
  
        $feature = new FeatureModel();
        $feature->name = $req->name;
        $feature->icon_id = $req->icon_id;
        $feature->status = $req->status;
        $feature->save();

        Session::flash('message','Successfully created.');
        return redirect('agent/feature/list');
    }

    public function edit($id){
        $feature = FeatureModel::find($id);
        $icon = FeatureIconModel::all();

        return view('agent.feature.edit',compact('feature','icon'));
    }

    public function update(Request $req){
        $feature = FeatureModel::find($req->id); 
        $feature->name = $req->name;
        $feature->icon_id = $req->icon_id;
        $feature->status = $req->status;
        $feature->save();

        Session::flash('message','Successfully updated.');
        return redirect('agent/feature/list');
    }

    public function delete($id){
        FeatureModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('agent/feature/list');
    }
}
