<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\RoadSizeModel;

use App\Http\Controllers\Controller;

class RoadSizeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $size = RoadSizeModel::all();
        return view('agent.road-size.index',compact('size'));
    }

    public function create(){
        return view('agent.road-size.create');
    }

    public function store(Request $req){
        
        $size = new RoadSizeModel();
        $size->name = $req->name;
        $size->status = $req->status;
        $size->save();

        Session::flash('message','Successfully created.');
        return redirect('agent/roadSize/list');
    }

    public function edit($id){
        $size = RoadSizeModel::find($id);
        return view('agent.road-size.edit',compact('size'));
    }

    public function update(Request $req){
        $size = RoadSizeModel::find($req->id); 
        $size->name = $req->name;
        $size->status = $req->status;
        $size->save();

        Session::flash('message','Successfully updated.');
        return redirect('agent/roadSize/list');
    }

    public function delete($id){
        RoadSizeModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('agent/roadSize/list');
    }
}
