<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\AdminModel;

use App\User;

use App\PropertyModel;
use App\CityModel;

use App\AdminNotificationModel;

use Auth;

class AdminController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function showMessages(){
        $notification = AdminNotificationModel::orderBy('created_at','DESC')->get();
        return view('admin.notifications',compact('notification'));
    }

    public function showMessageDetails($id){
        $notification = AdminNotificationModel::find($id);
        return view('admin.notification_details',compact('notification'));
    }

    public function index(){
        $agent = User::all();
        $agentCount = count($agent);

        $property = PropertyModel::all();
        $propertyCount = count($property);

        $city = CityModel::all();
        $cityCount = count($city);

        $views = DB::table("property")->get()->sum("views");

        $activeProperty = PropertyModel::where('status',1)->get();
        $activeCount = count($activeProperty);

        $sell = PropertyModel::whereHas('purpose', function($q){
                    $q->where('name', 'LIKE','%'.'sell'.'%');
                })->where('status',1)->orderBy('created_at','desc')->get();

        $sellCount = count($sell);

        $rent = PropertyModel::whereHas('purpose', function($q){
                    $q->where('name', 'LIKE','%'.'rent'.'%');
                })->where('status',1)->orderBy('created_at','desc')->get();

        $rentCount = count($rent);

        //property type
        $rooms = PropertyModel::whereHas('type', function($q){
                    $q->where('name', 'LIKE','%'.'room'.'%');
                })->where('status',1)->orderBy('created_at','desc')->get();

        $roomCount = count($rooms);

        $lands = PropertyModel::whereHas('type', function($q){
                    $q->where('name', 'LIKE','%'.'land'.'%');
                })->where('status',1)->orderBy('created_at','desc')->get();

        $landCount = count($lands);

        $houses = PropertyModel::whereHas('type', function($q){
                    $q->where('name', 'LIKE','%'.'house'.'%');
                })->where('status',1)->orderBy('created_at','desc')->get();

        $houseCount = count($houses);

        $flats = PropertyModel::whereHas('type', function($q){
                    $q->where('name', 'LIKE','%'.'flat'.'%');
                })->where('status',1)->orderBy('created_at','desc')->get();

        $flatCount = count($flats);

         $shops = PropertyModel::whereHas('type', function($q){
                    $q->where('name', 'LIKE','%'.'shop'.'%');
                })->where('status',1)->orderBy('created_at','desc')->get();

        $shopCount = count($shops);

        return view('admin.index',compact('agentCount','propertyCount','cityCount','views','activeCount','sellCount','rentCount','roomCount','landCount','houseCount','flatCount','shopCount'));
    }

    public function logout(Request $request){

    	Auth::guard('admin')->logout();

        $request->session()->invalidate();
        $request->session()->flush();

        return redirect('/admin');
    }

    public function changePassword($id){
        return view('admin.password.edit',compact('id'));
    }

    public function updatePassword(Request $req){
        $admin = AdminModel::find($req->id);
        $admin->password = bcrypt($req->password);
        $admin->save();

        Auth::guard('admin')->logout();

        return redirect('/');
    }
}
