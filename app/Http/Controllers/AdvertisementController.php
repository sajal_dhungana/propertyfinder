<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\AdModel;

class AdvertisementController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
    	$ad = AdModel::all();
    	return view('admin.ad.index',compact('ad'));
    }

    public function create(){
    	return view('admin.ad.create');
    }

    public function store(Request $req){
    	$ad = new AdModel();
    	$ad->link = $req->link;
    	$ad->page = $req->page;
    	$ad->section = $req->section;
    	$ad->status = $req->status; 

    	 if ($req->file('image')) 
        {    
            $file = $req->file('image');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/ad',$name);           
            $ad->image = $name;
        }
    	$ad->save();
    	Session::flash('message','Advertisement Created Successfully.');
        return redirect('admin/ad/list');
    }

    public function edit($id){
    	$ad = AdModel::find($id);
    	return view('admin.ad.edit',compact('ad'));
    }

    public function update(Request $req){
    	$id = $req->id;
    	$ad = AdModel::find($id);
    	$ad->link = $req->link;
    	$ad->page = $req->page;
    	$ad->section = $req->section;
    	$ad->status = $req->status; 

    	 if ($req->file('image')) 
        {    
            $file = $req->file('image');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/ad',$name);           
            $ad->image = $name;
        }
    	$ad->save();
    	Session::flash('message','Advertisement Updated Successfully.');
        return redirect('admin/ad/list');

    }
}
