<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\PlansFeaturesModel;
use App\PlanFeatureModel;
use App\PlanModel;

class PlansFeaturesController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function index(){
    	$plansFeatures = PlansFeaturesModel::all();
    	return view('admin.plansFeatures.index',compact('plansFeatures'));
    }

    public function create(){
    	$plans = PlanModel::all();
    	$features = PlanFeatureModel::all();
    	return view('admin.plansFeatures.create',compact('plans','features'));
    }

    public function store(Request $req){
    	$planFeatures = new PlansFeaturesModel();
    	$planFeatures->plan_id = $req->plan_id;
    	$planfeatureSrting = implode(',',$req->plan_feature);
    	$planFeatures->plan_feature = $planfeatureSrting;
    	$planFeatures->save();

    	Session::flash('message','Successfully saved.');
        return redirect('admin/plans/features/list');
    }

    public function edit($id){
    	$plans = PlanModel::all();
    	$features = PlanFeatureModel::all();

    	$plansFeatures = PlansFeaturesModel::where('id',$id)->first();
    	return view('admin.plansFeatures.edit',compact('plans','features','id','plansFeatures'));
    }

    public function update(Request $req){
    	$id = $req->id;

    	$planFeatures = PlansFeaturesModel::find($id);
    	$planFeatures->plan_id = $req->plan_id;
    	$planfeatureSrting = implode(',',$req->plan_feature);
    	$planFeatures->plan_feature = $planfeatureSrting;
    	$planFeatures->save();

    	Session::flash('message','Successfully updated.');
        return redirect('admin/plans/features/list');
    }

    public function delete($id){
    	PlansFeaturesModel::where('id',$id)->delete();
    	Session::flash('message','Successfully deleted.');
        return redirect('admin/plans/features/list');
    }

}
