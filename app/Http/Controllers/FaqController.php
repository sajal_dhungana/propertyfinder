<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\FaqModel;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function index(){
    	$faq = FaqModel::all();
    	return view('admin.faq.index',compact('faq'));
    }

    public function create(){
    	return view('admin.faq.create');
    }

    public function store(Request $req){
    	$faq = new FaqModel();
    	$faq->question = $req->question;
    	$faq->answer = $req->answer;
    	$faq->save();

    	Session::flash('message','Successfully saved.');
        return redirect('admin/faq/list');

    }

    public function edit($id){
    	$faq= FaqModel::find($id);
    	return view('admin.faq.edit',compact('faq'));
    }

    public function update(Request $req){
    	$faq = FaqModel::find($req->id);
        $faq->question = $req->question;
    	$faq->answer = $req->answer;
    	$faq->save();

        Session::flash('message','Successfully updated.');
        return redirect('admin/faq/list');
    }

    public function delete($id){
    	  FaqModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/faq/list');
    }


}
