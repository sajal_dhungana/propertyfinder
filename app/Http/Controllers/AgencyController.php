<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\AgencyModel;

class AgencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function index(){
    	$agency = AgencyModel::all();
    	return view('admin.agency.index',compact('agency'));
    }

    public function create(){
    	return view('admin.agency.create');
    }

    public function store(Request $req){
    	$agency = new AgencyModel();

    	if ($req->file('image')) 
        {    
            $file = $req->file('image');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/agency',$name);           
            $agency->image = $name;
        }

        $agency->name = $req->name;
        $agency->email = $req->email;
        $agency->phone = $req->phone;
        $agency->location = $req->location;
        $agency->status = $req->status;

    	$agency->save();
    	Session::flash('message','Agency Created Successfully.');
        return redirect('admin/agency/list');

    }

    public function edit($id){
    	$agency = AgencyModel::find($id);
    	return view('admin.agency.edit',compact('agency'));
    }

    public function update(Request $req){
    		$id = $req->id;
    		$agency = AgencyModel::find($id);
    		if ($req->file('image')) 
        {    
            $file = $req->file('image');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/agency',$name);           
            $agency->image = $name;
        }

        $agency->name = $req->name;
        $agency->email = $req->email;
        $agency->phone = $req->phone;
        $agency->location = $req->location;
        $agency->status = $req->status;

    	$agency->save();
    	Session::flash('message','Agency Updated Successfully.');
        return redirect('admin/agency/list');
    }

    public function delete($id){
    	AgencyModel::where('id',$id)->delete();
    	Session::flash('message','Agency Deleted Successfully.');
        return redirect('admin/agency/list');
    }
}
