<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\StateModel;
use App\CountryModel;

use Auth;

class StateController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');

    }
    
    public function index(){
        $state = StateModel::all();
        return view('admin.state.index',compact('state'));
    }

    public function create(){
        $country = CountryModel::all();
        return view('admin.state.create',compact('country'));
    }

    public function store(Request $req){
        
        $state = new StateModel();
        $state->name = $req->name;

        
        // $state->country_id = $req->country_id;
        $state->google_map = $req->google_map;
        $state->status = $req->status;
        $state->save();

        Session::flash('message','Successfully saved.');
        return redirect('admin/state/list');
    }

    public function delete($id){
        StateModel::where('id',$id)->delete();
        Session::flash('message','Successfully deleted.');
        return redirect('admin/country/list');
    }

    public function edit($id){
        $state= StateModel::find($id);

        $country = CountryModel::all();
        return view('admin.state.edit',compact('state','country'));
    }

    public function update(Request $req){
       
        $state = StateModel::find($req->id);
        $state->name = $req->name;

        
        // $state->country_id = $req->country_id;
        $state->google_map = $req->google_map;
        $state->status = $req->status;
        $state->save(); 

        Session::flash('message','Successfully updated.');
        return redirect('admin/state/list');

    }
}
