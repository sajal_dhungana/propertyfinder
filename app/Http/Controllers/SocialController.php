<?php
 namespace App\Http\Controllers;
 use Illuminate\Http\Request;
 use Validator,Redirect,Response,File;
 use Socialite;
 // use App\User;
 use App\CustomerModel;

 use Illuminate\Support\Facades\URL;
  use Illuminate\Support\Facades\Session;

 class SocialController extends Controller
 {
 public function redirect($provider,$id)
 {
    Session::put('property_id',$id);
     return Socialite::driver($provider)->redirect();
      
 }
 public function callback($provider,Request $request)
 {
    $id = Session::get('property_id');
 	
 	$getInfo = Socialite::driver($provider)->user();
 
   $user = $this->createUser($getInfo,$provider); 
   // auth()->login($user);
   Session::forget('property_id');

   Session::put('getInfo',$getInfo);

   if($id){
    return redirect('property/description/'.$id);
   } 
   
 }
 function createUser($getInfo,$provider){
 // $user = User::where('provider_id', $getInfo->id)->first();

 	$user = CustomerModel::where('provider_id', $getInfo->id)->first();
 if (!$user) {
     //  $user = User::create([
     //     'name'     => $getInfo->name,
     //     'email'    => $getInfo->email,
     //     'provider' => $provider,
     //     'provider_id' => $getInfo->id
     // ]);

 	$user = CustomerModel::create([
         'name'     => $getInfo->name,
         'email'    => $getInfo->email,
         'provider' => $provider,
         'provider_id' => $getInfo->id
     ]);
   }
   return $user;
 }
 }