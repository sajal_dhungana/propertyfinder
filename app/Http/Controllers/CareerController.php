<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CareerModel;

class CareerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function changeStatus(Request $req){
        $applicant = CareerModel::find($req->id);
        $applicant['status'] = $req->status;
        $applicant->save();
    }

    public function index(){
    	$applicants = CareerModel::all();
    	 return view('admin.applicant.index',compact('applicants'));
    }

    public function details($id){
        $applicant = CareerModel::find($id);
        return view('admin.applicant.details',compact('applicant'));
    	
    }
}
