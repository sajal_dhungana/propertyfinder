<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;

use App\SettingModel;
use App\SocialSettingModel;

use Auth;

class SettingController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');

    }

    public function gIndex(){
        $setting = SettingModel::all();
        return view('admin.setting.index',compact('setting'));
    }

    public function gCreate(){
        return view('admin.setting.create');
    }

    public function gStore(Request $req){
        
        $setting = new SettingModel();
        $setting->site_title = $req->site_title;
        $setting->site_address = $req->site_address;
        $setting->site_email = $req->site_email;
        $setting->site_phone = $req->site_phone;
        $setting->site_mobile = $req->site_mobile;
        $setting->site_fax = $req->site_fax;
        $setting->site_open_time = $req->site_open_time;
        $setting->site_close_time = $req->site_close_time;
        $setting->site_footer_text = $req->site_footer_text;
        $setting->site_person_name = $req->site_person_name;
        $setting->site_person_number = $req->site_person_number;
        $setting->site_person_email = $req->site_person_email;
        $setting->site_embed_map = $req->site_embed_map;
        $setting->site_msg_title = $req->site_msg_title;
        $setting->site_msg_name = $req->site_msg_name;
        $setting->site_msg_message = $req->site_msg_message;

        if ($req->file('site_msg_photo')) 
        {    
            $file = $req->file('site_msg_photo');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/setting',$name);           
            $setting->site_msg_photo = $name;
        }

        if ($req->file('site_logo')) 
        {    
            $file = $req->file('site_logo');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/setting',$name);           
            $setting->site_logo = $name;
        }

        if ($req->file('site_favicon')) 
        {    
            $file = $req->file('site_favicon');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/setting',$name);           
            $setting->site_favicon = $name;
        }
        $setting->save();

        Session::flash('message','Successfully saved.');
        return redirect('admin/general-setting/list');
    }

    // public function gDelete($id){
    //     SettingModel::where('id',$id)->delete();
    //     Session::flash('message','Successfully deleted.');
    //     return redirect('admin/general-setting/list');
    // }

    public function gEdit($id){
        $setting = SettingModel::find($id);

        return view('admin.setting.edit',compact('setting'));
    }

    public function gUpdate(Request $req){
       
        $setting = SettingModel::find($req->id);
        $setting->site_title = $req->site_title;
        $setting->site_address = $req->site_address;
        $setting->site_email = $req->site_email;
        $setting->site_phone = $req->site_phone;
        $setting->site_mobile = $req->site_mobile;
        $setting->site_fax = $req->site_fax;
        $setting->site_open_time = $req->site_open_time;
        $setting->site_close_time = $req->site_close_time;
        $setting->site_footer_text = $req->site_footer_text;
        $setting->site_person_name = $req->site_person_name;
        $setting->site_person_number = $req->site_person_number;
        $setting->site_person_email = $req->site_person_email;
        $setting->site_embed_map = $req->site_embed_map;
        $setting->site_msg_title = $req->site_msg_title;
        $setting->site_msg_name = $req->site_msg_name;
        $setting->site_msg_message = $req->site_msg_message;

        if ($req->file('site_msg_photo')) 
        {    
            $file = $req->file('site_msg_photo');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/setting',$name);           
            $setting->site_msg_photo = $name;
        }

        if ($req->file('site_logo')) 
        {    
            $file = $req->file('site_logo');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/setting',$name);           
            $setting->site_logo = $name;
        }

        if ($req->file('site_favicon')) 
        {    
            $file = $req->file('site_favicon');          
            $name = time().$file->getClientOriginalName();
            $file->move('uploads/setting',$name);           
            $setting->site_favicon = $name;
        }
        $setting->save(); 

        Session::flash('message','Successfully updated.');
        return redirect('admin/general-setting/list');

    }

    //social media settings
    public function sIndex(){
        $setting = SocialSettingModel::all();
        return view('admin.setting.social.index',compact('setting'));
    }

    public function sCreate(){
        return view('admin.setting.social.create');
    }

    public function sStore(Request $req){
        
        $setting = new SocialSettingModel();
        $setting->site_facebook = $req->site_facebook;
        $setting->site_twitter = $req->site_twitter;
        $setting->site_linkedin = $req->site_linkedin;
        $setting->site_instagram = $req->site_instagram;
        $setting->site_viber = $req->site_viber;
        $setting->site_youtube = $req->site_youtube;
        $setting->site_whatsapp = $req->site_whatsapp;
        $setting->save();

        Session::flash('message','Successfully saved.');
        return redirect('admin/social-media-setting/list');
    }

    // public function sDelete($id){
    //     SettingModel::where('id',$id)->delete();
    //     Session::flash('message','Successfully deleted.');
    //     return redirect('admin/social-media-setting/list');
    // }

    public function sEdit($id){
        $setting = SocialSettingModel::find($id);

        return view('admin.setting.social.edit',compact('setting'));
    }

    public function sUpdate(Request $req){
       
        $setting = SocialSettingModel::find($req->id);
        $setting->site_facebook = $req->site_facebook;
        $setting->site_twitter = $req->site_twitter;
        $setting->site_linkedin = $req->site_linkedin;
        $setting->site_instagram = $req->site_instagram;
        $setting->site_viber = $req->site_viber;
        $setting->site_youtube = $req->site_youtube;
        $setting->site_whatsapp = $req->site_whatsapp;
        $setting->save();

        Session::flash('message','Successfully updated.');
        return redirect('admin/social-media-setting/list');

    }
}
