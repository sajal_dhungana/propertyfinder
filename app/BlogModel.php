<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogModel extends Model
{
    protected $table = 'blog';

    public function admin(){
        return $this->belongsTo('App\AdminModel','created_by');
    }
}
