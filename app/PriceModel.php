<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceModel extends Model
{
    protected $table = 'price';

    public function propertyTye(){
        return $this->belongsTo('App\PropertyTypeModel','property_type_id');
    }
}
