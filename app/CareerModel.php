<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerModel extends Model
{
    protected $table = 'careers';
}
