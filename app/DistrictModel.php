<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistrictModel extends Model
{
    protected $table = 'district';

    public function country(){
        return $this->belongsTo('App\CountryModel','country_id');
    }

    public function state(){
        return $this->belongsTo('App\StateModel','state_id');
    }
}
