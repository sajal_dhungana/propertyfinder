<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyGallaryModel extends Model
{
    protected $table = 'property_gallary';
}
