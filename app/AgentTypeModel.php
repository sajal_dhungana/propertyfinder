<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentTypeModel extends Model
{
    protected $table = 'agent_type';
}
