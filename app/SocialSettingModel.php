<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialSettingModel extends Model
{
    protected $table = 'social_setting';
}
