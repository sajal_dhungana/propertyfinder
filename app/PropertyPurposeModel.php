<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyPurposeModel extends Model
{
    protected $table = 'property_purpose';
}
